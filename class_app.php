<?php 
/*******************************************************
 * Copyright (C) 2015 Business Connexion (T) 
 * 
 * This file is part of Uhuru Pay API.
 * 
 * This source can not be copied and/or distributed without the express
 * permission of the owner
 
 *******************************************************/

Class transaction extends CRUD{
    protected $msg;                     //Request message Object
        protected $db;                     //Database connection
        protected $messageCode;
    
    function __construct($db){
       //Set the database connection and recieve the transaction request message
       $this->db=$db;   
       $rawJSON = file_get_contents("php://input");
       $this->msg=json_decode($rawJSON, true);    
       $this->log_event('JsonReq',$rawJSON);       
      // $this->msg=json_decode($rawJSON, true);
         
    }

    //API FOR BOOKING SYSTEM
    public function processBooking()
    {
        
        if(isset($this->msg['route_code']))
        {
			$field_61=$this->msg['route_code'];
        }else{ 
            if(isset($this->msg[0]['route_code']))
            {
              $field_61=$this->msg[0]['route_code'];
            }        
		}
        switch($field_61){
            case '000':
                //SplashScreen
                $this->splashScreenVerification();
                break;
            case '001':                
                 $this->getBusesPerRoutePerDate();
                break;
            case '002':
            
                 $this->getStations();  

                 break;     
            case '003':
        
                $this->getBookingTickets();

                 break;  
            case '033':
                    
                $this->getBookingTicketsConductor();

                 break;     
             case '004':
             
                $this->getBookedSeats();

                 break;
             case '005':

                $this->cargoPayment();
             
                 break;
             case '006':
            
                 $this->getClasses();   

                 break;

            case '007':

                $this->getOperator();
            
                break;  

            case '008':

                $this->inquireReferenceNumber();

                break;  

            case '009':

                $this->getAllBuses();

                break;  
            case '010':

                $this->verifyTicket();
            
                break;   
            case '011':
                
                $this->getBusesPerRoutePerDatePerClass();

                break; 
            case '012':

                $this->printSoldTicket();
            
                break;  
            case '013':
            
                $this->displaySummaryOfSales();

                break;  
            case '014':

                $this->getBookedSeatsConductor();
            
                break;             
            case '021':
            
                $this->getPickDropPoints();

                break; 
            case '040':

                $this->checkAmountCollected();

               break;    
            case '220':
					$this->process_offline_transaction();
               break;    

            default:
                # code...

                break;
        }
    }


    public function checkAmountCollected()
    {
        if(isset($this->msg['bus_id']))
        {
            $bus_id=$this->msg['bus_id'];
        } 
        else{
            $bus_id="0";
        }
        
        if(isset($this->msg['route_from']))
        {
             $route_from=$this->msg['route_from'];
        }
        else{
            $route_from="0";
        }
        if(isset($this->msg['route_to']))
        {
             $route_to=$this->msg['route_to'];
        }
        else{
            $route_to="0";
        }

        if(isset($this->msg['start_date']))
        {
            $start_date=$this->dateConverter($this->msg['start_date']);
        }
        else{
            $start_date="";
        }

        if(isset($this->msg['end_date']))
        {
             $end_date=$this->dateConverter($this->msg['end_date']);
        }
        else{
            $end_date="";
        }




        $sql="SELECT SUM(amount) as amount , COUNT(*) as tickets FROM `tbl_booking_details` WHERE bus_id=:bus_id OR from_stop_id=:route_from AND to_stop_id=:route_to AND pay_status=:pay_status AND terminal_date BETWEEN :from_date AND :to_date";//

        try{

            $pay_status="P";
            $sql = $this->db->prepare($sql);
            $sql->bindParam(':bus_id',$bus_id);
            $sql->bindParam(':from_date',$start_date);
            $sql->bindParam(':to_date',$end_date);
            $sql->bindParam(':route_from',$route_from);
            $sql->bindParam(':route_to',$route_to);
            $sql->bindParam(':pay_status',$pay_status);
            $sql->execute();
            $output = $sql->fetch(PDO::FETCH_ASSOC);
            $output['field_39']='00';

        }catch(PDOException $e)
        {
            echo $e->getMessage();
             $output['field_39']='91';
        }


        echo json_encode($output);



    }



    public function splashScreenVerification()
    {

        if(isset($this->msg['field_68']))
        {
            $device=$this->msg['field_68'];
        }

       // $sql="SELECT count(*) as counter,device_type,printer_BDA FROM `tbl_device_details` WHERE device_imei=:device_imei";
        $sql="SELECT device_type,printer_BDA,device_imei FROM `tbl_device_details` WHERE device_imei=:device_imei";

        try{

            $sql = $this->db->prepare($sql);
            $sql->bindParam(':device_imei',$device);
            $sql->execute();
            $output = $sql->fetch(PDO::FETCH_ASSOC);

            //if($output['counter']>=1)
            if($output['device_imei']!='')
            {
                $output['field_39']='00';
            }
            else{
                $output['field_39']='91';
            }

        }catch(PDOException $e)
        {
            echo $e->getMessage();
        }


        echo json_encode($output);

    }

    public function displaySummaryOfSales()
    {

        if(isset($this->msg['from_date'],$this->msg['to_date'],$this->msg['user_id']))
        {
            $from_date=$this->dateConverter($this->msg['from_date']);
            $to_date=$this->dateConverter($this->msg['to_date']);
            $user_id=$this->msg['user_id'];
        }
        $sql = "SELECT count(*) as tickets, sum(amount) as total_amount FROM `tbl_booking_details` WHERE user_id=:user_id AND terminal_date BETWEEN :from_date AND :to_date";

        try{

            $sql=$this->db->prepare($sql);
            $sql->bindParam(':from_date',$from_date);
            $sql->bindParam(':to_date',$to_date);
            $sql->bindParam(':user_id',$user_id);
            $sql->execute();
            $output=$sql->fetch(PDO::FETCH_ASSOC);
            $output['field_39']='00';


        }catch(PDOException $e)
        {
            $e->getMessage();
            $output['field_39']='91';
        }

        echo json_encode($output);
    }
    
    public function getPickDropPoints(){
        if(isset($this->msg['bus_id'])){
            $bus_id = $this->msg['bus_id'];
        }
        $sql="SELECT `id` as 'stop_id',stop_name,`status`
                FROM `tbl_pick_drop_stops` AS PD INNER JOIN tbl_stops AS S ON S.stop_id=PD.stop_id 
                INNER JOIN tbl_routes AS R ON R.route_id=PD.route_id 
                INNER JOIN tbl_bus_details AS B ON B.route_id=R.`route_id` 
                WHERE B.bus_id=:bus_id";
        try
        {
            $sql=$this->db->prepare($sql);
            $sql->bindParam(':bus_id',$bus_id);
            $sql->execute();
            $output=$sql->fetchAll(PDO::FETCH_ASSOC);
            //$output['field_39']='00';
        }catch(PDOException $e)
        {
            $e->getMessage();
            $output['field_39']='91';
        }
        echo json_encode($output);
    }

    public function printSoldTicket()
    {
        $sql="SELECT  A.first_name, A.last_name, A.category,A.gender, A.seatno, B.booking_ref,B.amount ,C.bus_name,
				C.plate_number,B.terminal_date as travel_date,B.pay_status ,get_time_depert(B.terminal_date,B.bus_id,B.company_id) AS time_departure, 
				get_time_arrival(B.terminal_date,B.bus_id,B.company_id) AS time_arrival
				,O.stop_name as stop_from, N.stop_name as stop_to,P.phone_number, NULL as 'p_o_box'
				FROM `tbl_customer_details` as A 
				LEFT JOIN `tbl_booking_details` as B ON A.booking_id=B.book_ID 
				LEFT JOIN `tbl_bus_details` as C ON B.bus_id=C.bus_id 
				LEFT JOIN tbl_stops as O ON O.stop_id=B.`from_stop_id`
				LEFT JOIN tbl_stops as N ON N.stop_id=B.`to_stop_id`
				INNER JOIN tbl_bus_companies AS P ON P.company_id=B.company_id
				WHERE B.booking_ref=:booking_ref";

        try
		{

            $sql=$this->db->prepare($sql);
            $sql->bindParam(':booking_ref',$this->msg['booking_ref']);
            $sql->execute();
            $output = $sql->fetchAll(PDO::FETCH_ASSOC);

        }
		catch(PDOException $e)
        {
            $e->getMessage();
        }

        echo json_encode($output);
    }


    public function verifyTicket()
    {

        if(isset($this->msg['booking_ref']))
        {
            $booking_ref=$this->msg['booking_ref'];
        }

        $sql="SELECT A.booking_ref, A.seat_no, B.bus_name, B.plate_number, A.pay_status,A.terminal_date as travel_date,
		A.amount FROM `tbl_booking_details` as A INNER JOIN `tbl_bus_details` as B ON A.bus_id=B.bus_id 
		WHERE booking_ref=:booking_ref OR phone_number=:booking_ref";

        try{
        $sql = $this->db->prepare($sql);
        $sql->bindParam(':booking_ref',$booking_ref);
        $sql->execute();
        $output=$sql->fetch(PDO::FETCH_ASSOC);

        }catch(PDOException $e)
        {
            echo $e->getMessage();
        }
        echo json_encode($output);
    }
    public function inquireReferenceNumber()
    {

        if(isset($this->msg['phone_number'],$this->msg['bus_id']))
        {
            $phone_number=$this->msg['phone_number'];
            $bus_id=$this->msg['bus_id'];
            $reference='P';
        }

        $sql="SELECT DISTINCT payment_ref,terminal_date,seat_no,amount 
				FROM `tbl_booking_details` WHERE phone_number=:phone_number 
				AND pay_status=:pay_status AND bus_id=:bus_id AND terminal_date>=CURDATE()";

        try{
        $sql = $this->db->prepare($sql);
        $sql->bindParam('phone_number',$phone_number);
        $sql->bindParam('pay_status',$reference);
        $sql->bindParam('bus_id',$bus_id);
        $sql->execute();
        $output=$sql->fetchAll(PDO::FETCH_ASSOC);

        }catch(PDOException $e)
        {
            echo $e->getMessage();
        }
        echo json_encode($output);

    }

    public function cargoPayment($route_to, $route_from)
    {
        if(isset($this->msg['receipient_name']) && isset($this->msg['phone_number']) && isset($this->msg['stop_from_id']) && isset($this->msg['stop_to_id']) && isset($this->msg['price']))
        {
                $name=$this->msg['receipient_name'];
                $phone = $this->msg['phone_number'];
                $from = $this->msg['route_from'];
                $to = $this->msg['route_to'];
                $price = $this->msg['price'];
                $ref = time().rand(10*45, 100*98);
        }


        $sql="INSERT INTO `tbl_luggage_payments`
                (
                    `receipient_name`,`phone_number`,`stop_from_id`, `stop_to_id`, 
                    `price`,`payment_ref`,`payment_datetime`
                )       
                VALUES 
                (
                    :name,:phone,:route_from,:route_to,
                    :price,:ref,NOW()
                )";

        $sql=$this->db->prepare($sql);

        $parameters=array(
                                            'name'=>$name,
                                            'phone'=>$phone,
                                            'route_from'=>$route_from,
                                            'route_to'=>$route_to,
                                            'price'=>$price,
                                            'ref'=>$ref
                                            
                                    );

        $status=$sql->execute($parameters);//


        return $status;
                    


    }

    public function dateConverter($date)
    {
        
        $date = str_replace('/', '-', $date);
        $final_date  = date("Y-m-d",strtotime($date));

        return $final_date;
    }


    public function getBookedSeats()
    {

        if(isset($this->msg['bus_id']) && isset($this->msg['travel_date']))
        {
            $bus_id = $this->msg['bus_id'];
            $booking_date=$this->dateConverter($this->msg['travel_date']);
        }

        $sql="SELECT seat_no as seats,seat_position FROM `tbl_booking_details` 
			  WHERE (book='R' OR book='B') AND bus_id=:bus_id 
				AND terminal_date=:travel_date";

        try{
            $sql=$this->db->prepare($sql);
            $sql->bindParam(':bus_id',$bus_id);
            $sql->bindParam(':travel_date',$booking_date);
            $sql->execute();
            $output=$sql->fetchAll(PDO::FETCH_ASSOC);
        }
        catch(PDOException $e){
            echo $e->getMessage();
        }

        echo json_encode($output);


    }



     public function getBookedSeatsConductor()
    {

        if(isset($this->msg['bus_id']) && isset($this->msg['travel_date']))
        {
            $bus_id = $this->msg['bus_id'];
            $booking_date=$this->dateConverter($this->msg['travel_date']);
        }
		$this->log_event('DATA-014',$bus_id.','.$booking_date);
        $sql="SELECT seat_no as seats,seat_position FROM `tbl_booking_details` WHERE book<>'E' 
			AND bus_id=:bus_id AND terminal_date=:travel_date";

        $sql1= "SELECT `bus_id`, `bus_name`, `plate_number`, `route_id`, `class_id`, `total_seats`, 
				`number_row`, `total_back_seats`, CONCAT(`left_seat_format`,'x' ,`right_seat_format`) as seat_orientation, 
				`door_row`, `company_id` FROM `tbl_bus_details` WHERE bus_id=:bus_id";

        //$sql2="SELECT fare FROM tbl_fare_table WHERE route_id=:route_id AND class_id=:class_id";
        $sql2="SELECT fare FROM tbl_fare_configurations 
				WHERE route_id=:route_id AND class_id=:class_id AND company_id=:company_id";

        try{
            //Query for booked seats
            $sql=$this->db->prepare($sql);
            $sql->bindParam(':bus_id',$bus_id);
            $sql->bindParam(':travel_date',$booking_date);
            $sql->execute();
            $output=$sql->fetchAll(PDO::FETCH_ASSOC);

            //Query for bus details
            $sql1=$this->db->prepare($sql1);
            $sql1->bindParam(':bus_id',$bus_id);
            $sql1->execute();
            $result = $sql1->fetch(PDO::FETCH_ASSOC);
            $result['booked_seats']=$output;

            $class_id=$result['class_id'];
            $route_id=$result['route_id'];
            $company_id=$result['company_id'];
          

            //Query for fare
            $sql2=$this->db->prepare($sql2);
            $sql2->bindParam(':route_id',$route_id);
            $sql2->bindParam(':class_id',$class_id);
            $sql2->bindParam(':company_id',$company_id);
            $sql2->execute();

            $result2 = $sql2->fetch(PDO::FETCH_ASSOC);
            $result['fare']=$result2['fare'];

        }
        catch(PDOException $e){
            echo $e->getMessage();
        }

        echo json_encode($result);


    }




    public function getBusesPerRoutePerDate()
    {

        if(isset($this->msg['route_from'])){
			$from = $this->msg['route_from'];
			$to = $this->msg['route_to'];
			$terminal_date  = strtotime($this->msg['travel_date']);
			$date = str_replace('/', '-', $this->msg['travel_date']);
			$booking_date  = date("Y-m-d",strtotime($date));
        }
        $sql="SELECT B.bus_id,B.plate_number,B.total_seats,C.class_name,V.`stop_name` as via, 
            CONCAT(O.`stop_name`, ' -TO- ', D.`stop_name`) AS route_name,
             B.total_back_seats, CONCAT_WS('x',B.left_seat_format,B.right_seat_format) as seat_orientation, 
            B.bus_name, B.company_id,S.time_departure,S.time_arrival, 
            get_company_configured_fare(B.route_id,B.class_id,B.company_id) AS fare,
            B.route_id 
            FROM `tbl_bus_travel_schedule` AS S 
            INNER JOIN tbl_bus_details AS B ON B.bus_id=S.bus_id 
            INNER JOIN tbl_routes AS R ON R.route_id=B.route_id 
            INNER JOIN tbl_classes AS C ON C.class_id=B.class_id 
            INNER JOIN tbl_stops AS O ON O.stop_id = R.stop_from
            INNER JOIN tbl_stops AS D ON D.stop_id = R.stop_to
            INNER JOIN tbl_stops AS V ON V.stop_id = R.via_stop
            WHERE S.date_travel=:date_travel AND R.stop_from=:stop_from AND R.stop_to=:stop_to
			AND NOW() <= concat(S.date_travel,' ',S.time_departure) - INTERVAL 1 HOUR";
	
        try{
            $sql=$this->db->prepare($sql);
            $sql->bindParam(':date_travel',$booking_date);
            $sql->bindParam(':stop_from',$from);
            $sql->bindParam(':stop_to',$to);
            $sql->execute();
            $outputs=$sql->fetchAll(PDO::FETCH_ASSOC);
			$i=0;
			foreach($outputs as $row){
				$output['bus_id']=$row['bus_id'];
				$output['plate_number']=$row['plate_number'];
				$output['total_seats']=$row['total_seats'];
				$output['class_name']=$row['class_name'];
				$output['via']=$row['via'];
				$output['route_name']=$row['route_name'];
				$output['total_back_seats']=$row['total_back_seats'];
				$output['seat_orientation']=$row['seat_orientation'];
				$output['bus_name']=$row['bus_name'];
				$output['company_id']=$row['company_id'];
				$output['time_departure']=$row['time_departure'];
				$output['time_arrival']=$row['time_arrival'];
				$output['fare']=$row['fare'];
				$output['route_id']=$row['route_id'];
				$output['label']= $this->get_seats_label($row['bus_id']);	
				$outputs[$i]=$output;
				$i++;
			}
			$output=$outputs;
        }
        catch(PDOException $e){
            echo $e->getMessage();
        }

        echo json_encode($output);
    }

	public function get_seats_label($bus_id){
		$sql="SELECT `seat_position`, `label` FROM `tbl_seat_records` WHERE `bus_id`=:bus";
		try{
			$sql=$this->db->prepare($sql);
            $sql->bindParam(':bus',$bus_id);
            $sql->execute();
            $outputs=$sql->fetchAll(PDO::FETCH_ASSOC);
		}catch(PDOException $e){
			echo $e->getMessage();
		}
		return $outputs;
	}
	
	public function get_route_passed(){
		$array=array();
		for($i=1; $i<3; $i++){
			$array[]='{"route_id":"'.$i.'","stop_from_id":"'.$i.'","stop_to_id":"'.$i.'","fare":"1000"}';
		}
		return $array;
	}

     public function getBusesPerRoutePerDatePerClass()
    {

        if(isset($this->msg['route_from'])){
        $from = $this->msg['route_from'];
        $to = $this->msg['route_to'];
        $class_id = $this->msg['class_id'];
        $terminal_date  = strtotime($this->msg['travel_date']);

        $date = str_replace('/', '-', $this->msg['travel_date']);

        $booking_date  = date("Y-m-d",strtotime($date));
        }
        $sql="SELECT B.bus_id,B.plate_number,B.total_seats,C.class_name,V.`stop_name` as via, 
            CONCAT(O.`stop_name`, ' -TO- ', D.`stop_name`) AS route_name,
             B.total_back_seats, CONCAT_WS('x',B.left_seat_format,B.right_seat_format) as seat_orientation, 
            B.bus_name, B.company_id,S.time_departure,S.time_arrival, 
            get_company_configured_fare(B.route_id,B.class_id,B.company_id) AS fare,
            B.route_id 
            FROM `tbl_bus_travel_schedule` AS S 
            INNER JOIN tbl_bus_details AS B ON B.bus_id=S.bus_id 
            INNER JOIN tbl_routes AS R ON R.route_id=B.route_id 
            INNER JOIN tbl_classes AS C ON C.class_id=B.class_id 
            INNER JOIN tbl_stops AS O ON O.stop_id = R.stop_from
            INNER JOIN tbl_stops AS D ON D.stop_id = R.stop_to
            INNER JOIN tbl_stops AS V ON V.stop_id = R.via_stop
            WHERE S.date_travel=:date_travel AND R.stop_from=:stop_from AND R.stop_to=:stop_to 
			AND C.class_id=:class_id AND NOW() <= concat(S.date_travel,' ',S.time_departure) - INTERVAL 1 HOUR";

			try{
            $sql=$this->db->prepare($sql);
            $sql->bindParam(':date_travel',$booking_date);
            $sql->bindParam(':stop_from',$from);
            $sql->bindParam(':stop_to',$to);
            $sql->bindParam(':class_id',$class_id);
            $sql->execute();
            $outputs=$sql->fetchAll(PDO::FETCH_ASSOC);
			$i=0;
			foreach($outputs as $row){
				$output['bus_id']=$row['bus_id'];
				$output['plate_number']=$row['plate_number'];
				$output['total_seats']=$row['total_seats'];
				$output['class_name']=$row['class_name'];
				$output['via']=$row['via'];
				$output['route_name']=$row['route_name'];
				$output['total_back_seats']=$row['total_back_seats'];
				$output['seat_orientation']=$row['seat_orientation'];
				$output['bus_name']=$row['bus_name'];
				$output['company_id']=$row['company_id'];
				$output['time_departure']=$row['time_departure'];
				$output['time_arrival']=$row['time_arrival'];
				$output['fare']=$row['fare'];
				$output['route_id']=$row['route_id'];				
				$output['label']= $this->get_seats_label($row['bus_id']);	
				$outputs[$i]=$output;
				$i++;
			}
			$output=$outputs;
        }
        catch(PDOException $e){
            echo $e->getMessage();
        }

        echo json_encode($output);
    }



    //

 public function getAllBuses()
    {

        

        $sql="SELECT DISTINCT B.bus_id,B.plate_number,B.total_seats,C.class_name,R.route_name,
             B.total_back_seats, CONCAT_WS('x',B.left_seat_format,B.right_seat_format) as seat_orientation, 
            B.bus_name, B.company_id,S.time_departure,S.time_arrival, get_fare_route_class(B.route_id,B.class_id) AS fare,
            B.route_id 
            FROM `tbl_bus_travel_schedule` AS S 
            INNER JOIN tbl_bus_details AS B ON B.bus_id=S.bus_id 
            INNER JOIN tbl_routes AS R ON R.route_id=B.route_id 
            INNER JOIN tbl_classes AS C ON C.class_id=B.class_id 
            ";


        try{
            $sql=$this->db->prepare($sql);
            // $sql->bindParam(':date_travel',$booking_date);
            // $sql->bindParam(':stop_from',$from);
            // $sql->bindParam(':stop_to',$to);
            $sql->execute();
            $output=$sql->fetchAll(PDO::FETCH_ASSOC);
        }
        catch(PDOException $e){
            echo $e->getMessage();
        }

        echo json_encode($output);
    }
    


    public function getBuses()
    {

          //Variable from APP

        if(isset($this->msg['route_from'])){
        $from = $this->msg['route_from'];
        $to = $this->msg['route_to'];
        $go_date = $this->msg['go_date'];
        $return_date = $this->msg['return_date'];
        }
        

        $sql="SELECT A.bus_id,A.bus_name, A.plate_number, 
        B.route_name, A.class_id, C.class_name, A.total_seats,
        A.total_back_seats, CONCAT_WS('x',A.left_seat_format,A.right_seat_format) as seat_orientation,A.company_id,D.company_name FROM 
        `tbl_bus_details` as A INNER JOIN `tbl_routes` as B ON A.route_id=B.route_id INNER JOIN `tbl_classes` as C ON A.class_id=C.class_id INNER JOIN `tbl_bus_companies` as D ON A.company_id=D.company_id";
        
        try{
            $sql=$this->db->prepare($sql);
            $sql->execute();
            $output=$sql->fetchAll(PDO::FETCH_ASSOC);
        }
        catch(PDOException $e){
            echo $e->getMessage();
        }

         echo json_encode($output);
    }


    public function getStations()
    {
        //Variable from APP
        if(isset($this->msg['route_from']) && isset($this->msg['route_to']) && isset($this->msg['go_date'])){
        $from = $this->msg['route_from'];
        $to = $this->msg['route_to'];
        $go_date = $this->msg['go_date'];
        }
        

        $sql="SELECT * FROM tbl_stops";
        
        try{
            $sql=$this->db->prepare($sql);
            $sql->execute();
            $output=$sql->fetchAll(PDO::FETCH_ASSOC);
        }
        catch(PDOException $e){
            echo $e->getMessage();
        }

        echo json_encode($output);
    }


    public function getStationsConductor()
    {
        //Variable from APP
        if(isset($this->msg['route_from']) && isset($this->msg['route_to']) && isset($this->msg['go_date']) && isset($this->msg['route_id'])){
        $from = $this->msg['route_from'];
        $to = $this->msg['route_to'];
        $go_date = $this->msg['go_date'];
        $route_id=$this->msg['route_id'];
        }
        

        $sql="SELECT * FROM tbl_stops WHERE route_id=:route_id";
        
        try{
            $sql=$this->db->prepare($sql);
            $sql->bindParam('route_id',$route_id);
            $sql->execute();
            $output=$sql->fetchAll(PDO::FETCH_ASSOC);
        }
        catch(PDOException $e){
            echo $e->getMessage();
        }

        echo json_encode($output);
    }




        public function getClasses()
    {
        $sql="SELECT `class_id`,`class_name` FROM `tbl_classes` WHERE 1";
        
        try{
            $sql=$this->db->prepare($sql);
            $sql->execute();
            $output=$sql->fetchAll(PDO::FETCH_ASSOC);
        }
        catch(PDOException $e){
            echo $e->getMessage();
        }

        echo json_encode($output);
    }

    public function getOperator()
    {
					
		$username = $this->msg['field_42'];
		$password = $this->msg['field_52'];
		$imei = $this->msg['field_68'];
		$operator_type=$this->check_who_login($imei,$username);
		$hastrip=$this->check_if_hastrip($operator_type[2]);
		if(empty($hastrip)){
			$hastrip='NO';
		}else{
			$hastrip='YES';
		}
		if($operator_type[0]==1){
			$sql="SELECT operator_id,operator_type,P.phone_number, NULL as 'p_o_box' FROM tbl_operator AS O
				INNER JOIN bus_ticket.tbl_device_details AS D ON D.device_ID=O.device_ID
				INNER JOIN tbl_bus_companies AS P ON P.company_id=O.company_id
				where username=:username AND password=:password AND device_imei=:imei";
			$sql=$this->db->prepare($sql);
			$sql->bindParam(':username',$username);
			$sql->bindParam(':password',$password);
			$sql->bindParam(':imei',$imei);
			$sql->execute();
			$result=$sql->fetch();
			if($result[0]==0)
			{
				$this->msg['field_39']='91';
				echo (json_encode($this->msg));
			}else{
				$this->msg['field_39']='00';
				$this->msg['operator_id']=$result[0];
				$this->msg['operator_type']=$result[1];
				echo (json_encode($this->msg));
			}
		}
		else{
            try{
				$sql="SELECT O.operator_id,O.operator_type,O.company_id,O.bus_id,B.bus_name, B.plate_number,
							CONCAT(F.stop_name, '-TO-', T.stop_name, '-Via-', V.stop_name) as route,
							`total_seats`,`number_row`, `total_back_seats`, CONCAT(`left_seat_format`,'x' ,`right_seat_format`) as seat_orientation
							,F.stop_name, T.stop_name,P.phone_number, NULL as 'p_o_box',get_company_configured_fare(B.route_id,B.class_id,B.company_id) AS fare,R.route_id
						FROM bus_ticket.`tbl_operator` AS O 
						INNER JOIN bus_ticket.tbl_device_details AS D ON D.device_ID=O.device_ID
                    				inner join tbl_bus_details AS B ON B.bus_id=O.bus_id
                     				inner join tbl_routes AS R ON B.route_id=R.route_id
                       				inner join tbl_stops AS F ON F.stop_id=R.stop_from
                      				inner join tbl_stops AS T ON T.stop_id=R.stop_to
                     				inner join tbl_stops AS V ON V.stop_id=R.via_stop
						INNER JOIN tbl_bus_companies AS P ON P.company_id=O.company_id
						WHERE O.username=:username AND O.password=:password AND D.device_imei=:imei AND O.status=1 AND D.activation_status='1'";
				

				$sql=$this->db->prepare($sql);
				$sql->bindParam(':username',$username);
				$sql->bindParam(':password',$password);
				$sql->bindParam(':imei',$imei);
				$sql->execute();
				$result=$sql->fetch();
				if($result[0]==0)
				{
					$this->msg['field_39']='91';
					$this->msg['count']=$result[0];
					echo (json_encode($this->msg));
				}else{
					$this->msg['field_39']='00';
					$this->msg['operator_id']=$result[0];
					$this->msg['operator_type']=$result[1];
					$this->msg['company_id']=$result[2];
					$this->msg['bus_id']=$result[3];
					$this->msg['bus_name']=$result[4];
					$this->msg['plate_number']=$result[5];
					$this->msg['route']=$result[6];
					$this->msg['total_seats']=$result[7];
					$this->msg['number_row']=$result[8];
					$this->msg['total_back_seats']=$result[9];
					$this->msg['seat_orientation']=$result[10];
					$this->msg['from_stop']=$result[11];
					$this->msg['to_stop']=$result[12];
					$this->msg['phone_number']=$result[13];
					$this->msg['p_o_box']=$result[14];
					$this->msg['fare']=$result[15];
					$this->msg['route_id']=$result[16];
					$this->msg['hastrip']=$hastrip;
					$this->msg['main_route']=$this->get_route_passed();
					$this->msg['label']=$this->get_seats_label($result[3]);

					
					echo (json_encode($this->msg));
				}

            }catch(PDOException $e){
				echo json_encode($e->getMessage());
            }
        }
    }
    
	public function check_who_login($imei,$username){
		$sql="SELECT operator_type,device_imei,bus_id FROM tbl_operator 
				INNER JOIn tbl_device_details ON tbl_operator.device_ID=tbl_device_details.device_ID
				where device_imei=:imei AND username=:username";
		$parameters=array(':imei'=>$imei, ':username'=>$username);
		$result = $this->fetch_query_result($sql, $parameters, FALSE);
		return $result;
	}
	
	public function check_if_hastrip($busid){
		$sql="SELECT bus_id FROM tbl_bus_travel_schedule where date_travel=curdate() and bus_id=:busid";
		$parameters=array(':busid'=>$busid);
		$result = $this->fetch_query_result($sql, $parameters, FALSE);
		return $result;
	}
	
    public function getBookingTickets()
    {
		if(empty($this->msg['route_from']) && empty($this->msg['route_to'])){
			$stops_id = $this->get_route_stops($this->msg['route_id']);
			$stop1=$stops_id[0];
			$stop2=$stops_id[1];
		}else{
			$stop1=$this->msg['route_from'];
			$stop2=$this->msg['route_to'];
		}
		$reference_no= time('is');
		
		/* if($this->msg['channel_id']=='A'){
			$this->post_to_agent_float($this->msg,$reference_no);
		} */
		
        $sql="INSERT INTO `tbl_booking_details`
                (
                    `booking_ref`,`amount`,`payment_ref`, `phone_number`, 
                    `seat_no`,`seat_position`, `bus_id`, `channel_id`, 
                    `system_date`, `terminal_date`,`travel_date`,`from_stop_id`, 
                    `to_stop_id`, `book`, `pay_status`,
                    `user_id`,`company_id`
                )       
                VALUES 
                (
                    :b_ref,:amount,:pay_ref,:p_number,
                    :s_no,:seat_position,:b_id,:channel_id,NOW(),
                    :terminal_date,:travel_date,:from_stop_id,:to_stop_id,
                    :book_status,:pay_status,:user_id,
                    :company_id
                )";


        $sql1="INSERT INTO `tbl_customer_details`
                (
                    `first_name`,`last_name`, `gender`, 
                    `booking_id`,`category`,`seatno`
                )       
                VALUES 
                (
                    :first_name,:last_name,:gender,
                    :booking_id,:category,:seatno
                )";
                
        try{
            if(isset($this->msg['terminal_date']))
            {

            $terminal_date = $this->dateConverter($this->msg['terminal_date']);

            }
            else{

             $terminal_date = $this->dateConverter($this->msg['travel_date']);
   
            }

            $travel_date = $this->dateConverter($this->msg['travel_date']);

            
            $route_from = $this->msg['route_from'];
            $route_to = $this->msg['route_to'];

            $sql=$this->db->prepare($sql);
            $parameters=array(
                                            'b_ref'=>$reference_no,
                                            'amount'=>$this->msg['amount'],
                                            'pay_ref'=>$reference_no,
                                            'p_number'=>$this->msg['phone_number'],
                                            's_no'=>$this->msg['seats'],
                                            'seat_position'=>$this->msg['seat_position'],
                                            'b_id'=>$this->msg['bus_id'],
                                            'channel_id'=>$this->msg['channel_id'],
                                            'terminal_date'=>$terminal_date,
                                            'travel_date'=>$travel_date,
                                            'from_stop_id'=>$stop1,
                                            'to_stop_id'=>$stop2,
                                            'book_status'=>'R',
                                            'pay_status'=>'N',
                                            'user_id'=>$this->msg['user_id'],
                                            'company_id'=>$this->msg['company_id']
                                    );

            $status=$sql->execute($parameters);
            
            if($status!==false){
				$status = $this->db->lastInsertId();
            }
            $sql1 = $this->db->prepare($sql1);
            $someArray = $this->msg['passengers']; 
            foreach ($someArray as $key => $msg){              

				$parameters=array(     
										'first_name'=>$msg['fname'],
										'last_name'=>$msg['lname'],
										'gender'=>$msg['gender'],
										'category'=>$msg['category'],
										'booking_id'=>$status,
										'seatno'=>$msg['seat_no']
								);                
                $sql1->execute($parameters);
            }
			if($this->msg['channel_id']=='A'){
				$details=$this->get_details_booking($reference_no,$this->msg['amount']);
				if($details[0]!='' && $details[1]!=''){					
						$resp_float = $this->post_to_agent_float($this->msg,$reference_no);
						if($resp_float[2]["value"]["string"]=='0'){	
							if($this->post_transaction($details[1],$details[2],$resp_float[0]["value"]["string"],'Merchant','1',$details[3],$details[4], $details[0], date('Ymdhis'),date('ydms')))
							{
								$this->update_booking_details($details[0],$details[1],'B','P');
								$bankResp = $this->post_trnx_bank($details);
								if($bankResp['responseCode']=="00"){
									$parameters['field_39']="00";
									$parameters['booking_ref']=$reference_no;
									$parameters['pay_ref']="TKT".$reference_no;
									print_r(json_encode($parameters));
								}
								else
								{
									$this->update_failed_transaction($details[0],$details[1],$bankResp);
									$this->log_event('REPORT','Failed To post to bank');
									$parameters['field_39']="00";
									$parameters['booking_ref']=$reference_no;
									$parameters['pay_ref']="TKT".$reference_no;
									print_r(json_encode($parameters));
								}
							}
						}else{
							$this->update_booking_details($details[0],$details[1],'E','N');
							$parameters['field_39']="01";
							$parameters['booking_ref']=$reference_no;
							$parameters['pay_ref']="TKT".$reference_no;
							print_r(json_encode($parameters));
						}
				}
			}
			else
			{
				$busDetails = $this->get_bus_details($this->msg['bus_id']);
				$fullName=$this->get_passenger_details($status);
				$text="Ndugu ".$fullName[0].", Umefanikiwa kuhifadhi tiketi namba ".$reference_no.", siti namba ".$this->msg['seats'].", kwenye basi namba ".$busDetails[0].", Nauli Tsh".$this->msg['amount']." lipa kupitia kumbukumbu namba TKT".$reference_no.". Malipo yafanyike ndani ya dakika 15. Asante.";
				$this->completeRegistration($this->msg['phone_number'],$text);
				$resp='00';
				if($resp=='00' || $resp=='01'){
					$parameters['field_39']="00";
					$parameters['booking_ref']=$reference_no;
					$parameters['pay_ref']="TKT".$reference_no;

					print_r(json_encode($parameters));
				}else{
					$parameters['field_39']="00";
					$parameters['booking_ref']=$reference_no;
					$parameters['pay_ref']="TKT".$reference_no;

					print_r(json_encode($parameters));
				}
			}
            // return $status;

        }catch(PDOException $e)
        {
            echo $e->getMessage();

            $parameters['field_39']="91";

            print_r(json_encode($parameters));
        }   
             
    }

	public function get_route_stops($route){
		$sql="SELECT `stop_from`,`stop_to`,`via_stop` FROM `tbl_routes` WHERE `route_id`=:route";
		$parameters=array(':route'=>$route);
		$result=$this->fetch_query_result($sql, $parameters, FALSE);
		return $result;
	}

	public function post_to_agent_float($msg,$ref){
		$timestamp = date('Y-m-d\TH:i:s\Z', time());
		$agent_details = $this->get_agent_details($msg);
		$msisdn=$agent_details[0];
		$sender=$agent_details[1];
		$secret_key='@etick2o2opay';
		$amount = $msg['amount'];
		$cellid = $msg['field_58'];
		$pin=$msg['pin_number'];
		$msisdn=$this->resolve_mobile_number($msisdn);
		$checksum = $ref.'+'.$pin.'+'.$timestamp.'+'.$amount.'+'.$cellid.'+'.$ref.'+'.$msisdn.$secret_key;	
		$this->log_event('CHECKSUM',$checksum);
		$checksum = base64_encode(hash('SHA256',$checksum,true));
        //1. Prepare the connectivity parameters.
        $url="http://10.60.81.5:8096/kwava/pay";
        //2.Prepare the xml data
		$snip='<methodCall>
				<methodName>KWAVA.Pay</methodName>
					<params>
						<param>
							<value>
								<struct>
									<member>
										<name>vendor</name>
										<value>
											<string>ETICK</string>
										</value>
									</member>
									<member>
										<name>SecretKey</name>
										<value>
											<string>'.$secret_key.'</string>
										</value>
									</member>
									<member>
										<name>pin</name>
										<value>
											<string>'.$pin.'</string>
										</value>
									</member>
									<member>
										<name>utilitycode</name>
										<value>
											<string>POS'.$cellid.'</string>
										</value>
									</member>
									<member>
										<name>utilityref</name>
										<value>
											<string>'.$ref.'</string>
										</value>
									</member>
									<member>
										<name>cellid</name>
										<value>
											<string>'.$cellid.'</string>
										</value>
									</member>
									<member>
										<name>transid</name>
										<value>
											<string>'.$ref.'</string>
										</value>
									</member>
									<member>
										<name>amount</name>
										<value>
											<string>'.$amount.'</string>
										</value>
									</member>
									<member>
										<name>msisdn</name>
										<value>
											<string>'.$msisdn.'</string>
										</value>
									</member>
									<member>
										<name>sender</name>
										<value>
											<string>'.$sender.'</string>
										</value>
									</member>
									<member>
										<name>timestamp</name>
										<value>
											<string>'.$timestamp.'</string>
										</value>
									</member>
									<member>
										<name>checksum</name>
										<value>
											<string>'.$checksum.'</string>
										</value>
									</member>
								</struct>
							</value>
						</param>
				</params>
			</methodCall>';
        $xml_data = str_replace("\n", '', $snip); 
        $this->log_event('JSON',$xml_data);
        //3.Send the Request to the API
        try
        {
            $curl = curl_init();
            curl_setopt_array($curl, array(
                CURLOPT_POST=>1,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_HTTPHEADER =>array('Accept: application/xml','Content-Type: application/xml'),
                CURLOPT_URL => "$url",
                CURLOPT_SSL_VERIFYPEER=>false,
                CURLOPT_SSL_VERIFYHOST=>0,
                CURLOPT_POSTFIELDS=>$xml_data,
                CURLOPT_CONNECTTIMEOUT=>0,
                CURLOPT_TIMEOUT=>12
            ));            
            $resp = curl_exec($curl);
            $httpCode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
            if (FALSE === $resp)
            {
               throw new Exception(curl_error($curl), curl_errno($curl));
            }
            curl_close($curl);
        }
        catch(Exception $e) {
        //trigger_error(sprintf('Curl failed with error #%d: %s',   $e->getCode(), $e->getMessage()),E_USER_ERROR);

        }
        //4. Parse the Server Response
        $resp=trim($resp);
        $response='';
        if($httpCode==200)
        {
			//5.Interprets a string of XML into an object 
            $response=$resp;			
			$response=$this->process_xml_response($response);
			$json = json_encode($response);
			$array = json_decode($json,TRUE);
			$response=$array["params"]["param"]["value"]["struct"]["member"];
        }
        else{
            $response='01';
        }		
		return $response;
	}
	
	public function process_xml_response($values){
		$xml = new SimpleXMLElement($values);
		return $xml;
	}
	public function get_agent_details($details){
		$imei = $details['field_58'];
		$agentid = $details['user_id'];
		$sql="SELECT phone, operator_name 
				FROM tbl_operator
				inner join tbl_device_details on tbl_operator.device_ID=tbl_device_details.device_ID
				where operator_id=:agent and device_imei=:imei";
		$parameters=array(':imei'=>$imei, ':agent'=>$agentid);
		$result=$this->fetch_query_result($sql, $parameters, FALSE);
		return $result;
	}
	
	public function process_offline_transaction(){
        $response=array();
        $i=0;
        ob_implicit_flush(true);
        
        foreach ($this->msg as $msg)
        {
            $response[$i]=$this->processAuthAdvc($msg);
            $i++;
        }        
        $this->msg=$response;
		print_r(json_encode($this->msg));
    }
	public function processAuthAdvc($msg){
		 
		if(empty($msg['route_from']) && empty($msg['route_to'])){
			$stops_id = $this->get_route_stops($msg['route_id']);
			$stop1=$stops_id[0];
			$stop2=$stops_id[1];
		}else{
			$stop1=$msg['route_from'];
			$stop2=$msg['route_to'];
		}
		$reference_no= $msg['booking_ref'];
		
		
        $sql="INSERT INTO `tbl_booking_details`
                (
                    `booking_ref`,`amount`,`payment_ref`, `phone_number`, 
                    `seat_no`,`seat_position`, `bus_id`, `channel_id`, 
                    `system_date`, `terminal_date`,`travel_date`,`from_stop_id`, 
                    `to_stop_id`, `book`, `pay_status`,
                    `user_id`,`company_id`
                )       
                VALUES 
                (
                    :b_ref,:amount,:pay_ref,:p_number,
                    :s_no,:seat_position,:b_id,:channel_id,NOW(),
                    :terminal_date,:travel_date,:from_stop_id,:to_stop_id,
                    :book_status,:pay_status,:user_id,
                    :company_id
                )";


        $sql1="INSERT INTO `tbl_customer_details`
                (
                    `first_name`,`last_name`, `gender`, 
                    `booking_id`,`category`,`seatno`
                )       
                VALUES 
                (
                    :first_name,:last_name,:gender,
                    :booking_id,:category,:seatno
                )";
                
        try{
            if(isset($msg['terminal_date']))
            {

            $terminal_date = $this->dateConverter($msg['terminal_date']);

            }
            else{

             $terminal_date = $this->dateConverter($msg['travel_date']);
   
            }

            $travel_date = $this->dateConverter($msg['travel_date']);
            
            $route_from = $msg['route_from'];
            $route_to = $msg['route_to'];

            $sql=$this->db->prepare($sql);
            $parameters=array(
								'b_ref'=>$reference_no,
								'amount'=>$msg['amount'],
								'pay_ref'=>$reference_no,
								'p_number'=>$msg['phone_number'],
								's_no'=>$msg['seatno'],
								'seat_position'=>$msg['seat_position'],
								'b_id'=>$msg['bus_id'],
								'channel_id'=>'A',
								'terminal_date'=>$terminal_date,
								'travel_date'=>$travel_date,
								'from_stop_id'=>$stop1,
								'to_stop_id'=>$stop2,
								'book_status'=>'R',
								'pay_status'=>'N',
								'user_id'=>$msg['user_id'],
								'company_id'=>$msg['company_id']
                             );

            $status=$sql->execute($parameters);
            
            if($status!==false){
				$status = $this->db->lastInsertId();
            }
            $sql1 = $this->db->prepare($sql1);		
			$parameters=array(     
									'first_name'=>$msg['first_name'],
									'last_name'=>$msg['last_name'],
									'gender'=>$msg['gender'],
									'category'=>$msg['category'],
									'booking_id'=>$status,
									'seatno'=>$msg['seatno']
							);                
			$sql1->execute($parameters);
            
				$details=$this->get_details_booking($reference_no,$msg['amount']);
				if($details[0]!='' && $details[1]!=''){					
						$resp_float = $this->post_to_agent_float($msg,$reference_no);
						if($resp_float[2]["value"]["string"]=='0'){	
							if($this->post_transaction($details[1],$details[2],$resp_float[0]["value"]["string"],'Merchant','1',$details[3],$details[4], $details[0], date('Ymdhis'),date('ydms')))
							{
								$this->update_booking_details($details[0],$details[1],'B','P');
								$bankResp = $this->post_trnx_bank($details);
								if($bankResp['responseCode']=="00"){
									$msg['status']="00";
								}
								else
								{
									$this->update_failed_transaction($details[0],$details[1],$bankResp);
									$this->log_event('REPORT','Failed To post to bank');
									$msg['status']="00";
								}
							}
						}else{
							//$this->update_booking_details($details[0],$details[1],'E','N');
							$msg['status']="01";
						}
				}           

        }catch(PDOException $e)
        {
            echo $e->getMessage();

            $msg['field_39']="91";
        }   
		return $msg;
	}
    public function getBookingTicketsConductor($stop_id){
		if(!isset($this->msg['route_from']) && !isset($this->msg['route_to'])){
			$stops_id = $this->get_route_stops($this->msg['route_id']);
			$stop1=$stop_id[0];
			$stop2=$stop_id[1];
		}else{
			$stop1=$this->msg['route_from'];
			$stop2=$this->msg['route_to'];
		}
		
        $sql="INSERT INTO `tbl_booking_details`
                (
                    `booking_ref`,`amount`,`payment_ref`, `phone_number`, 
                    `seat_no`,`seat_position`, `bus_id`, `channel_id`, 
                    `system_date`, `terminal_date`,`travel_date`,`route_id`, 
                    `book`, `pay_status`,
                    `user_id`,`company_id`,`from_stop_id`, 
                    `to_stop_id`
                )       
                VALUES 
                (
                    :b_ref,:amount,:pay_ref,:p_number,
                    :s_no,:seat_position,:b_id,:channel_id,NOW(),
                    :terminal_date,:travel_date,:route_id,
                    :book_status,:pay_status,:user_id,
                    :company_id,:from_Stop,:to_stop
                )";


        $sql1="INSERT INTO `tbl_customer_details`
                (
                    `first_name`,`last_name`, `gender`, 
                    `booking_id`,`category`,`seatno`
                )       
                VALUES 
                (
                    :first_name,:last_name,:gender,
                    :booking_id,:category,:seatno
                )";
                


        try{

            

            if(isset($this->msg['terminal_date']))
            {

            $terminal_date = $this->dateConverter($this->msg['terminal_date']);

            }
            else{

             $terminal_date = $this->dateConverter($this->msg['travel_date']);
   
            }

            $travel_date = $this->dateConverter($this->msg['travel_date']);

            $reference_no= time('is');
        

            $sql=$this->db->prepare($sql);
            $parameters=array(
                                            'b_ref'=>$reference_no,
                                            'amount'=>$this->msg['amount'],
                                            'pay_ref'=>$reference_no,
                                            'p_number'=>$this->msg['phone_number'],
                                            's_no'=>$this->msg['seats'],
                                            'seat_position'=>$this->msg['seat_position'],
                                            'b_id'=>$this->msg['bus_id'],
                                            'channel_id'=>$this->msg['channel_id'],
                                            'terminal_date'=>$terminal_date,
                                            'travel_date'=>$travel_date,
                                            'route_id'=>$this->msg['route_id'],
                                            'book_status'=>'R',
                                            'pay_status'=>'N',
                                            'user_id'=>$this->msg['user_id'],
                                            'company_id'=>$this->msg['company_id'],
                                            'from_stop'=>$stop1,
                                            'to_stop'=>$stop2
                                    );

            $status=$sql->execute($parameters);
            
            if($status!==false){
            $status = $this->db->lastInsertId();
            }


            $sql1 = $this->db->prepare($sql1);

             $someArray = $this->msg['passengers']; 
             foreach ($someArray as $key => $msg) {
                

                $parameters=array(          'first_name'=>$msg['fname'],
                                            'last_name'=>$msg['lname'],
                                            'gender'=>$msg['gender'],
                                            'category'=>$msg['category'],
                                            'booking_id'=>$status,
                                            'seatno'=>$msg['seat_no']
                             );

                
                $sql1->execute($parameters);
            }
			$busDetails=$this->get_bus_details($this->msg['bus_id']);
			$fullName=$this->get_passenger_details($status);
			$text="Ndugu ".$fullName[0].", Umefanikiwa kuhifadhi tiketi namba ".$reference_no.", siti namba ".$this->msg['seats'].", kwenye basi namba ".$busDetails[0].", Nauli Tsh".$this->msg['amount']." lipa kupitia kumbukumbu namba TKT".$reference_no.". Malipo yafanyike ndani ya dakika 15. Asante.";
            $this->completeRegistration($this->msg['phone_number'],$text);
            $resp = '00';
            if($resp=='00' || $resp=='01'){
                $parameters['field_39']="00";
                $parameters['booking_ref']=$reference_no;
                $parameters['pay_ref']="TKT".$reference_no;

                print_r(json_encode($parameters));
            }else{
                $parameters['field_39']="00";
                $parameters['booking_ref']=$reference_no;
                $parameters['pay_ref']="TKT".$reference_no;

                print_r(json_encode($parameters));
            }
    
            // return $status;

        }catch(PDOException $e)
        {
            echo $e->getMessage();

            $parameters['field_39']="91";

            print_r(json_encode($parameters));
        }   
             
    }

    public function completeRegistration($msisdn, $text){
         $msisdn=$this->resolve_mobile_number($msisdn);
        //1. Prepare the connectivity parameters.
        //$url="http://41.188.142.126:7070/UmojaMobileTransactions/services/UmojaMobileService/tTiketiSmsGateway";
$url="http://41.188.142.126:7070/UmojaMobileTransactions/services/UmojaMobileService/thirdPartySmsGateway";

        //2.Prepare the xml data
        $snip='{    
                        "text":"'.$text.'", 
                        "msisdn":"'.$msisdn.'", 
                        "source":"Tkt-Mtandao"  
                    }';

        $xml_data = str_replace("\n", '', $snip); 
        $this->log_event('JSON',$xml_data);
        //3.Send the Request to the API
        try
        {
            $curl = curl_init();
            curl_setopt_array($curl, array(
                CURLOPT_POST=>1,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_HTTPHEADER =>array('Accept: application/json','Content-Type: application/json'),
                CURLOPT_URL => "$url",
                CURLOPT_SSL_VERIFYPEER=>false,
                CURLOPT_SSL_VERIFYHOST=>0,
                CURLOPT_POSTFIELDS=>$xml_data,
                CURLOPT_CONNECTTIMEOUT=>0,
                CURLOPT_TIMEOUT=>12
            ));
            
            $resp = curl_exec($curl);
            $httpCode = curl_getinfo($curl, CURLINFO_HTTP_CODE);

            if (FALSE === $resp)
            {
               throw new Exception(curl_error($curl), curl_errno($curl));
            }
            curl_close($curl);
        }
        catch(Exception $e) {
        //trigger_error(sprintf('Curl failed with error #%d: %s',   $e->getCode(), $e->getMessage()),E_USER_ERROR);

        }
        //4. Parse the Server Response
        $resp=trim($resp);
        $response='';
        if($httpCode==200)
        {
            //5.Interprets a string of XML into an object 
            header("Content-type: application/json");   
            $this->log_event("RESPONSE-->",$resp);        
            $json=json_encode($resp);
            $response='00';
        }
        else{
            $response='01';
        }
        //return $response;
    }

    public function resolve_mobile_number($mobile){
              $first_char=substr($mobile, 0,1);
              $country_code='255';
              
              if($first_char=='0')                          //number in format 0754 {123 456} 
              {
              
                $prefix=substr($mobile, 1,2);
                $identifier=substr($mobile,3,strlen($mobile));          
              
              }
             
              elseif(substr($mobile,0,3)=='255')   //number in format 255 754 123 456
              {
              
                $prefix=substr($mobile, 3,2);
                $identifier=substr($mobile,5,strlen($mobile));
                
              }
              else
              {
                $prefix=null;
                $identifier=null;
              }           
              
             return  "$country_code$prefix$identifier";
    }
	public function get_bus_details($bus){
		$sql='SELECT CONCAT(`plate_number`, ", ",company_name) 
				FROM `tbl_bus_details` 
				INNER JOIN tbl_bus_companies ON tbl_bus_details.company_id=tbl_bus_companies.company_id 
				WHERE `bus_id`=:bus';
		try{
            $sql=$this->db->prepare($sql);
			$sql->bindParam(':bus',$bus);
            $sql->execute();
            $result=$sql->fetch();
		}catch(PDOException $e){
			echo $e->getMessage();        
		}
		return $result;
	}
	public function get_passenger_details($book){
		$sql="SELECT CONCAT_WS(' ',`first_name`,`last_name`) FROM `tbl_customer_details` 
				WHERE `booking_id`=:bookid
			ORDER BY `ID` ASC LIMIT 1";
		try{
            $sql=$this->db->prepare($sql);
			$sql->bindParam(':bookid',$book);
            $sql->execute();
            $result=$sql->fetch();
		}catch(PDOException $e){
			echo $e->getMessage();        
		}
		return $result;
	}

    public function log_event($msgType,$text){
           $now = new DateTime();
           $now12 = new DateTime();        
           $now=$now->format('Ymd His'); 
           $today=$now12->format('Y-m-d');
               
           $logs= 'logsx-'.$today.'.txt';
          
           $file = '../logs/'.$logs.'';
          
            //Open the file to get existing content
            if(file_exists($file)){
                $current = file_get_contents($file);
                if (strpos($msgType, 'Res') === false) {
                    $current .= "\n$now-$msgType-$text";
              }else{
                    $current .= "\n$now-$msgType-$text\n";
                }
            
                
            }else {
                $current = "\n$now-$msgType-$text";
            }
            
            // Write the contents back to the file
            file_put_contents($file, $current);
    }
 
  	public function get_details_booking($reference, $amount){
		$sql="SELECT `book_ID`, `payment_ref`, `amount`,tbl_booking_details.`bus_id`, tbl_booking_details.`company_id`,`account_number`, 
						`account_name`, `bank_code`,tbl_booking_details.phone_number,tbl_booking_details.email,
						CONCAT(bus_name,' : ',plate_number),company_name,seat_no,terminal_date,company_institution_id
				FROM `tbl_booking_details` 
				INNER JOIN tbl_bus_companies ON tbl_booking_details.company_id=tbl_bus_companies.company_id 
				INNER JOIN tbl_bank_lists ON tbl_bus_companies.bank_id=tbl_bank_lists.bank_id 
				INNER JOIN tbl_bus_details ON tbl_booking_details.bus_id=tbl_bus_details.bus_id
				WHERE `payment_ref`=:ref AND `amount`=:amount AND `pay_status`!='P'";
		$parameters = array(
								':ref'=>$reference,
								':amount'=>$amount
							);	
		$result = $this->fetch_query_result($sql,$parameters,false);

		return $result;
	}

	public function post_transaction($payment_ref,$total,$phone,$source,$channel,$bus_id,$company_id, $book_id, $datet,$request){
		$status=false;
		$datet = date('Y-m-d H:i:s', strtotime($datet));
		$agg=$this->get_nidc_commission($total,'aggr');
		$latra = $total*0.005;
		$nidc = $this->get_nidc_commission($total,'nidc');	
		$sql="INSERT INTO `tbl_transactions`
						(
							`transaction_amount`, 
							`transaction_datetime`, 
							`reference_number`, 
							`request_id`, 
							`channel_id`, 
							`mobile_number`, 
							`source`, 
							`booking_id`, 
							`bus_id`, 
							`company_id`, 
							`transaction_status`,
							agg_commision,
							nidc_commision,
							latra_commission
						)
				VALUES
						(
							:amount,
							:datet,
							:reference,
							:request,
							:channel,
							:mobile,
							:source,
							:book,
							:bus,
							:company,
							'00',
							:agg,
							:nidc,
							:latra
						)";

		$parameters=array(
							
							':amount' =>$total,
							':datet' => $datet,
							':reference' =>$payment_ref,
							':request'=>$request,
							':channel'=>$channel,
							':mobile'  =>$phone,
							':source' =>$source,
							':book'=>$book_id,
							':bus'=>$bus_id,
							':company'=>$company_id,
							':agg'=>$agg,
							':nidc'=>$nidc,
							':latra'=>$latra
						);
									
		if($this->execute_query($sql,$parameters)){
			$status=true;
		}

		return $status;
	}
	public function update_failed_transaction($book,$ref,$bankMsg){
		$sql="UPDATE `tbl_transactions` 
				SET `transaction_status`='01',`message`=:msg
				 WHERE `booking_id`=:book AND `reference_number`=:ref";
		$parameters = array(
							':book'=>$book,
							':ref'=>$ref,
							':msg'=>$bankMsg['responseDescription']
						);
		$this->execute_query($sql,$parameters);
	}

	public function get_nidc_commission($amont,$id){
		if($id=='nidc'){
		$sql="SELECT `nidc` FROM `tbl_commission_per_transctions` 
				WHERE `fare_from`<=:amount AND `fare_to`>=:amount";
		}else if($id=='aggr'){
			$sql="SELECT `aggregator` FROM `tbl_commission_per_transctions` 
				WHERE `fare_from`<=:amount AND `fare_to`>=:amount";
		}else if($id=='latra'){
			$sql="SELECT `latra` FROM `tbl_commission_per_transctions` 
				WHERE `fare_from`<=:amount AND `fare_to`>=:amount";
		}
		$parameters=array(
							':amount'=>$amont
						);
		$result = $this->fetch_query_result($sql,$parameters,FALSE);
		return $result[0];
	}
	
	public function update_booking_details($book,$ref,$bk,$pay){
		$sql="UPDATE `tbl_booking_details` 
				SET `book`=:bk,`pay_status`=:pay
				 WHERE `book_ID`=:book AND `payment_ref`=:ref";
		$parameters = array(
							':book'=>$book,
							':ref'=>$ref,
							':bk'=>$bk,
							':pay'=>$pay							
						);
		$this->execute_query($sql,$parameters);
	}
	public function post_trnx_bank($details){	
		$amountC = $this->get_company_owner_amount($details[0]);
		//1. Prepare the connectivity parameters.
        $url="http://41.188.142.126:7070/UmojaMobileTransactions/services/UmojaMobileService/tiketiBankPayment";
        //2.Prepare the xml data
		$snip='{
					"accountNo":"01299830001",
					"bankID":"'.$details[5].'",
					"bankName":"BankM",
					"busOwnerName":"'.$details[6].'",
					"referenceNo":"'.$details[1].'",
					"ticketAmount":"'.number_format($amountC,2,'.','').'",
					"status":"Verified",
					"code":"200"
				}';
		$jsondata = str_replace("\n", '', $snip); 
        //3.Send the Request to the API
        try
        {
            $curl = curl_init();
            curl_setopt_array($curl, array(
                CURLOPT_POST=>1,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_HTTPHEADER =>array('Accept: application/json','Content-Type: application/json'),
                CURLOPT_URL => "$url",
                CURLOPT_SSL_VERIFYPEER=>false,
                CURLOPT_SSL_VERIFYHOST=>0,
                CURLOPT_POSTFIELDS=>$jsondata,
                CURLOPT_CONNECTTIMEOUT=>0,
                CURLOPT_TIMEOUT=>12
            ));            
            $resp = curl_exec($curl);
            $httpCode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
            if (FALSE === $resp)
            {
               throw new Exception(curl_error($curl), curl_errno($curl));
            }
            curl_close($curl);
        }
        catch(Exception $e) {
        //trigger_error(sprintf('Curl failed with error #%d: %s',   $e->getCode(), $e->getMessage()),E_USER_ERROR);

        }
        //4. Parse the Server Response
        $resp=trim($resp);
        $response='';
        if($httpCode==200)
        {
			//5.Interprets a string of XML into an object 
			$response = json_decode($resp,TRUE);
        }
        else{
            $response='01';
        }		
		return $response;
	}
	public function get_company_owner_amount($book){
		$sql='SELECT `transaction_amount`-(`agg_commision`+`nidc_commision`+`latra_commission`) FROM `tbl_transactions` WHERE booking_id=:book';
		$parameters = array(
								':book'=>$book
							);	
		$result = $this->fetch_query_result($sql,$parameters,false);

		return $result[0];
	}
	
	
}

Class CRUD{
    public function generate_json_response($response){
          $outputArr = array();
          $jsonData[] = $response;
          $outputArr['Response'] = $jsonData;
          print_r(json_encode($outputArr));
    }
    public function execute_query($sql,$parameters){
        $status=FALSE;
        try
          {
             $sql = $this->db->prepare($sql);
             if($sql->execute($parameters))
             {
                $num_rows=$sql->rowCount(); 
                if($num_rows>0)
                {
                    $_SESSION['message_code']='SS';
                    $status=TRUE;
                }
                else
                {
                    $_SESSION['message_code']='NR';
                }
             }
            
            
          }
        catch (PDOException $e)
        {
            $this->messageCode=$e->errorInfo[1];
            
            if(DEBUG_MOD)
            {
               //echo $e->getMessage();
            }
        }
        return $status;
    }
    public function fetch_query_result($sql,$parameters,$isloop){
          $status=FALSE;
          $result=null;
            try
                  {
                     $sql = $this->db->prepare($sql);
                      if($parameters!=NULL)
                      {
                        $sql->execute($parameters);
                      }
                      else
                      {
                        $sql->execute();
                      }
                     if($isloop==true)
                     {
                        $result=$sql->fetchAll();
                     }
                     else $result=$sql->fetch();
                    
                  }
                catch (PDOException $e)
                {
                    $_SESSION['message_code']='FE';
                    if(DEBUG_MOD)
                    {
                        echo $e->getMessage();
                        echo '<br/><br/>';
                        //var_dump($e);
                    }
                }
            
            return $result;
    }
    public function get_money_value($value,$isMinor){
            if($isMinor)
            {
                $value=substr($value, 0, -2);
            }
        
            if(is_numeric($value)){
                return number_format($value,2,'.',',');
            }
            else return '-';
    }
}

