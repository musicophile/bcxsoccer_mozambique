package com.cfm.football.clubs.matcheticketing.utilities;

import android.annotation.SuppressLint;

import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 * Created by jamesb on 2017/03/02.
 */

public class Barcode {

    String MTI, field_3, field_61,field_7,field_11, field_68, field_69,  barcode,idNo,phoneNo;

    public String getIdNo() {
        return idNo;
    }

    public void setIdNo(String idNo) {
        this.idNo = idNo;
    }

    public String getPhoneNo() {
        return phoneNo;
    }

    public void setPhoneNo(String phoneNo) {
        this.phoneNo = phoneNo;
    }

    public  Barcode(){

    }

    public Barcode(String MTI, String field_3, String field_61, String barcode) {
        this.MTI = MTI;
        this.field_3 = field_3;
        this.field_61 = field_61;
        this.barcode = barcode;
    }

    @SuppressLint("SimpleDateFormat")
    public void set_transmission_datetime() {
        Calendar cal = Calendar.getInstance();
        cal.getTime();
        SimpleDateFormat sdf = new SimpleDateFormat("yyMMddHHmmss");
        String _datetime = sdf.format(cal.getTime()).toString();
        this.field_7=_datetime;
    }

    public String get_transmission_datetime() {
        return field_7;
    }

    public void set_stan(String _STAN) {
        this.field_11 = _STAN;//field_7.substring(6)
    }

    public void set_stan() {
        this.field_11 = field_7.substring(6);
    }

    public String get_stan() {
        return field_11;
    }

    public void set_field_68(String value){
        this.field_68=value;
    }
    public void set_field_69(String value){
        this.field_69=value;
    }

    public String get_field_68() {
        return field_68;
    }
    public String get_field_69() {
        return field_69;
    }

    public String getMTI() {
        return MTI;
    }


    public void setMTI(String MTI) {
        this.MTI = MTI;
    }

    public String getField_3() {
        return field_3;
    }

    public void setField_3(String field_3) {
        this.field_3 = field_3;
    }

    public String getField_61() {
        return field_61;
    }

    public void setField_61(String field_61) {
        this.field_61 = field_61;
    }

    public String getBarcode() {
        return barcode;
    }

    public void setBarcode(String barcode) {
        this.barcode = barcode;
    }
}
