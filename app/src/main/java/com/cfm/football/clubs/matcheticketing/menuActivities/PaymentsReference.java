package com.cfm.football.clubs.matcheticketing.menuActivities;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.cfm.football.clubs.matcheticketing.R;
import com.cfm.football.clubs.matcheticketing.utilities.Action;
import com.cfm.football.clubs.matcheticketing.utilities.JsonArrayRequestAlt;
import com.cfm.football.clubs.matcheticketing.utilities.Route;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

public class PaymentsReference extends AppCompatActivity {

    //Android Variables
    private PaymentsAdapter adapter;
    private RecyclerView.LayoutManager layoutManager;
    private RecyclerView recyclerView;
    private ArrayList<PaymentData> items;
    private ProgressDialog mProgressDialog;

    private SharedPreferences pref;
    private String bus_id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Action.DefaultLanguage(PaymentsReference.this, Action.language);
        setContentView(R.layout.activity_payments_reference);

        pref = getApplicationContext().getSharedPreferences("UhuruPayPref", 0);
        bus_id = pref.getString("busID", "");

        String phone_number;
        if (savedInstanceState == null) {
            Bundle extras = getIntent().getExtras();
            if (extras == null) {
                phone_number = null;
            } else {
                phone_number = extras.getString("phone_number");
            }
        } else {
            phone_number = (String) savedInstanceState.getSerializable("phone_number");
        }

        mProgressDialog = new ProgressDialog(PaymentsReference.this);

        loading_data();

        recyclerView =  findViewById(R.id.my_recycler_payment);
        recyclerView.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(PaymentsReference.this);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        items = new ArrayList<>();

        InquireAvailableReferences(phone_number);


    }

    /*
    Loading progress dialog
     */
    private void loading_data() {
        mProgressDialog.setIndeterminate(true);
        mProgressDialog.setMessage(getResources().getString(R.string.loader_text));
        mProgressDialog.setCancelable(false);
        mProgressDialog.show();
    }


    /*
    Sending request to check if any paid tickets for the given phone number
     */
    private void InquireAvailableReferences(String phone_number) {
        RequestQueue queue = Action.getInstance(getApplicationContext()).getRequestQueue();
        Route route = new Route();
        route.setRoute_code("008");
        route.setPhone_number(phone_number);
        route.setBus_id(bus_id);
        String ENDPOINT = Action.getEndpoint("eticketing.php");
        JSONObject reqMsg = Action.getRoutes(route);
        JSONArray requestArray = new JSONArray();
        requestArray.put(reqMsg);
        Log.v("REFERENCE", "" + reqMsg);
        JsonArrayRequestAlt myReq = new JsonArrayRequestAlt(Request.Method.POST, ENDPOINT, reqMsg, createMyReqSuccessListenerReferencesInquiry(), createMyReqErrorListener());
        myReq.setRetryPolicy(new DefaultRetryPolicy((int) TimeUnit.SECONDS.toMillis(5), 0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        queue.add(myReq);
    }


    /*
    Success listener
     */

    private Response.Listener<JSONArray> createMyReqSuccessListenerReferencesInquiry() {
        return new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(final JSONArray response) {

                Log.v("RESPONSE-RETURNED", "" + response);

                if (response.length() == 0) {
                    mProgressDialog.dismiss();
                    Toast.makeText(PaymentsReference.this, R.string.no_reference, Toast.LENGTH_SHORT).show();
                    PaymentsReference.this.finish();
                } else {
                    for (int i = 0; i < response.length(); i++) {
                        try {
                            JSONObject jsonobject = response.getJSONObject(i);

                            items.add(new PaymentData(jsonobject.getString("payment_ref"), jsonobject.getString("amount"), jsonobject.getString("terminal_date")));
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    adapter = new PaymentsAdapter(items, PaymentsReference.this);
                    recyclerView.setAdapter(adapter);

                    adapter.setClickListener(new PaymentsAdapter.ClickListener() {
                        @Override
                        public void itemClicked(int position) {
                            printingPaidTicket(items.get(position).getReference_number());
                        }
                    });
                    mProgressDialog.dismiss();
                }


            }
        };
    }


    private void printingPaidTicket(String bookingRef) {

        try{
            loading_data();
            RequestQueue queue = Action.getInstance(getApplicationContext()).getRequestQueue();
            JSONObject jsonObject = new JSONObject();
            jsonObject.accumulate("route_code","012");
            jsonObject.accumulate("booking_ref",bookingRef);

            Log.v("XXXXXX",jsonObject+"");

            String ENDPOINT = Action.getEndpoint("eticketing.php");
            JsonArrayRequestAlt myReq = new JsonArrayRequestAlt(Request.Method.POST, ENDPOINT, jsonObject, new Response.Listener<JSONArray>() {
                @Override
                public void onResponse(JSONArray jsonArray) {
                    Log.v("PAID_Ticket",""+jsonArray);
                    mProgressDialog.dismiss();
//                    PrinterHelper.getInstance(PaymentsReference.this).printKondakta(PaymentsReference.this, mIzkcService, jsonArray);
                }
            }, createMyReqErrorListener());
            myReq.setRetryPolicy(new DefaultRetryPolicy((int) TimeUnit.SECONDS.toMillis(20), 0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

            queue.add(myReq);


        }catch(JSONException e)
        {
            e.printStackTrace();
        }
    }


    /**************
     *Getting error response after submission
     *************/
    private Response.ErrorListener createMyReqErrorListener() {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError ex) {
                mProgressDialog.dismiss();
                new AlertDialog.Builder(PaymentsReference.this)
                        .setTitle(R.string.h_error_details)
                        .setMessage(R.string.connection_error)
                        .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int whichButton) {
                                dialog.dismiss();
                                PaymentsReference.this.finish();
                            }
                        }).show();
            }
        };
    }

    @Override
    protected void onPostResume() {
        super.onPostResume();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

    }
}
