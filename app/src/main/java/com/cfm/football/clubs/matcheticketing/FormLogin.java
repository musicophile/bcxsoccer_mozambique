package com.cfm.football.clubs.matcheticketing;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.cfm.football.clubs.matcheticketing.menuActivities.MainMDC;
import com.cfm.football.clubs.matcheticketing.utilities.Action;
import com.cfm.football.clubs.matcheticketing.utilities.Hash;
import com.cfm.football.clubs.matcheticketing.utilities.Msg;
import com.cfm.football.clubs.matcheticketing.utilities.SqliteHelper;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class FormLogin extends AppCompatActivity implements View.OnClickListener {


    private EditText username;
    private EditText password;
    private Button login;
    private ProgressDialog mProgressDialog;
    private SharedPreferences pref;
    private static final int PERMISSION_REQUEST_CODE = 1;
    private TextView password_reset;

    private List<String> seatLabel;

    private SqliteHelper db;

    // 2.Java Variables
    String checksum;
    private String usernameVar;
    private String passwordVar;
    private String simSerial;
    private String deviceID;
    private String deviceType;
    private String route_id;
    private String fare;
    private String stop_to_id;
    private String stop_from_id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (!checkPermission()) {
            ActivityCompat.requestPermissions(FormLogin.this,
                    new String[]{Manifest.permission.READ_PHONE_STATE},
                    PERMISSION_REQUEST_CODE);
            Toast.makeText(this, R.string.accept_permission, Toast.LENGTH_SHORT).show();
            return;
        }
        setContentView(R.layout.activity_form_login);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        db = new SqliteHelper(this);
        pref = getApplicationContext().getSharedPreferences("UhuruPayPref", 0);

        seatLabel = new ArrayList<>();
        mProgressDialog = new ProgressDialog(this);

        username = findViewById(R.id.username);
        password = findViewById(R.id.password);
        login = findViewById(R.id.login_button);
        // password_reset = (TextView) findViewById(R.id.reset);

        login.setOnClickListener(this);

        //Get Telephony details
        TelephonyManager tm = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);


        deviceID = tm.getDeviceId();
        simSerial =  tm.getDeviceId();

        if (pref.getString("operatorID", null) != null){
            username.setVisibility(View.VISIBLE);
        }


    }

    @Override
    public void onBackPressed() {

        FormLogin.this.finish();
    }

    private boolean checkPermission() {
        int result = ContextCompat.checkSelfPermission(FormLogin.this, Manifest.permission.READ_PHONE_STATE);
        return result == PackageManager.PERMISSION_GRANTED;
    }

    public void onClick(View view) {
        usernameVar = username.getText().toString();
        passwordVar = password.getText().toString();
        postRequest();
    }
//metodo para fazer login
    private void postRequest() {
        mProgressDialog.setIndeterminate(true);
        mProgressDialog.setMessage(getString(R.string.loading));
        mProgressDialog.setCancelable(false);
        mProgressDialog.show();

        RequestQueue queue =  Action.getInstance(this).getRequestQueue();
        Msg msg=new Msg();

        msg.set_transmission_datetime();
        msg.set_business_name(usernameVar);
        msg.set_PIN_data(passwordVar);
        msg.set_field_68(deviceID);
        msg.setRoute_code("007");//codego para requizitar permissao para logar
        String ENDPOINT=Action.getEndpoint("index_api.php");
        JSONObject reqMsg=Action.getJSONObject(msg);
        Log.v("FIRST_POST",reqMsg.toString());
        JsonObjectRequest myReq = new JsonObjectRequest(Request.Method.POST, ENDPOINT, reqMsg, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                mProgressDialog.dismiss();
                Log.v("RESPONSE-MSG",response+"");

                String rspCode=Action.getFieldValue(response,"field_39");
                if(rspCode.equals("00"))
                {
                    String authToken=usernameVar+passwordVar+deviceID+simSerial;
                    authToken= Hash.createHash(authToken);
                    setUserPass(passwordVar);
                    setOperator(username.getText().toString());
                    setOperatorID(Action.getFieldValue(response,"operator_id"));
                    setDeviceId(deviceID);
                    setAuthToken(authToken);
                    if (Action.getFieldValue(response,"operator_type").equals("2")){
                        setEventFare(Action.getFieldValue(response, "price"));
                        setTotalCapacity(Action.getFieldValue(response, "capacity"));
                        setEventName(Action.getFieldValue(response, "event_name"));
                        setClubName(Action.getFieldValue(response, "club_name"));
                        setOppTeam(Action.getFieldValue(response, "opp_team"));
                        setEventDate(Action.getFieldValue(response, "event_date"));
                        Log.v("SIKU",""+Action.getFieldValue(response, "event_date"));
                        setEventTime(Action.getFieldValue(response, "kickoff_time"));
                        setLocation(Action.getFieldValue(response, "location"));

                        db.deleteRow();
                        db.addUser(usernameVar,passwordVar);
                    }
                    setOperatorType(Action.getFieldValue(response, "operator_type"));
                    Intent intent = new Intent(FormLogin.this, MainMDC.class);
                    startActivity(intent);
                    finish();


                } else {
                    Action.resolveRspCode(rspCode);
                    new AlertDialog.Builder(FormLogin.this)
                            .setTitle(R.string.h_login_failure)
                            .setMessage(getString(R.string.incorrect_user)+rspCode)
                            .setPositiveButton(android.R.string.yes, null).show();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError ex) {
                mProgressDialog.dismiss();
                String message=Action.parseErrorResponse(ex);
                String userName = username.getText().toString().trim();
                String pwd = password.getText().toString().trim();

                boolean res = db.checkUser(userName, pwd);
                if (res){
                    Toast.makeText(FormLogin.this, getString(R.string.successfull_login), Toast.LENGTH_SHORT).show();
                    Intent intent = new Intent(FormLogin.this, MainMDC.class);
                    startActivity(intent);
                }else{
                    new AlertDialog.Builder(FormLogin.this)// alerta de erro de internet
                            .setTitle(R.string.h_error_details)
                            .setMessage(message + "\n\n" + getString(R.string.network_error))
                            .setNegativeButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    finish();
                                }
                            })
                            .show();
                }
            }
        });
        myReq.setRetryPolicy(new DefaultRetryPolicy((int) TimeUnit.SECONDS.toMillis(20), 0, 1f));
        queue.add(myReq);
    }




    public boolean setEventFare(String name){
        SharedPreferences.Editor editor = pref.edit();
        editor.putString("mEventFare", name);
        editor.commit(); //commit changes
        return true;
    }

    public boolean setOppTeam(String name){
        SharedPreferences.Editor editor = pref.edit();
        editor.putString("oppTeam", name);
        editor.commit(); //commit changes
        return true;
    }

    public boolean setRouteTo(String name){
        SharedPreferences.Editor editor = pref.edit();
        editor.putString("busRouteTo", name);
        editor.commit(); //commit changes
        return true;
    }

    public boolean setUserPass(String name){
        SharedPreferences.Editor editor = pref.edit();
        editor.putString("userPassword", name);
        editor.commit(); //commit changes
        return true;
    }

    public boolean setOperatorType(String name){
        SharedPreferences.Editor editor = pref.edit();
        editor.putString("operatorType", name);
        editor.commit(); //commit changes
        return true;
    }

    public boolean setClubName(String name){
        SharedPreferences.Editor editor = pref.edit();
        editor.putString("clubName", name);
        editor.commit(); //commit changes
        return true;
    }


    public boolean setLocation(String name){
        SharedPreferences.Editor editor = pref.edit();
        editor.putString("location", name);
        editor.commit(); //commit changes
        return true;
    }

    public boolean setTotalCapacity(String name){
        SharedPreferences.Editor editor = pref.edit();
        editor.putString("totalCapacity", name);
        editor.commit(); //commit changes
        return true;
    }

    public boolean setEventDate(String name){
        SharedPreferences.Editor editor = pref.edit();
        editor.putString("eventDate", name);
        editor.commit(); //commit changes
        return true;
    }

    public boolean setEventTime(String name){
        SharedPreferences.Editor editor = pref.edit();
        editor.putString("eventTime", name);
        editor.commit(); //commit changes
        return true;
    }

    public boolean setPlateNumber(String number){
        SharedPreferences.Editor editor = pref.edit();
        editor.putString("nPlateNo", number);
        editor.commit(); //commit changes
        return true;
    }

    public boolean setEventName(String name){
        SharedPreferences.Editor editor = pref.edit();
        editor.putString("nEventName", name);
        editor.commit(); //commit changes
        return true;
    }

    public boolean setOperator(String operator){
        SharedPreferences.Editor editor = pref.edit();
        editor.putString("operator", operator);
        editor.commit(); //commit changes
        return true;
    }

    public boolean setOperatorID(String operator){
        SharedPreferences.Editor editor = pref.edit();
        editor.putString("operatorID", operator);
        editor.commit(); //commit changes
        return true;
    }

    public boolean setBusID(String busID){
        SharedPreferences.Editor editor = pref.edit();
        editor.putString("busID", busID);
        editor.commit(); //commit changes
        return true;
    }

    public boolean setDeviceId(String operator){
        SharedPreferences.Editor editor = pref.edit();
        editor.putString("deviceID", operator);
        editor.commit(); //commit changes
        return true;
    }

    public boolean setAuthToken(String authToken){
        SharedPreferences.Editor editor = pref.edit();
        editor.putString("AuthToken", authToken);
        editor.commit(); //commit changes
        return true;
    }
}


