package com.cfm.football.clubs.matcheticketing;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.annotation.SuppressLint;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.device.PiccManager;
import android.device.PrinterManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.cfm.football.clubs.matcheticketing.utilities.Action;
import com.cfm.football.clubs.matcheticketing.utilities.SqliteHelper;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

public class NfcActivity extends AppCompatActivity {

    private static final String TAG = "PiccCheck";
    private static final int MSG_FOUND_UID = 12;
    private PiccManager piccReader;
    private Handler handler;
    private ExecutorService exec;
    int scan_card = -1;
    int SNLen = -1;
    private Timer mTimer;
    private SharedPreferences pref;
    private String deviceID,operator_id;

    private String eventName,clubName,eventLocation;
    private String oppTeam,eventPrice,capacity,eventTime,eventDate;
    private PrinterManager printer;

    SqliteHelper db;
    private int counter2,counter;
    @SuppressLint("HandlerLeak")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_nfc);
        Toolbar toolbar =  findViewById(R.id.app_bar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(R.string.Verify_Member);
        getSupportActionBar().setDisplayShowTitleEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        exec = Executors.newSingleThreadExecutor();
        piccReader = new PiccManager();
        printer = new PrinterManager();

        pref = getApplicationContext().getSharedPreferences("UhuruPayPref", 0);
        db = new SqliteHelper(this);
        operator_id = pref.getString("operatorID", "");
        clubName = pref.getString("clubName", "");
        eventName = pref.getString("nEventName", "");
        capacity = pref.getString("totalCapacity", "");
        oppTeam = pref.getString("oppTeam", "");
        eventPrice = pref.getString("mEventFare", "");
        eventTime = pref.getString("eventTime", "");
        eventDate = pref.getString("eventDate", "");
        eventLocation = pref.getString("location", "");


        initializer();

        mTimer  =new Timer();
        handler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                // TODO Auto-generated method stub
                switch (msg.what) {

                    case MSG_FOUND_UID:
                        String uid = (String) msg.obj;
                        CheckMember(uid);
                        break;
                    default:
                        break;
                }
                super.handleMessage(msg);
            }
        };

    }

    private void initializer() {
        exec.execute(new Thread(new Runnable() {
            @Override
            public void run() {
                // TODO Auto-generated method stub
                piccReader.open();
            }
        }, "picc open"));
    }

    @Override
    protected void onResume() {
        mTimer.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                exec.execute(new Thread(new Runnable() {

                    @Override
                    public void run() {
                        // TODO Auto-generated method stub
                        byte CardType[] = new byte[2];
                        byte Atq[] = new byte[14];
                        char SAK = 1;
                        byte sak[] = new byte[1];
                        sak[0] = (byte) SAK;
                        byte SN[] = new byte[10];
                        scan_card = piccReader.request(CardType, Atq);
                        scan_card = piccReader.request_type((byte)0x07, CardType, Atq);
                        if(scan_card > 0) {
                            SNLen = piccReader.antisel(SN, sak);
                            Log.d(TAG, "SNLen = " + SNLen);
                            Message msg = handler.obtainMessage(MSG_FOUND_UID);
                            msg.obj = bytesToHexString(SN, SNLen);
                            handler.sendMessage(msg);

                        }

                    }
                }, "picc check"));
            }
        }, 0, 1000);
        super.onResume();
    }

    @Override
    protected void onDestroy() {
        // TODO Auto-generated method stub
        mTimer.cancel();
        exec.shutdown();
        super.onDestroy();
    }

    public static String bytesToHexString(byte[] src, int len) {
        StringBuilder stringBuilder = new StringBuilder("");
        if (src == null || src.length <= 0) {
            return null;
        }
        if (len <= 0) {
            return "";
        }
        for (int i = 0; i < len; i++) {
            int v = src[i] & 0xFF;
            String hv = Integer.toHexString(v);
            if (hv.length() < 2) {
                stringBuilder.append(0);
            }
            stringBuilder.append(hv);
        }
        return stringBuilder.toString();
    }

    @Override
    protected void onPause() {
        // TODO Auto-generated method stub
        super.onPause();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // TODO Auto-generated method stub
        PackageManager pk = getPackageManager();
        PackageInfo pi;
        try {
            pi = pk.getPackageInfo(getPackageName(), 0);
            Toast.makeText(this, "V" +pi.versionName , Toast.LENGTH_SHORT).show();
        } catch (PackageManager.NameNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return super.onOptionsItemSelected(item);
    }

    private void CheckMember(String msg) {

        try {
            Log.v("MSG_CAPTURED","CARD: "+msg);
            RequestQueue queue = Action.getInstance(getApplicationContext()).getRequestQueue();
            JSONObject reqMsg = new JSONObject();
            reqMsg.accumulate("route_code", "003");
            reqMsg.accumulate("card_id", msg);
//            reqMsg.accumulate("card_no", card_no);
            reqMsg.accumulate("operator_id", operator_id);
            String ENDPOINT = Action.getEndpoint("index_api.php");

            JSONArray requestArray = new JSONArray();
            requestArray.put(reqMsg);
            Log.v("SEAT_MSG", "" + reqMsg);
            JsonObjectRequest myReq = new JsonObjectRequest(Request.Method.POST, ENDPOINT, reqMsg, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    Log.v("CARD_MSG", "" + response);
                    String responseCode = Action.getFieldValue(response, "field_39");

                    String card_no = Action.getFieldValue(response, "card_no");
                    String first_name = Action.getFieldValue(response, "first_name");
                    String last_name = Action.getFieldValue(response, "last_name");
                    String status = Action.getFieldValue(response, "status");

                    if(status.equalsIgnoreCase("Paid")){

                        trxPopStatus(getString(R.string.valid),card_no, first_name,last_name);

                    }else if(status.equalsIgnoreCase("Not Paid")){

                        trxPopError("Not Paid");
                    }else if (status.equalsIgnoreCase("inactive")){
                        trxPopError("Inactive OR Invalid");
                    }

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError ex) {

                    String message = Action.parseErrorResponse(ex);

                    new AlertDialog.Builder(NfcActivity.this)
                            .setTitle(R.string.h_error_details)
                            .setMessage(R.string.connection_error)
                            .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int whichButton) {
                                    dialog.dismiss();
                                 onBackPressed();
                                }
                            }).show();
                }
            });
            myReq.setRetryPolicy(new DefaultRetryPolicy((int) TimeUnit.SECONDS.toMillis(5), 0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            queue.add(myReq);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
    public void trxPopStatus(String a, final String card_no, String first_name, String last_name)
    {
        ImageView image = new ImageView(this);
        image.setImageResource(R.drawable.accept);
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(getString(R.string.payment))
                .setMessage(Action.getstringResource(getApplicationContext(), R.string.status,"",false)+
                        "\n" +  "First name"+": " + first_name +
                        "\n" + "Last name"+": " + last_name +
                        "\n" + "Card number"+": " + card_no +
                        "\n" + "status"+": " + a
                )
                .setView(image)
                .setCancelable(false)
                .setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        Cursor res = db.getTickets();
                        if(res.getCount() == 0){
                            counter=0;
                            counter2 = Integer.parseInt(capacity);
                                    counter += 1;
                                    counter2 -= 1;
                                    if (counter2 == -1){
                                        new AlertDialog.Builder(NfcActivity.this)
                                                .setTitle(getString(R.string.out_of_tickets_header))
                                                .setMessage(getString(R.string.out_of_ticket_message))
                                                .setPositiveButton(android.R.string.ok, null).show();
                                    }else{

                                        printer.clearPage();
                                        doprintwork(card_no);
                                        db.clearTickets();
                                        db.addTickets(String.valueOf(counter),String.valueOf(counter2));
                                    }
                        }else{

                            while (res.moveToNext()){
                                counter2 = Integer.parseInt(res.getString(2));
                                counter = Integer.parseInt(res.getString(1));

                                        counter += 1;
                                        counter2 -= 1;
                                        if (counter2 == -1){
                                            new AlertDialog.Builder(NfcActivity.this)
                                                    .setTitle(getString(R.string.out_of_tickets_header))
                                                    .setMessage(getString(R.string.out_of_ticket_message))
                                                    .setPositiveButton(android.R.string.ok, null).show();
                                        }else{

                                            printer.clearPage();
                                            doprintwork(card_no);
                                            db.clearTickets();
                                            db.addTickets(String.valueOf(counter),String.valueOf(counter2));
                                        }

                            }
                        }

                        dialog.cancel();
                        onBackPressed();
                    }
                })
                .setView(image);
        AlertDialog alert = builder.create();
        alert.show();
    }

    public void trxPopError(String a){
        ImageView image = new ImageView(this);
        image.setImageResource(R.drawable.deny);
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(getString(R.string.payment))
                .setMessage(
                        "\n\n" + Action.getstringResource(getApplicationContext(), R.string.info,"",false)+": " + a
                )
                .setCancelable(false)
                .setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                       onBackPressed();
                    }
                })
                .setView(image);
        AlertDialog alert = builder.create();
        alert.show();
    }

    private void doprintwork(String card_no) {

        String ref = System.currentTimeMillis()/1000+"";

        Bitmap bitmap = Action.CreateCodeBar(ref, 384, 80);
        Log.v("TOTAL_PRICE", "KIASI: " + ref);
        String str = null;
        str = "";
        str += "       " + eventName;
        str += "\n" + "------------------------------";
        str += "\n" + " "+(posFormatPrintLine(getString(R.string.event_location), eventLocation));
        str += "\n" + " "+posFormatPrintLine(getString(R.string.event_date), eventDate);
        str += "\n" + " "+posFormatPrintLine(getString(R.string.event_time),eventTime);
        str += "\n" + " "+posFormatPrintLine(getString(R.string.event_price),(eventPrice));
        str += "\n" + "------------------------------" + "\n";
        str += "\n" + posFormatPrintLineCenter(clubName) + "\n";
        str += "\n" + posFormatPrintLineCenter(getString(R.string.versus)) + "\n";
        str += "\n" + posFormatPrintLineCenter(oppTeam) + "\n";
        str += "\n" + "------------------------------" + "\n";
        str += "\n" + "  " + "\n";

//        Bitmap xx = busName(str);
        printer.setupPage(384, 750);
        BitmapFactory.Options opts = new BitmapFactory.Options();
        opts.inPreferredConfig = Bitmap.Config.ARGB_8888;
        opts.inDensity = getResources().getDisplayMetrics().densityDpi;
        opts.inTargetDensity = getResources().getDisplayMetrics().densityDpi;
        Bitmap img = BitmapFactory.decodeResource(getResources(), R.drawable.cfm_logo, opts);
        int header = printer.drawBitmap(img, 50, -1);
        int btm = printer.drawBitmap(bitmap, 20, 470);
//        printer.drawBitmap(xx, 0, -1);
        printer.drawTextEx(str, 5, 100, 400, -1, "arial", 25, 0, 0, 0);
        printer.drawTextEx(posFormatPrintLineCenter("POWERED BY BCX"), 50, 555, 400, -1, "arial", 20, 0, 0, 0);
        printer.prn_paperForWard(0);
        printer.printPage(0);
        FinalRequest(ref,card_no);
    }

    private void FinalRequest(String ref, String card_no) {
        Log.v("TOTAL_PRICE","CHINI: "+ ref);
        try {
            RequestQueue queue = Action.getInstance(getApplicationContext()).getRequestQueue();
            JSONObject jsonObject = new JSONObject();
            jsonObject.accumulate("route_code", "004");
            jsonObject.accumulate("event_name", pref.getString("nEventName",""));
            jsonObject.accumulate("location", pref.getString("location",""));
            jsonObject.accumulate("event_date", pref.getString("eventDate",""));
            jsonObject.accumulate("event_time", pref.getString("eventTime",""));
            jsonObject.accumulate("away_team", pref.getString("oppTeam",""));
            jsonObject.accumulate("home_team", pref.getString("clubName",""));
            jsonObject.accumulate("field_68", pref.getString("deviceID",""));
            jsonObject.accumulate("operator_id", pref.getString("operatorID",""));
            jsonObject.accumulate("ref_no",ref);
            jsonObject.accumulate("price", pref.getString("mEventFare",""));
            jsonObject.accumulate("issued_time",Action.set_terminal_date());
            jsonObject.accumulate("card_no", card_no);

            JSONArray jsonArray = new JSONArray();
            jsonArray.put(jsonObject);
            Log.d("JSON_SENT", "SENT: " + jsonObject);
            String ENDPOINT = Action.getEndpoint("index_api.php");
            JsonArrayRequest myReq = new JsonArrayRequest(Request.Method.POST, ENDPOINT, jsonArray, new Response.Listener<JSONArray>() {
                @Override
                public void onResponse(JSONArray arr) {
                    Log.v("PIN_REQUEST", "received: " + arr);
                    for (int i=0; i < arr.length(); i++){
                        try {
                            JSONObject obj = arr.getJSONObject(i);
                            String status = obj.getString("status");
                            Log.v("Status_",status);
                            if (status.equals("00")) {
                                Toast.makeText(NfcActivity.this, getString(R.string.successful_postrequest), Toast.LENGTH_SHORT).show();
                            } else {
                                new AlertDialog.Builder(NfcActivity.this)
                                        .setTitle(R.string.h_payment_failed)
                                        .setMessage(status)
                                        .setPositiveButton(android.R.string.yes, null).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError volleyError) {

                }
            });
            myReq.setRetryPolicy(new DefaultRetryPolicy((int) TimeUnit.SECONDS.toMillis(10), 1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            queue.add(myReq);

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


    public static String posFormatPrintLine(String leftAlignment, String rightAlignment) {
        String emptyLine = "                           ";

        if (leftAlignment != "" && rightAlignment != "" && leftAlignment != null && rightAlignment != null) {
            try {
                emptyLine = emptyLine.substring(leftAlignment.length() + rightAlignment.length());
                if (emptyLine.length() > 32) {
                    emptyLine += emptyLine.substring(leftAlignment.length() + rightAlignment.length());
                }


            } catch (StringIndexOutOfBoundsException e) {
                e.printStackTrace();

            }
        } else if (leftAlignment != "" && leftAlignment != null && (rightAlignment == "" || rightAlignment == null)) {
            rightAlignment = "";
            emptyLine = "";
        }
        return leftAlignment + emptyLine + rightAlignment;
    }


    private String posFormatPrintLineCenter(String value) {
        String emptyLine = "                          ";
        if (value != "" && value != null) {
            emptyLine = emptyLine.substring(0, (emptyLine.length() - value.length()) / 2);
        }
        return emptyLine + value;
    }

}


