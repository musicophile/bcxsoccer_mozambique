package com.cfm.football.clubs.matcheticketing.utilities;

import android.annotation.SuppressLint;

import java.text.SimpleDateFormat;
import java.util.Calendar;

public class Msg {
    private String MTI;
    private String field_3;
    private String field_4;
    private String field_7;
    private String field_11;
    private String field_12;
    private String field_37;
    private String field_38;
    private String field_39;
    private String field_41;
    private String field_42;
    private String field_43;
    private String field_48;
    private String field_52;
    private String field_58;
    private String field_61;
    private String field_62;
    private String field_67;
    private String field_68;
    private String field_69;
    private String field_100;
    private String field_102;
    private String field_103;
    private String field_128;
    private String route_code;

    ///atributos para registo das transacoes
    //---------------------------------
          public String system_date;
        public String event_name;
        public String location;
        public String event_date;
        public String event_time;
        private String away_team; 
        public String home_team;
        public String operator_id;
        public String operator_type;
        public String ref_no;
        public String price;
        public String issued_time;

    public String getSystem_date() {
        return system_date;
    }

    public void setSystem_date(String system_date) {
        this.system_date = system_date;
    }

    public String getEvent_name() {
        return event_name;
    }

    public void setEvent_name(String event_name) {
        this.event_name = event_name;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getEvent_date() {
        return event_date;
    }

    public void setEvent_date(String event_date) {
        this.event_date = event_date;
    }

    public String getEvent_time() {
        return event_time;
    }

    public void setEvent_time(String event_time) {
        this.event_time = event_time;
    }

    public String getHome_team() {
        return home_team;
    }

    public void setHome_team(String home_team) {
        this.home_team = home_team;
    }

    public String getOperator_id() {
        return operator_id;
    }

    public void setOperator_id(String operator_id) {
        this.operator_id = operator_id;
    }

    public String getOperator_type() {
        return operator_type;
    }

    public void setOperator_type(String operator_type) {
        this.operator_type = operator_type;
    }

    public String getRef_no() {
        return ref_no;
    }

    public void setRef_no(String ref_no) {
        this.ref_no = ref_no;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getIssued_time() {
        return issued_time;
    }

    public void setIssued_time(String issued_time) {
        this.issued_time = issued_time;
    }
//-----------------------------

public String getRoute_code() {
   return route_code;  }
    public void setRoute_code(String route_code) {
        this.route_code = route_code;
    }

    public String get_MTI() {
        return MTI;
    }
    public String get_processing_code() {
        return field_3;
    }
    public String get_amount() {
        return field_4;
    }
    public String get_transmission_datetime() {
        return field_7;
    }
    public String get_stan() {
        return field_11;
    }
    public String get_localtime() {
        return field_12;
    }
    public String get_terminal_id() {
        return field_41;
    }
    public String get_additional_data() {
        return field_48;
    }
    public String get_PIN_data() {
        return field_52;
    }
    public String get_extended_tran_type() {
        return field_61;
    }
    public String get_extended_payment_code() {
        return field_67;
    }
    public String get_receiving_inst() {
        return field_100;
    }
    public String get_from_acc() {
        return field_102;
    }
    public String get_to_acc() {
        return field_103;
    }
    public String get_rsp_code() {
        return field_39;
    }
    public String get_receipt_no() {
        return field_38;
    }
    public String get_business_name() {
        return field_42;
    }
    public String get_agent_location() {
        return field_43;
    }
    public String get_provider_name() {
        return field_58;
    }
    public String get_field_62() {
        return field_62;
    }
    public String get_field_68() {
        return field_68;
    }
    public String get_field_69() {
        return field_69;
    }
    public String get_field_128() {
        return field_128;
    }
    public String get_field_37() {
        return field_37;
    }

    public String getAway_team() {
        return away_team;
    }

    public void setAway_team(String away_team) {
        this.away_team = away_team;
    }

    public void set_MTI(String value) {
        this.MTI=value;
    }
    public void set_processing_code(String value) {
        this.field_3=value;
    }
    public void set_amount(String value) {
        this.field_4=value;
    }
    @SuppressLint("SimpleDateFormat")
    public void set_transmission_datetime() {
        Calendar cal = Calendar.getInstance();
        cal.getTime();
        SimpleDateFormat sdf = new SimpleDateFormat("yyMMddHHmmss");
        String _datetime = sdf.format(cal.getTime()).toString();
        this.field_7=_datetime;
    }
    public void set_stan(String _STAN) {
        this.field_11 = _STAN;//field_7.substring(6)
    }
    public void set_stan() {
        this.field_11 = field_7.substring(6);
    }
    public void set_localtime() {
        this.field_12 = field_7.substring(6);
    }
    public void set_terminal_id(String value) {
        this.field_41=value;
    }
    public void set_PIN_data(String value) {
        this.field_52=value;
    }
    public void set_receiving_inst(String value) {
        this.field_100=value;
    }
    public void set_from_acc(String value) {
        this.field_102=value;
    }
    public void set_to_acc(String value) {

        this.field_103=value;
    }
    public void set_rsp_code(String value) {
        this.field_39=value;
    }
    public void set_receipt_no(String value) {
        this.field_38=value;
    }
    public void set_business_name(String value) {
        this.field_42=value;
    }
    public void set_agent_location(String value) {
        this.field_43=value;
    }
    public void set_provider_name(String value) {
        this.field_58=value;
    }
    public void set_extended_payment_code(String value){
        this.field_67=value;
    }
    public void set_extended_tran_type(String value){
        this.field_61=value;
    }
    public void set_additional_data(String value){
        this.field_48=value;
    }
    public void set_field_62(String value){
        this.field_62=value;
    }
    public void set_field_68(String value){
        this.field_68=value;
    }
    public void set_field_69(String value){
        this.field_69=value;
    }
    public void set_field_128(String value){
        this.field_128=value;
    }
    public void set_field_37(String field_37) {
        this.field_37 = field_37;
    }


      ///atributos para inserir as transacoes
    

}