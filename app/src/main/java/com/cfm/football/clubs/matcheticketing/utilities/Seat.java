package com.cfm.football.clubs.matcheticketing.utilities;

/**
 * Created by jamesb on 2017/03/28.
 */

public class Seat {
    private String status;
    private int id;
    private String number, label;
    private int row,index;



    public Seat()
    {
    }



    public Seat(int id, String status, String label, int index, int row) {
        this.id = id;
        this.status = status;
        this.label = label;
        this.index = index;
        this.row = row;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public int getRow() {
        return row;
    }

    public void setRow(int row) {
        this.row = row;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public int getId() {
        return id;
    }

    public void setId(int id){
        this.id=id;
    }

    public String getStatus() { return status; }

    public void setStatus(String status){
        this.status=status;
    }
}
