package com.cfm.football.clubs.matcheticketing;

import android.app.ProgressDialog;
import android.app.job.JobInfo;
import android.app.job.JobScheduler;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.device.PrinterManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.text.StaticLayout;
import android.text.TextPaint;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.print.PrintHelper;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.cfm.football.clubs.matcheticketing.utilities.Action;
import com.cfm.football.clubs.matcheticketing.utilities.SqliteHelper;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.concurrent.TimeUnit;

import static java.lang.String.valueOf;


public class FormOperator extends AppCompatActivity {


    private int lcd_width;
    private int lcd_height;
    int ret;
    private Bitmap mBitmap;
    private Canvas mCanvas;
    private Paint mp;
    ProgressDialog mProgressDialog;

    private String deviceID, operator_id, eventName, clubName, eventLocation;
    private String oppTeam, eventPrice, capacity, eventTime, eventDate;
    private TextView event_name;
    private TextView club_name;
    private TextView opp_team;
    private TextView available_tkt;
    private TextView total_price;
    private Button printing;
    private TextView printed_tkt;
    private int counter2, counter;
    private PrinterManager printer;
    private SharedPreferences pref;
    private JSONArray offLineArray, nowSavingArray;
    SqliteHelper db;
    private Trans tns;
    private long savedTns;
    private Cursor sync;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_form_conductor);
        Toolbar toolbar = findViewById(R.id.app_bar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(getString(R.string.ticket_issue_header));
        getSupportActionBar().setElevation(5);
        getSupportActionBar().setDisplayShowTitleEnabled(true);

//        tkt_no = findViewById(R.id.tkt_no);

        db = new SqliteHelper(this);
        mProgressDialog = new ProgressDialog(this);
        printer = new PrinterManager();
        pref = getApplicationContext().getSharedPreferences("UhuruPayPref", 0);
        deviceID = pref.getString("deviceID", null);
        operator_id = pref.getString("operatorID", "");
        clubName = pref.getString("clubName", "");
        eventName = pref.getString("nEventName", "");
        capacity = pref.getString("totalCapacity", "");
        oppTeam = pref.getString("oppTeam", "");
        eventPrice = pref.getString("mEventFare", "");
        eventTime = pref.getString("eventTime", "");
        eventDate = pref.getString("eventDate", "");
        eventLocation = pref.getString("location", "");

        checkConnection();
        scheduleJob();

        event_name = findViewById(R.id.event_name);
        club_name = findViewById(R.id.first_team);
        opp_team = findViewById(R.id.second_team);
        available_tkt = findViewById(R.id.available_tkt);
        total_price = findViewById(R.id.total_price);
        printed_tkt = findViewById(R.id.printed_tkt);
        printing = findViewById(R.id.btn_print);


        offLineArray = new JSONArray();
        event_name.setText(eventName);
        club_name.setText(clubName);
        opp_team.setText(oppTeam);
        total_price.setText(eventPrice);

        Cursor res = db.getTickets();
        if (res.getCount() == 0) {

            counter = 0;
            counter2 = Integer.parseInt(capacity);
            printed_tkt.setText(String.valueOf(counter));
            available_tkt.setText(String.valueOf(counter2));
            printing.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    checkConnection();
                    scheduleJob();
                    counter += 1;
                    counter2 -= 1;
                    if (counter2 == -1) {
                        new AlertDialog.Builder(FormOperator.this)
                                .setTitle(getString(R.string.out_of_tickets_header))
                                .setMessage(getString(R.string.out_of_ticket_message))
                                .setPositiveButton(android.R.string.ok, null).show();
                    } else {
                        printer.clearPage();
//                        doprintwork();
                        printed_tkt.setText(String.valueOf(counter));
                        available_tkt.setText(String.valueOf(counter2));
                        db.clearTickets();
                        db.addTickets(String.valueOf(counter), String.valueOf(counter2));
                    }
                }
            });

        } else
            {

            while (res.moveToNext()) {
                counter2 = Integer.parseInt(res.getString(2));
                counter = Integer.parseInt(res.getString(1));
                printed_tkt.setText(String.valueOf(counter));
                available_tkt.setText(String.valueOf(counter2));
                printing.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        checkConnection();
                        scheduleJob();
                        counter += 1;
                        counter2 -= 1;
                        if (counter2 == -1) {
                            printing.setVisibility(View.GONE);
                            new AlertDialog.Builder(FormOperator.this)
                                    .setTitle(getString(R.string.out_of_tickets_header))
                                    .setMessage(getString(R.string.out_of_ticket_message))
                                    .setPositiveButton(android.R.string.ok, null).show();
                        } else {
                           db.clearMainRoute();
                            printer.clearPage();
                            doprintwork();
                            printed_tkt.setText(String.valueOf(counter));
                            available_tkt.setText(String.valueOf(counter2));
                            db.clearTickets();
                            db.addTickets(String.valueOf(counter), String.valueOf(counter2));
                        }

                    }
                });
            }
        }




    }

    private void doprintwork() {
        String ref = System.currentTimeMillis() / 1000 + "";

        Bitmap bitmap = Action.CreateCodeBar(ref, 384, 80);
        Log.v("TOTAL_PRICE", "KIASI: " + ref);
        String str = null;
        str = "";
        str += "       " + eventName;
        str += "\n" + "------------------------------" + "\n";
        str += "\n" + posFormatPrintLineCenter(clubName) + "\n";
        str += "\n" + posFormatPrintLineCenter(getString(R.string.versus)) + "\n";
        str += "\n" + posFormatPrintLineCenter(oppTeam) + "\n";
        str += "\n" + "------------------------------";
        str += "\n" + " "+posFormatPrintLine(getString(R.string.event_location), eventLocation);
        str += "\n" + " "+posFormatPrintLine(getString(R.string.event_date), eventDate);
        str += "\n" + " "+posFormatPrintLine(getString(R.string.event_time),eventTime);
        str += "\n" + " "+posFormatPrintLine(getString(R.string.event_price),eventPrice);
        str += "\n" + "------------------------------" + "\n";
        str += "\n" + "  " + "\n";

//        Bitmap xx = busName(str);
        printer.setupPage(384, 750);
        BitmapFactory.Options opts = new BitmapFactory.Options();
        opts.inPreferredConfig = Bitmap.Config.ARGB_8888;
        opts.inDensity = getResources().getDisplayMetrics().densityDpi;
        opts.inTargetDensity = getResources().getDisplayMetrics().densityDpi;
        Bitmap img = BitmapFactory.decodeResource(getResources(), R.drawable.cfm_logo, opts);
        int header = printer.drawBitmap(img, 50, -1);
        int btm = printer.drawBitmap(bitmap, 20, 470);
//        printer.drawBitmap(xx, 0, -1);
        printer.drawTextEx(str, 5, 100, 400, -1, "arial", 25, 0, 0, 0);
        printer.drawTextEx(posFormatPrintLineCenter("POWERED BY BCX"), 50, 555, 400, -1, "arial", 20, 0, 0, 0);
        printer.prn_paperForWard(0);
        printer.printPage(0);
        FinalRequest(ref);
    }

    private void FinalRequest(final String ref) {
        Log.v("TOTAL_PRICE", "CHINI: " + ref);

        tns = null;
        tns = new Trans();

        RequestQueue queue = Action.getInstance(getApplicationContext()).getRequestQueue();

        tns.setRoute_code("004");
        tns.setSystem_date(Action.set_terminal_time());
        tns.setEvent_name(pref.getString("nEventName", ""));
        tns.setLocation(pref.getString("location", ""));
        tns.setEvent_date(pref.getString("eventDate", ""));
        tns.setEvent_time(pref.getString("eventTime", ""));
        tns.setAway_team(pref.getString("oppTeam", ""));
        tns.setHome_team(pref.getString("clubName", ""));
        tns.setField_68(pref.getString("deviceID", ""));
        tns.setOperator_id(pref.getString("operatorID", ""));
        tns.setOperator_type(pref.getString("operatorType", ""));
        tns.setRef(ref);
        tns.setPrice(pref.getString("mEventFare", ""));
        tns.setIssued_time(Action.set_terminal_date());

        savedTns = db.insertTransaction(tns, "","11","");
        JSONObject reqMsg = Action.getJSONObject(tns);
        JSONArray jsonArray = new JSONArray();
        jsonArray.put(reqMsg);
        Log.d("JSON_SENT", "SENT: " + jsonArray);
        String ENDPOINT = Action.getEndpoint("index_api.php");
        JsonArrayRequest myReq = new JsonArrayRequest(Request.Method.POST, ENDPOINT, jsonArray, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray arr) {
                Log.v("PIN_REQUEST", "received: " + arr);
                for (int i = 0; i < arr.length(); i++) {
                    try {
                        JSONObject obj = arr.getJSONObject(i);
                        String status = obj.getString("status");
                        Log.v("Status_", status);
                        if (status.equals("00")) {
                            printing.setVisibility(View.VISIBLE);
                            Toast.makeText(FormOperator.this, getString(R.string.successful_postrequest), Toast.LENGTH_SHORT).show();
                            db.updateTransStatus( savedTns, "00","11","");
                        } else {
                            new AlertDialog.Builder(FormOperator.this)
                                    .setTitle(R.string.h_payment_failed)
                                    .setMessage(status)
                                    .setPositiveButton(android.R.string.yes, null).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {

                printing.setVisibility(View.VISIBLE);
                Toast.makeText(FormOperator.this, getString(R.string.connection_error), Toast.LENGTH_SHORT).show();
                db.updateTransStatus(savedTns,"","11","99");
            }
        });
        myReq.setRetryPolicy(new DefaultRetryPolicy((int) TimeUnit.SECONDS.toMillis(10), 1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        queue.add(myReq);

    }

    private Bitmap busName(String cfm) {
        lcd_width = 384;
        lcd_height = 500;
        mBitmap = Bitmap.createBitmap(lcd_width, lcd_height, Bitmap.Config.valueOf("ARGB_8888"));

        mCanvas = new Canvas(mBitmap);
        mCanvas.drawColor(Color.WHITE);


        mp = new Paint();
        // mp.setTypeface(Typeface.DEFAULT);
        mp.setTypeface(Typeface.DEFAULT_BOLD);
        TextPaint p = new TextPaint(mp);

        p.setAntiAlias(false);
        p.setTextSize(30);
        p.setARGB(0xff, 0x0, 0x0, 0x0);

        String str = cfm + "\n";

        Log.v("LENGTH-TEXT", String.valueOf(str.length()));

        StaticLayout layout = new StaticLayout(str,
                p, lcd_width, StaticLayout.Alignment.ALIGN_NORMAL, 1.0F, 0.0F, false);

        layout.draw(mCanvas);

        return mBitmap;

    }

    public static String posFormatPrintLine(String leftAlignment, String rightAlignment) {
        String emptyLine = "  ";

        if (leftAlignment != "" && rightAlignment != "" && leftAlignment != null && rightAlignment != null) {
            try {
                emptyLine = emptyLine.substring(leftAlignment.length() + rightAlignment.length());
                if (emptyLine.length() > 32) {
                    emptyLine += emptyLine.substring(leftAlignment.length() + rightAlignment.length());
                }


            } catch (StringIndexOutOfBoundsException e) {
                e.printStackTrace();

            }
        } else if (leftAlignment != "" && leftAlignment != null && (rightAlignment == "" || rightAlignment == null)) {
            rightAlignment = "";
            emptyLine = "";
        }
        return leftAlignment + emptyLine + rightAlignment;
    }

    public static String printVsline(String leftAlignment, String rightAlignment) {
        String emptyLine = "               VS             ";

        if (leftAlignment != "" && rightAlignment != "" && leftAlignment != null && rightAlignment != null) {
            try {
                emptyLine = emptyLine.substring(leftAlignment.length() + rightAlignment.length());
                if (emptyLine.length() > 32) {
                    emptyLine += emptyLine.substring(leftAlignment.length() + rightAlignment.length());
                }


            } catch (StringIndexOutOfBoundsException e) {
                e.printStackTrace();

            }
        } else if (leftAlignment != "" && leftAlignment != null && (rightAlignment == "" || rightAlignment == null)) {
            rightAlignment = "";
            emptyLine = "";
        }
        return leftAlignment + emptyLine + rightAlignment;
    }

    private String posFormatPrintLineCenter(String value) {
        String emptyLine = "                          ";
        if (value != "" && value != null) {
            emptyLine = emptyLine.substring(0, (emptyLine.length() - value.length()) / 2);
        }
        return emptyLine + value;
    }

    private void checkConnection() {
        boolean isConnected = ConnectivityReceiver.isConnected(getApplicationContext());
        showSnack(isConnected);

        sync = db.getTransactions();
        if (sync.getCount() == 0) {
            Toast.makeText(FormOperator.this,"SQLITE IS EMPTY",Toast.LENGTH_LONG).show();
        }else {
            while (sync.moveToNext()) {

                do {
                    JSONArray tnsArr = new JSONArray();
                    JSONObject trx = new JSONObject();
                    try {
                        trx.put("route_code", "004");
                        trx.put("trans_id", sync.getInt(0));
                        trx.put("ref_no", sync.getString(5));
                        trx.put("field_68", sync.getString(6));
                        trx.put("price", sync.getString(7));
                        trx.put("operator_id", sync.getString(8));
                        trx.put("home_team", sync.getString(9));
                        trx.put("away_team", sync.getString(10));
                        trx.put("event_name", sync.getString(11));
                        trx.put("location", sync.getString(12));
                        trx.put("event_date", sync.getString(13));
                        trx.put("event_time", sync.getString(14));
                        tnsArr.put(trx);

                        Log.v("DATA_TO_ARRAY", "array: " + tnsArr);
                        if (tnsArr.length() > 0) {
                            RequestQueue queue = Action.getInstance(FormOperator.this).getRequestQueue();
                            String ENDPOINT = Action.getEndpoint("index_api.php");
                            Log.v("DATA_TO_SERVER", "SENT: " + tnsArr);
                            JsonArrayRequest request = new JsonArrayRequest(Request.Method.POST, ENDPOINT, tnsArr, new Response.Listener<JSONArray>() {
                                @Override
                                public void onResponse(JSONArray arr) {

                                    Log.v("REsp", "ARR: " + arr);
                                    for (int i = 0; i < arr.length(); i++) {

                                        try {
                                            JSONObject obj = arr.getJSONObject(i);
                                            String status = obj.getString("status");

                                            if (status.equals("00")) {
                                                db.updateTransStatus(obj.getInt("trans_id"), "00", "11", "99");
                                                Log.v("Status_", obj.getInt("trans_id") + "");
                                            } else {
                                                Toast.makeText(FormOperator.this, "No access to send data", Toast.LENGTH_LONG).show();
                                            }
                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                        }
                                    }

                                }
                            }, new Response.ErrorListener() {
                                @Override
                                public void onErrorResponse(VolleyError volleyError) {
                                    volleyError.printStackTrace();
                                    Log.v("EMPTY", "OBJ: " + volleyError);

                                }
                            });
                            request.setRetryPolicy(new DefaultRetryPolicy((int) TimeUnit.SECONDS.toMillis(10), 1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                            queue.add(request);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                while (sync.moveToNext());
            }
        }
    }

    // Showing the status in Snackbar
    private void showSnack(boolean isConnected) {
        String message;
        int color;
        if (isConnected) {
            Toast.makeText(this, getString(R.string.network_detection_message), Toast.LENGTH_LONG).show();
        } else {
            Toast.makeText(this, getString(R.string.no_network_error), Toast.LENGTH_LONG).show();
        }

    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    private void scheduleJob() {
        JobInfo myJob = new JobInfo.Builder(0, new ComponentName(this, NetworkSchedulerService.class))
                .setRequiresCharging(false)
                .setMinimumLatency(1000)
                .setOverrideDeadline(2000)
                .setRequiredNetworkType(JobInfo.NETWORK_TYPE_ANY)
                .setPersisted(true)
                .build();

        JobScheduler jobScheduler = (JobScheduler) getSystemService(Context.JOB_SCHEDULER_SERVICE);
        if (jobScheduler != null) {
            jobScheduler.schedule(myJob);
        }
    }


    @Override
    protected void onStop() {
        // A service can be "started" and/or "bound". In this case, it's "started" by this Activity
        // and "bound" to the JobScheduler (also called "Scheduled" by the JobScheduler). This call
        // to stopService() won't prevent scheduled jobs to be processed. However, failing
        // to call stopService() would keep it alive indefinitely.
        stopService(new Intent(this, NetworkSchedulerService.class));
        super.onStop();
    }

    @Override
    protected void onStart() {
        super.onStart();
        // Start service and provide it a way to communicate with this class.
        Intent startServiceIntent = new Intent(this, NetworkSchedulerService.class);
        startService(startServiceIntent);
    }


    private AlertDialog validationDialog;

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {

            case R.id.home:
                this.finish();
                return true;

            case R.id.restart_button:
                restartDialog();
        }
        return super.onOptionsItemSelected(item);

    }

    private void restartDialog() {
        try {
            AlertDialog.Builder pinDialogBuilder = new AlertDialog.Builder(FormOperator.this);
            LayoutInflater inflater = FormOperator.this.getLayoutInflater();
            View pinDialogView = inflater.inflate(R.layout.restart_menu, null);
            pinDialogBuilder.setView(pinDialogView);

            Button funga = pinDialogView.findViewById(R.id.button_restart);
            Button rudi = pinDialogView.findViewById(R.id.button_back);

            validationDialog = pinDialogBuilder.create();
            validationDialog.setCancelable(false);

            rudi.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    try {

                        if (validationDialog != null)
                            if (validationDialog.isShowing()) {
                                validationDialog.dismiss();
                                validationDialog = null;
                            }
                    } catch (NullPointerException e) {
                        e.printStackTrace();

                        if (validationDialog != null)
                            if (validationDialog.isShowing()) {
                                validationDialog.dismiss();
                                validationDialog = null;
                            }
                    }
                }
            });

            funga.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (validationDialog != null)
                        if (validationDialog.isShowing()) {
                            validationDialog.dismiss();
                        }
                    db.deleteRow();
                    db.clearTickets();
                }
            });
            validationDialog.show();
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onBackPressed() {
        FormOperator.super.onBackPressed();
        FormOperator.this.finish();
    }

}