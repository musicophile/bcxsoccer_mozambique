package com.cfm.football.clubs.matcheticketing;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.device.PrinterManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.cfm.football.clubs.matcheticketing.menuActivities.MainMDC;
import com.cfm.football.clubs.matcheticketing.utilities.Action;
import com.cfm.football.clubs.matcheticketing.utilities.Hash;
import com.cfm.football.clubs.matcheticketing.utilities.JsonArrayRequestAlt;
import com.cfm.football.clubs.matcheticketing.utilities.Msg;
import com.cfm.football.clubs.matcheticketing.utilities.Route;
import com.cfm.football.clubs.matcheticketing.utilities.SqliteHelper;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.concurrent.TimeUnit;

import gr.escsoft.michaelprimez.searchablespinner.SearchableSpinner;
import gr.escsoft.michaelprimez.searchablespinner.interfaces.OnItemSelectedListener;

import static com.cfm.football.clubs.matcheticketing.utilities.Action.getEndpoint;

public class FormAgent extends AppCompatActivity {

    private SearchableSpinner event_spinner;
    private String[] event_date;
    private String selectedDate = null;
    private String[] event_time;
    private String selectedTime = null;
    private LinearLayout ly;
    private TextView event_name;
    private TextView club_name;
    private TextView opp_team;
    private TextView available_tkt;
    private TextView total_price;
    private Button printing;
    private TextView printed_tkt;
    private String eventName;
    private String clubName;
    private String oppTeam;
    private String price;
    SqliteHelper db;
    private String capacity;
    private int counter2, counter;
    private PrinterManager printer;
    private String location;
    private String eventDate;
    private String eventTime;
    private Trans tns;
    private SharedPreferences pref;
    private String deviceID;
    private String operator_id;
    private long savedTns;
    private ProgressDialog mProgressDialog;

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_form_agent);
        Toolbar toolbar = findViewById(R.id.app_bar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(getString(R.string.ticket_issue_header));
        getSupportActionBar().setElevation(5);
        getSupportActionBar().setDisplayShowTitleEnabled(true);

        db = new SqliteHelper(this);
        printer = new PrinterManager();
        pref = getApplicationContext().getSharedPreferences("UhuruPayPref", 0);
        deviceID = pref.getString("deviceID", null);
        operator_id = pref.getString("operatorID", "");
        ly = findViewById(R.id.linear_layout);
        ly.setVisibility(View.GONE);
        event_spinner = findViewById(R.id.event_spinner);
        event_name = findViewById(R.id.event_name);
        club_name = findViewById(R.id.first_team);
        opp_team = findViewById(R.id.second_team);
        available_tkt = findViewById(R.id.available_tkt);
        total_price = findViewById(R.id.total_price);
        printed_tkt = findViewById(R.id.printed_tkt);
        printing = findViewById(R.id.btn_print);

        getEvents();

        mProgressDialog = new ProgressDialog(this);
        event_spinner.setOnItemSelectedListener(new OnItemSelectedListener() {
            @Override
            public void onItemSelected(View view, int position, long id) {
                String event = event_spinner.getSelectedItem().toString();
                Toast.makeText(FormAgent.this, event, Toast.LENGTH_LONG).show();
                selectedDate = event_date[position];
                selectedTime = event_time[position];
                getEventDetails(selectedDate, selectedTime);
            }

            @Override
            public void onNothingSelected() {

            }
        });
    }


    private void getEventDetails(String selectedDate, String selectedTime) {
        RequestQueue queue = Volley.newRequestQueue(FormAgent.this);

        final JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("route_code", "010");
            jsonObject.put("kickoff_time", selectedTime);
            jsonObject.put("event_date", selectedDate);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.d("Resp", jsonObject.toString());
        String ENDPOINT = getEndpoint("index_api.php");
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, ENDPOINT, jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject resp) {
                Log.v("Resp", resp+"");
                try {

                    eventName = resp.getString("event_name");
                    clubName = resp.getString("club_name");
                    oppTeam = resp.getString("opp_team");
                    price = resp.getString("price");
                    capacity = resp.getString("assigned_tickets");
                    location = resp.getString("location");
                    eventDate = resp.getString("event_date");
                    eventTime = resp.getString("kickoff_time");

                    event_name.setText(eventName);
                    club_name.setText(clubName);
                    opp_team.setText(oppTeam);
                    total_price.setText(price);
                    ly.setVisibility(View.VISIBLE);
                    printingMethod();

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }


        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
            }
        });

        int socketTimeout = 30000;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        jsonObjectRequest.setRetryPolicy(policy);
        queue.add(jsonObjectRequest);

    }


    private void printingMethod() {
        Cursor res = db.getTicketsAgents();
        if (res.getCount() == 0) {
            counter = 0;
            counter2 = Integer.parseInt(capacity);
            printed_tkt.setText(String.valueOf(counter));
            available_tkt.setText(String.valueOf(counter2));
            printing.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    counter += 1;
                    counter2 -= 1;
                    if (counter2 == -1) {
                        new AlertDialog.Builder(FormAgent.this)
                                .setTitle(getString(R.string.out_of_tickets_header))
                                .setMessage(getString(R.string.out_of_ticket_message))
                                .setPositiveButton(android.R.string.ok, null).show();
                    } else {
                      printer.clearPage();
                        doprintwork();
                        printed_tkt.setText(String.valueOf(counter));
                        available_tkt.setText(String.valueOf(counter2));
                        db.clearTicketsA();
                        db.addTicketsAgent(String.valueOf(counter), String.valueOf(counter2));

                        //enviar aoA PI
                    }
                }
            });

        } else {

            while (res.moveToNext()) {
                counter2 = Integer.parseInt(res.getString(2));
                counter = Integer.parseInt(res.getString(1));
                printed_tkt.setText(String.valueOf(counter));
                available_tkt.setText(String.valueOf(counter2));
                printing.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        counter += 1;
                        counter2 -= 1;
                        if (counter2 == -1) {
                            printing.setVisibility(View.GONE);
                            new AlertDialog.Builder(FormAgent.this)
                                    .setTitle(getString(R.string.out_of_tickets_header))
                                    .setMessage(getString(R.string.out_of_ticket_message))
                                    .setPositiveButton(android.R.string.ok, null).show();
                        } else {
                           db.clearMainRoute();// limpar os dados da base de dados
                         //   printer.clearPage();
                            doprintwork();
                            postRequest();
                            printed_tkt.setText(String.valueOf(counter));
                            available_tkt.setText(String.valueOf(counter2));
                            db.clearTicketsA();
                            db.addTicketsAgent(String.valueOf(counter), String.valueOf(counter2));
                        }

                    }
                });
            }
        }
    }

    // metodo para salvar dados a base de dados no API
    //--------------------------------------------------
    private void postRequest() {
        mProgressDialog.setIndeterminate(true);
        mProgressDialog.setMessage(getString(R.string.loading));
        mProgressDialog.setCancelable(false);
        mProgressDialog.show();

        RequestQueue queue =  Action.getInstance(this).getRequestQueue();
        Msg msg=new Msg();
        msg.setRoute_code("004");//codigo do tipode acao a tomar

        msg.setAway_team(oppTeam);
        msg.setEvent_date(eventDate);
        msg.setLocation(location);
        msg.setEvent_name(eventName);
        msg.setPrice(price);
        msg.setRef_no("1569844013");
        msg.setEvent_time(eventTime);
        msg.setOperator_id(operator_id);
        msg.setOperator_type("2");
        msg.set_field_68(deviceID);//imai



        String ENDPOINT=Action.getEndpoint("index_api.php");
//        JSONObject reqMsg=Action.getJSONObject(msg);
        JSONObject reqMsg=new JSONObject();
        try {// melhor maneira de inser dados na eAPI
            reqMsg.put("route_code", "004");
            reqMsg.put("system_date", "");
            reqMsg.put("event_name", eventName);
            reqMsg.put("location", location);
            reqMsg.put("event_date", eventDate);
            reqMsg.put("event_time", eventTime);
            reqMsg.put("away_team", oppTeam);
            reqMsg.put("home_team", clubName);
            reqMsg.put("field_68", "866995031683422");
            reqMsg.put("operator_id", operator_id);
            reqMsg.put("operator_type", "2");
            reqMsg.put("ref_no", "1569843906");
            reqMsg.put("price", price);

        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.v("FIRST_POST",reqMsg.toString());
        JsonObjectRequest myReq = new JsonObjectRequest(Request.Method.POST, ENDPOINT, reqMsg, new Response.Listener<JSONObject>()
        {
            @Override
            public void onResponse(JSONObject response) {
                mProgressDialog.dismiss();
                Log.v("RESPONSE-MSG", response + "");

                String rspCode = Action.getFieldValue(response, "field_39");
                if (rspCode.equals("00")) {
                    Toast.makeText(FormAgent.this, "Salvo com sucesso", Toast.LENGTH_SHORT).show();
                }
//

                else {
                    Action.resolveRspCode(rspCode);
                    new AlertDialog.Builder(FormAgent.this)
                            .setTitle(R.string.transaction_failure)
                            .setMessage(getString(R.string.h_payment_failed) + rspCode)
                            .setPositiveButton(android.R.string.yes, null).show();
                }
            }

        },new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError ex) {
                mProgressDialog.dismiss();
                ex.printStackTrace();
            }
    });
        myReq.setRetryPolicy(new DefaultRetryPolicy((int) TimeUnit.SECONDS.toMillis(20), 0, 1f));
        queue.add(myReq);
    }
    //--------------------------------------------------
//imprimir no Pos laranja carregado somente...

    private void doprintwork() {

        String ref = System.currentTimeMillis() / 1000 + "";


        Bitmap bitmap = Action.CreateCodeBar(ref, 384, 80);
        Log.v("TOTAL_PRICE", "KIASI: " + ref);
        String str = null;
        str = "";
        str += "       " + eventName;
        str += "\n" + "------------------------------" + "\n";
        str += "\n" + posFormatPrintLineCenter(clubName) + "\n";
        str += "\n" + posFormatPrintLineCenter(getString(R.string.versus)) + "\n";
        str += "\n" + posFormatPrintLineCenter(oppTeam) + "\n";
        str += "\n" + "------------------------------";
        str += "\n" + " "+(posFormatPrintLine(getString(R.string.event_location), location));
        str += "\n" + " "+posFormatPrintLine(getString(R.string.event_date), eventDate);
        str += "\n" + " "+posFormatPrintLine(getString(R.string.event_time),eventTime);
        str += "\n" + " "+posFormatPrintLine(getString(R.string.event_price),(price));
        str += "\n" + "------------------------------" + "\n";
        str += "\n" + "  " + "\n";

//        Bitmap xx = busName(str);
        printer.setupPage(384, 750);
        BitmapFactory.Options opts = new BitmapFactory.Options();
        opts.inPreferredConfig = Bitmap.Config.ARGB_8888;
        opts.inDensity = getResources().getDisplayMetrics().densityDpi;
        opts.inTargetDensity = getResources().getDisplayMetrics().densityDpi;
        Bitmap img = BitmapFactory.decodeResource(getResources(), R.drawable.cfm_logo, opts);
        int header = printer.drawBitmap(img, 50, -1);
        int btm = printer.drawBitmap(bitmap, 20, 470);
//        printer.drawBitmap(xx, 0, -1);
        printer.drawTextEx(str, 5, 100, 400, -1, "arial", 25, 0, 0, 0);
        printer.drawTextEx(posFormatPrintLineCenter("POWERED BY BCX"), 50, 555, 400, -1, "arial", 20, 0, 0, 0);
        printer.prn_paperForWard(0);
        printer.printPage(0);
        FinalRequest(ref);
    }

//    private void doprintwork() {
//
////        String ref = System.currentTimeMillis() / 1000 + "";
////
////
////        Bitmap bitmap = Action.CreateCodeBar(ref, 384, 80);
////        Log.v("TOTAL_PRICE", "KIASI: " + ref);
//        String str = null;
//        str = "";
//        str += "       " + eventName;
//        str += "\n" + "------------------------------" + "\n";
//        str += "\n" + posFormatPrintLineCenter(clubName) + "\n";
//        str += "\n" + posFormatPrintLineCenter(getString(R.string.versus)) + "\n";
//        str += "\n" + posFormatPrintLineCenter(oppTeam) + "\n";
//        str += "\n" + "------------------------------";
//        str += "\n" + " "+(posFormatPrintLine(getString(R.string.event_location), location));
//        str += "\n" + " "+posFormatPrintLine(getString(R.string.event_date), eventDate);
//        str += "\n" + " "+posFormatPrintLine(getString(R.string.event_time),eventTime);
//        str += "\n" + " "+posFormatPrintLine(getString(R.string.event_price),(price));
//        str += "\n" + "------------------------------" + "\n";
//        str += "\n" + "  " + "\n";
//int length = 300;
//int width = 384;
//Bitmap bitmap = Bitmap.createBitmap(width,length,Bitmap.Config.ARGB_8888);
//        Canvas canvas = new Canvas(bitmap);
//        canvas.drawColor(Color.WHITE);
////        Bitmap xx = busName(str);
////        printer.setupPage(384, 750);
////        BitmapFactory.Options opts = new BitmapFactory.Options();
////        opts.inPreferredConfig = Bitmap.Config.ARGB_8888;
////        opts.inDensity = getResources().getDisplayMetrics().densityDpi;
////        opts.inTargetDensity = getResources().getDisplayMetrics().densityDpi;
////        Bitmap img = BitmapFactory.decodeResource(getResources(), R.drawable.cfm_logo, opts);
////        int header = printer.drawBitmap(img, 50, -1);
////        int btm = printer.drawBitmap(bitmap, 20, 470);
////        printer.drawBitmap(xx, 0, -1);
////        printer.drawTextEx(str, 5, 100, 400, -1, "arial", 25, 0, 0, 0);
////        printer.drawTextEx(posFormatPrintLineCenter("POWERED BY BCX"), 50, 555, 400, -1, "arial", 20, 0, 0, 0);
////        printer.prn_paperForWard(0);
////        printer.printPage(0);
////        FinalRequest(ref);
//    }

    private void FinalRequest(final String ref) {
        Log.v("TOTAL_PRICE", "CHINI: " + ref);

        tns = null;
        tns = new Trans();

        RequestQueue queue = Action.getInstance(getApplicationContext()).getRequestQueue();

        tns.setRoute_code("012");//12 codigo para insercao da trasacao
        tns.setSystem_date(Action.set_terminal_time());
        tns.setEvent_name(eventName);
        tns.setLocation(location);
        tns.setEvent_date(eventDate);
        tns.setEvent_time(eventTime);
        tns.setAway_team(oppTeam);
        tns.setHome_team(clubName);
        tns.setField_68(pref.getString("deviceID", ""));
        tns.setOperator_id(pref.getString("operatorID", ""));
        tns.setOperator_type(pref.getString("operatorType", ""));
        tns.setRef(ref);
        tns.setPrice(price);
        tns.setIssued_time(Action.set_terminal_date());

        savedTns = db.insertTransaction(tns, "","11","");
        JSONObject reqMsg = Action.getJSONObject(tns);
        JSONArray jsonArray = new JSONArray();
        jsonArray.put(reqMsg);
        Log.v("JSON_SENT", "SENT: " + jsonArray);
        String ENDPOINT = Action.getEndpoint("index_api.php");
        JsonArrayRequest myReq = new JsonArrayRequest(Request.Method.POST, ENDPOINT, jsonArray, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray arr) {
                Log.v("PIN_REQUEST", "received: " + arr);
                for (int i = 0; i < arr.length(); i++) {
                    try {
                        JSONObject obj = arr.getJSONObject(i);
                        String status = obj.getString("status");
                        Log.v("Status_", status);
                        if (status.equals("00")) {
                            printing.setVisibility(View.VISIBLE);
                            Toast.makeText(FormAgent.this, getString(R.string.successful_postrequest), Toast.LENGTH_SHORT).show();
                            db.updateTransStatus( savedTns, "00","11","");
                        } else {
                            new AlertDialog.Builder(FormAgent.this)
                                    .setTitle(R.string.h_payment_failed)
                                    .setMessage(status)
                                    .setPositiveButton(android.R.string.yes, null).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                printing.setVisibility(View.VISIBLE);
                Toast.makeText(FormAgent.this, getString(R.string.connection_error), Toast.LENGTH_SHORT).show();
                db.updateTransStatus(savedTns,"","11","99");

            }
        });
        myReq.setRetryPolicy(new DefaultRetryPolicy((int) TimeUnit.SECONDS.toMillis(10), 1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        queue.add(myReq);

    }


    private void getEvents(){
        RequestQueue queue = Action.getInstance(getApplicationContext()).getRequestQueue();
        Route route = new Route();
        route.setRoute_code("009");
        String ENDPOINT = getEndpoint("index_api.php");
        JSONObject reqMsg = Action.getRoutes(route);
        JsonObjectRequest myReq = new JsonObjectRequest(Request.Method.POST, ENDPOINT, reqMsg, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.d("JM", response+"");
                try {

                    String status = response.getString("status");
                    if (status.equals("00")) {
                        JSONArray jsonArray = response.getJSONArray("events");
                        String[] list = new String[jsonArray.length()];
                        event_date = new String[jsonArray.length()];
                        event_time = new String[jsonArray.length()];
                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject obj = jsonArray.getJSONObject(i);
                            event_date[i] = obj.getString("event_date");
                            event_time[i] = obj.getString("kickoff_time");
                            String event_name = obj.getString("event_name");
                            list[i] = event_name;
                        }
                        ArrayAdapter<String> adapter = new ArrayAdapter<>(FormAgent.this, android.R.layout.simple_list_item_1, list);
                        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                        event_spinner.setAdapter(adapter);

                    } else {

                        Toast.makeText(FormAgent.this, "Kifaa hakiruhusiwi", Toast.LENGTH_SHORT).show();
                        finish();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {

                Toast.makeText(FormAgent.this, getString(R.string.connection_error), Toast.LENGTH_SHORT).show();
            }
        });
        myReq.setRetryPolicy(new DefaultRetryPolicy((int) TimeUnit.SECONDS.toMillis(10), 1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        queue.add(myReq);

    }

    public static String posFormatPrintLine(String leftAlignment, String rightAlignment) {
        String emptyLine = "                           ";

        if (leftAlignment != "" && rightAlignment != "" && leftAlignment != null && rightAlignment != null) {
            try {
                emptyLine = emptyLine.substring(leftAlignment.length() + rightAlignment.length());
                if (emptyLine.length() > 32) {
                    emptyLine += emptyLine.substring(leftAlignment.length() + rightAlignment.length());
                }


            } catch (StringIndexOutOfBoundsException e) {
                e.printStackTrace();

            }
        } else if (leftAlignment != "" && leftAlignment != null && (rightAlignment == "" || rightAlignment == null)) {
            rightAlignment = "";
            emptyLine = "";
        }
        return leftAlignment + emptyLine + rightAlignment;
    }


    private String posFormatPrintLineCenter(String value) {
        String emptyLine = "                          ";
        if (value != "" && value != null) {
            emptyLine = emptyLine.substring(0, (emptyLine.length() - value.length()) / 2);
        }
        return emptyLine + value;
    }
}
