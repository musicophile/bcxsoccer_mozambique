package com.cfm.football.clubs.matcheticketing;

import android.app.IntentService;
import android.content.Intent;
import android.device.PrinterManager;
import android.graphics.Bitmap;

import com.cfm.football.clubs.matcheticketing.utilities.Action;

public class PrintBillService extends IntentService {

    private PrinterManager printer;

    public PrintBillService(String name) {
        super(name);
    }

//    public PrintBillService() {
//        super("bill");
//        // TODO Auto-generated constructor stub
//    }

    @Override
    public void onCreate() {
        // TODO Auto-generated method stub
        super.onCreate();
        printer = new PrinterManager();
    }

    @Override
    public void onDestroy() {
        // TODO Auto-generated method stub
        super.onDestroy();
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        // TODO Auto-generated method stub
        String context = intent.getStringExtra("SPRT");
        if(context== null || context.equals("")) return ;
        printer.setupPage(384, -1);
        printer.drawTextEx(context, 5, 0,384,-1, "arial", 24, 0, 0, 0);
        printer.printPage(0);
    }
    
    private void sleep(){
        try {
            Thread.currentThread();
			Thread.sleep(50);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

}