package com.cfm.football.clubs.matcheticketing.menuActivities;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.cfm.football.clubs.matcheticketing.R;

import java.util.ArrayList;


public class  MainUtilities extends AppCompatActivity {

    //Android Variables
    private Toolbar toolbar;
    private static RecyclerView.Adapter adapter;
    private RecyclerView.LayoutManager layoutManager;
    private static RecyclerView recyclerView;
    private static ArrayList<menuData> items;
    static View.OnClickListener myOnClickListener;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);

        toolbar=(Toolbar)findViewById(R.id.app_bar);

        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setTitle("Utilities");

        getSupportActionBar().setElevation(5);


        getSupportActionBar().setDisplayShowTitleEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        recyclerView = (RecyclerView) findViewById(R.id.my_recycler_view);
        recyclerView.setHasFixedSize(true);

        layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());

        items = new ArrayList<>();
        menuBuilder myMenu=new menuBuilder("MainUtilities",getApplicationContext());
        for (int i = 0; i < myMenu.nameArray.length; i++) {
            items.add(
                    new menuData(
                            myMenu.nameArray[i],
                            myMenu.descriptionArray[i],
                            myMenu.drawableArray[i]
                    )
            );
        }


        adapter = new menuMyAdapter(items);
        recyclerView.setAdapter(adapter);

        recyclerView.addOnItemTouchListener(
                new menuRecyclerItemClickListener(this.getApplicationContext(), new menuRecyclerItemClickListener.OnItemClickListener() {
                    @Override
                    public void onItemClick(View view, int position) {
                        int selectedItemPosition = recyclerView.getChildPosition(view);
                        RecyclerView.ViewHolder viewHolder = recyclerView.findViewHolderForPosition(selectedItemPosition);
                        TextView textViewName = (TextView) viewHolder.itemView.findViewById(R.id.textViewName);
                        String selectedName = (String) textViewName.getText();
                        //Toast.makeText(this, "you clicked "+selectedName, Toast.LENGTH_SHORT).show();
                        startActivity(new Intent("com.umoja.bcx.eticketing.DormantPackages.FormPaymentsInstant").putExtra("selectedName",selectedName));

                    }
                })
        );



    }


    @Override
    public boolean onOptionsItemSelected (MenuItem item){
        if (item.getItemId() == android.R.id.home) {
            this.finish();
        }
        return super.onOptionsItemSelected(item);

    }




    @Override
    public void onBackPressed() {
        //Add some stuff
        super.onBackPressed();
        this.finish();
    }



}
