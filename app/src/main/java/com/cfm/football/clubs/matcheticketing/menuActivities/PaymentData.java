package com.cfm.football.clubs.matcheticketing.menuActivities;

public class PaymentData {

    String reference_number, amount, terminal_date;


    public PaymentData(String reference_number, String amount, String terminal_date) {
        this.reference_number = reference_number;
        this.amount = amount;
        this.terminal_date = terminal_date;
    }

    public String getReference_number() {
        return reference_number;
    }

    public void setReference_number(String reference_number) {
        this.reference_number = reference_number;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getTerminal_date(){ return terminal_date; }

    public void setTerminal_date(String terminal_date){
        this.terminal_date = terminal_date;
    }

}
