package com.cfm.football.clubs.matcheticketing.utilities;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import androidx.annotation.Nullable;

import com.cfm.football.clubs.matcheticketing.Trans;

import org.json.JSONArray;

import java.util.ArrayList;
import java.util.List;

import static android.content.ContentValues.TAG;

public class SqliteHelper extends SQLiteOpenHelper {
    public static final String DATABASE_NAME = "cfm_soccer.db";
    public static final String TABLE_NAME = "registeruser";
    public static final String col_1 = "user_id";
    public static final String col_2 = "user_name";
    public static final String col_3 = "password";
    public static final String TABLE_SEATS = "booked_seats";
    public static final String seat_id = "seat_id";
    public static final String seat = "b_seats";
    public static final String TABLE_TICKETS = "tickets";
    public static final String PRINTED = "printed_tkt";
    public static final String AVAILABLE = "available_tkt";
    public static final String TABLE_TICKETS_A = "ticketsAgent";
    public static final String PRINTED_A = "printed_tkt";
    public static final String AVAILABLE_A = "available_tkt";
    public static final String TABLE_TRANS = "transactions";
    public static final String REF_NO = "ref_no";
    public static final String DEVICE_IMEI = "device_imei";
    public static final String PRICE = "price";
    public static final String OPERATOR_ID = "operator_id";
    public static final String OPERATOR_TYPE = "operator_type";
    public static final String HOME_TEAM = "home_team";
    public static final String AWAY_TEAM = "away_team";
    public static final String EVENT_NAME = "event_name";
    public static final String LOCATION = "location";
    public static final String EVENT_DATE = "event_date";
    public static final String EVENT_TIME = "event_time";
    public static final String STATUS = "status";
    public static final String STATUS11 = "status11";
    public static final String STATUS99 = "status99";
    public static final String DATE = "system_date";
    public static final String TRANS_ID = "trans_id";

    public SqliteHelper(@Nullable Context context) {
        super(context, DATABASE_NAME, null, 1);
    }


    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL("CREATE TABLE registeruser(user_id INTEGER PRIMARY KEY AUTOINCREMENT, user_name VARCHAR(20), password VARCHAR(20));");
        sqLiteDatabase.execSQL("CREATE TABLE booked_seats(seat_id INTEGER PRIMARY KEY AUTOINCREMENT, b_seats);");
        sqLiteDatabase.execSQL("CREATE TABLE tickets(_id INTEGER PRIMARY KEY AUTOINCREMENT, available_tkt, printed_tkt);");
        sqLiteDatabase.execSQL("CREATE TABLE ticketsAgent(_id INTEGER PRIMARY KEY AUTOINCREMENT, available_tkt, printed_tkt);");
        sqLiteDatabase.execSQL("CREATE TABLE transactions(trans_id INTEGER PRIMARY KEY AUTOINCREMENT, status11, status99, status, operator_type, system_date, ref_no, device_imei, price, operator_id, home_team, away_team, event_name, location, event_date, event_time);");
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        sqLiteDatabase.execSQL(" DROP TABLE IF EXISTS " + TABLE_NAME);
        sqLiteDatabase.execSQL(" DROP TABLE IF EXISTS " + TABLE_SEATS);
        sqLiteDatabase.execSQL(" DROP TABLE IF EXISTS " + TABLE_TICKETS);
        sqLiteDatabase.execSQL(" DROP TABLE IF EXISTS " + TABLE_TRANS);
        onCreate(sqLiteDatabase);
    }

    public long addUser(String username, String password) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("user_name", username);
        contentValues.put("password", password);
        long res = db.insert("registeruser", null, contentValues);
        db.close();
        return res;
    }

    public boolean checkUser(String Username, String password) {

        String[] columns = {col_1};
        SQLiteDatabase db = getReadableDatabase();
        String selection = col_2 + " =? " + " and " + col_3 + " =? ";
        String[] selectionArgs = {Username, password};
        Cursor cursor = db.query(TABLE_NAME, columns, selection, selectionArgs, null, null, null);
        int count = cursor.getCount();
        cursor.close();
        db.close();

        return count > 0;
    }

    public void deleteRow() {
        SQLiteDatabase db = getWritableDatabase();
        db.execSQL("delete from " + TABLE_NAME);
    }

    public void clearTickets() {
        SQLiteDatabase db = getWritableDatabase();
        db.execSQL("DELETE FROM " + TABLE_TICKETS);
    }

 public void clearTicketsA() {
        SQLiteDatabase db = getWritableDatabase();
        db.execSQL("DELETE FROM " + TABLE_TICKETS_A);
    }

    public void clearMainRoute() {
        SQLiteDatabase db = getWritableDatabase();
        db.execSQL("delete from " + TABLE_TRANS);
    }


    public void addSeats(String position) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(seat, position);
        db.insert(TABLE_SEATS, null, contentValues);
        db.close();
    }


    public long insertTransaction(Trans tns, String status, String status11, String status99) {
      long id;

        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();

        contentValues.put(STATUS, status);
        contentValues.put(STATUS11, status11);
        contentValues.put(STATUS99, status99);
        contentValues.put(DATE, tns.getSystem_date());
        contentValues.put(OPERATOR_TYPE, tns.getOperator_type());
        contentValues.put(REF_NO, tns.getRef());
        contentValues.put(DEVICE_IMEI, tns.getField_68());
        contentValues.put(PRICE, tns.getPrice());
        contentValues.put(OPERATOR_ID, tns.getOperator_id());
        contentValues.put(HOME_TEAM, tns.getHome_team());
        contentValues.put(AWAY_TEAM, tns.getAway_team());
        contentValues.put(EVENT_NAME, tns.getEvent_name());
        contentValues.put(LOCATION, tns.getLocation());
        contentValues.put(EVENT_DATE, tns.getEvent_date());
        contentValues.put(EVENT_TIME, tns.getEvent_time());

       id = db.insert(TABLE_TRANS, null, contentValues);
        Log.v("IN_SERT", "Recorded:" + tns.getSystem_date()+" ID:"+contentValues );
        db.close();

        return id;
    }

    public long updateTransStatus( long savedTns, String status, String status11, String status99) {
        long id = -1;

        String selection =  "trans_id=?" ;
        String []selectionArgs={String.valueOf(savedTns)};
        SQLiteDatabase db = this.getWritableDatabase();
            ContentValues contentValues = new ContentValues();
            contentValues.put(STATUS, status);
            contentValues.put(STATUS11, status11);
            contentValues.put(STATUS99, status99);
        db.update(TABLE_TRANS, contentValues, selection, selectionArgs);

        db.close();

        return id;
    }

    public Cursor getTransactions(){
        String selectQuery = "SELECT * FROM transactions WHERE status99 == '99' OR status11 == '11' AND status != '00' AND operator_type == '2'";
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);//selectQuery,selectedArguments
        return cursor;
    }


    public void addTickets(String available_tkt, String printed_tkt) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(AVAILABLE, available_tkt);
        contentValues.put(PRINTED, printed_tkt);
        db.insert(TABLE_TICKETS, null, contentValues);
        db.close();
    }

 public void addTicketsAgent(String available_tkt, String printed_tkt) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(AVAILABLE_A, available_tkt);
        contentValues.put(PRINTED_A, printed_tkt);
        db.insert(TABLE_TICKETS_A, null, contentValues);
        db.close();
    }

    public Cursor getTickets() {

        SQLiteDatabase db = this.getReadableDatabase();
        String select = "SELECT * FROM " + TABLE_TICKETS;
        Cursor cursor = db.rawQuery(select, null);//selectQuery,selectedArguments
        return cursor;
    }

   public Cursor getTicketsAgents() {

        SQLiteDatabase db = this.getReadableDatabase();
        String select = "SELECT * FROM " + TABLE_TICKETS_A;
        Cursor cursor = db.rawQuery(select, null);//selectQuery,selectedArguments
        return cursor;
    }
}
