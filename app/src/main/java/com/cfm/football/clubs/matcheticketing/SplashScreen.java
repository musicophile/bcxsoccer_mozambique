package com.cfm.football.clubs.matcheticketing;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.cfm.football.clubs.matcheticketing.utilities.Action;
import com.cfm.football.clubs.matcheticketing.utilities.Msg;

import org.json.JSONObject;

import java.util.concurrent.TimeUnit;

public class SplashScreen extends AppCompatActivity {


    private String simSerial;
    private String deviceID;

    private SharedPreferences pref;
    private static String TAG = "MainActivity";
    private static final int PERMISSION_REQUEST_CODE = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (!checkPermission()) {
            ActivityCompat.requestPermissions(SplashScreen.this,
                    new String[]{Manifest.permission.READ_PHONE_STATE},
                    PERMISSION_REQUEST_CODE);
            Toast.makeText(this, "Accept permission first", Toast.LENGTH_SHORT).show();
            return;
        }
        Action.DefaultLanguage(SplashScreen.this,"sw");
        setContentView(R.layout.activity_splash_screen);


        //Get Telephony details
        TelephonyManager tm = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
        deviceID = tm.getDeviceId();
        simSerial = tm.getDeviceId();

        pref = getApplicationContext().getSharedPreferences("UhuruPayPref", 0);
        postRequest();

    }

    private void postRequest() {
        RequestQueue queue = Action.getInstance(this).getRequestQueue();
        Msg msg = new Msg();
        msg.setRoute_code("000");
        msg.set_field_68(deviceID);
        msg.set_field_69(simSerial);

        String ENDPOINT = Action.getEndpoint("eticketing.php");
        JSONObject reqMsg = Action.getJSONObject(msg);

        Log.v("REQUEST_MESSAGE", "" + reqMsg + "");
        JsonObjectRequest myReq = new JsonObjectRequest(Request.Method.POST, ENDPOINT, reqMsg, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                findViewById(R.id.progressBar).setVisibility(View.GONE);
                Log.v("REQUEST_MESSAGE", "RES: " + response);
                try {
                    //Get Local Download Version in Installed App
                    String field_39 = Action.getFieldValue(response, "field_39");
                    String deviceType = Action.getFieldValue(response, "device_type");

                    setDeviceType(deviceType);

                    if (field_39.equalsIgnoreCase("00")) {
                        Action.next(SplashScreen.this, FormLogin.class);
                        finish();
                    } else {
                        Toast.makeText(SplashScreen.this, R.string.device_not_allowed, Toast.LENGTH_SHORT).show();
                        finish();
                    }

                } catch (NullPointerException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                findViewById(R.id.progressBar).setVisibility(View.GONE);
                String message = Action.parseErrorResponse(volleyError);
                Toast.makeText(SplashScreen.this, message, Toast.LENGTH_SHORT).show();
                Action.next(SplashScreen.this, FormLogin.class);
                finish();
                Log.v("REQUEST_MES", "RES: " + volleyError);
            }
        });
        myReq.setRetryPolicy(new DefaultRetryPolicy((int) TimeUnit.SECONDS.toMillis(20), DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        queue.add(myReq);

    }

    public boolean setDeviceType(String deviceType) {
        SharedPreferences.Editor editor = pref.edit();
        editor.putString("deviceType", deviceType);
        editor.commit(); //commit changes
        return true;
    }

    private boolean checkPermission() {
        int result = ContextCompat.checkSelfPermission(SplashScreen.this, Manifest.permission.READ_PHONE_STATE);
        return result == PackageManager.PERMISSION_GRANTED;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case PERMISSION_REQUEST_CODE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    doRestart(this);
                } else {
                    doFinish(this);
                }
                break;
        }
    }

    /*
     *Restart after allowing permission
     */
    public static void doRestart(Activity anyActivity) {
        anyActivity.startActivity(new Intent(anyActivity.getApplicationContext(), SplashScreen.class));
    }

    /*
     *Finish after denying permission
     */
    public static void doFinish(Activity anyActivity) {
        anyActivity.finish();
    }
}
