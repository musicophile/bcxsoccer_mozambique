package com.cfm.football.clubs.matcheticketing.utilities;

import android.annotation.SuppressLint;

import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 * Created by jamesb on 2017/02/27.
 */

public class Route {
    String From, To, Price,MTI, field_3, field_61, field_128, field_7, field_62, field_68, field_69, field_11, field_58 ;
    private String field_4;
    private String field_12;
    private String field_37;
    private String field_38;
    private String field_39;
    private String field_41;
    private String field_42;
    private String field_43;
    private String field_48;
    private String field_52;
    private String field_67;
    private String field_100;
    private String field_102;
    private String field_103;
    private String seat_no;
    private String customer_name;
    private String passenger_category;
    private String customer_category;
    private String excess_luggage;
    private String route_code;
    private String phone_number;
    private String bus_id;

    public String getBus_id(){
        return bus_id;
    }

    public void setBus_id(String bus_id){
        this.bus_id = bus_id;
    }

    public String getPhone_number() {
        return phone_number;
    }

    public void setPhone_number(String phone_number) {
        this.phone_number = phone_number;
    }

    public String getRoute_code() {
        return route_code;
    }

    public void setRoute_code(String route_code) {
        this.route_code = route_code;
    }

    public String getExcess_luggage() {
        return excess_luggage;
    }

    public void setExcess_luggage(String excess_luggage) {
        this.excess_luggage = excess_luggage;
    }

    public Route(){

    }

    public Route(String from, String to, String price, String MTI, String field_3) {
        this.From = from;
        this.To = to;
        this.Price = price;
        this.MTI = MTI;
        this.field_3 = field_3;
    }

    public String getPassenger_category() {
        return passenger_category;
    }

    public void setPassenger_category(String passenger_category) {
        this.passenger_category = passenger_category;
    }

    public String getCustomerCategory() {
        return customer_category;
    }

    public void setCustomerCategory(String customer_category) {
        this.customer_category = customer_category;
    }

    public String getSeat_no() {
        return seat_no;
    }

    public void setSeat_no(String seat_no) {
        this.seat_no = seat_no;
    }

    public String getCustomer_name() {
        return customer_name;
    }

    public void setCustomer_name(String customer_name) {
        this.customer_name = customer_name;
    }

    public String get_amount() {
        return field_4;
    }
    public String get_transmission_datetime() {
        return field_7;
    }

    public String get_localtime() {
        return field_12;
    }
    public String get_terminal_id() {
        return field_41;
    }
    public String get_additional_data() {
        return field_48;
    }
    public String get_PIN_data() {
        return field_52;
    }
    public String get_extended_payment_code() {
        return field_67;
    }
    public String get_receiving_inst() {
        return field_100;
    }
    public String get_from_acc() {
        return field_102;
    }
    public String get_to_acc() {
        return field_103;
    }
    public String get_rsp_code() {
        return field_39;
    }
    public String get_receipt_no() {
        return field_38;
    }
    public String get_business_name() {
        return field_42;
    }
    public String get_agent_location() {
        return field_43;
    }
    public String get_field_37() {
        return field_37;
    }


    public void set_MTI(String value) {
        this.MTI=value;
    }
    public void set_processing_code(String value) {
        this.field_3=value;
    }
    public void set_amount(String value) {
        this.field_4=value;
    }
    @SuppressLint("SimpleDateFormat")
    public void set_transmission_datetime() {
        Calendar cal = Calendar.getInstance();
        cal.getTime();
        SimpleDateFormat sdf = new SimpleDateFormat("yyMMddHHmmss");
        String _datetime = sdf.format(cal.getTime()).toString();
        this.field_7=_datetime;
    }
    public void set_stan(String _STAN) {
        this.field_11 = _STAN;//field_7.substring(6)
    }
    public void set_stan() {
        this.field_11 = field_7.substring(6);
    }
    public void set_localtime() {
        this.field_12 = field_7.substring(6);
    }
    public void set_terminal_id(String value) {
        this.field_41=value;
    }
    public void set_PIN_data(String value) {
        this.field_52=value;
    }
    public void set_receiving_inst(String value) {
        this.field_100=value;
    }
    public void set_from_acc(String value) {
        this.field_102=value;
    }
    public void set_to_acc(String value) {

        this.field_103=value;
    }
    public void set_rsp_code(String value) {
        this.field_39=value;
    }
    public void set_receipt_no(String value) {
        this.field_38=value;
    }
    public void set_business_name(String value) {
        this.field_42=value;
    }
    public void set_agent_location(String value) {
        this.field_43=value;
    }
    public void set_extended_payment_code(String value){
        this.field_67=value;
    }
    public void set_additional_data(String value){
        this.field_48=value;
    }
    public void set_field_37(String field_37) {
        this.field_37 = field_37;
    }

    public void set_field_128(String value){
        this.field_128=value;
    }

    public String get_field_128() {
        return field_128;
    }

    public String get_field_62() {
        return field_62;
    }
    public String get_field_68() {
        return field_68;
    }
    public String get_field_69() {
        return field_69;
    }

    public void set_field_62(String value){
        this.field_62=value;
    }
    public void set_field_68(String value){
        this.field_68=value;
    }
    public void set_field_69(String value){
        this.field_69=value;
    }

    public void set_extended_tran_type(String value){
        this.field_61=value;
    }

    public String get_stan() {
        return field_11;
    }


    public void set_provider_name(String value) {
        this.field_58=value;
    }

    public String get_provider_name() {
        return field_58;
    }




    public String get_extended_tran_type() {
        return field_61;
    }

    public String getMTI() {
        return MTI;
    }

    public void setMTI(String MTI) {
        this.MTI = MTI;
    }

    public String getProcessing_code() {
        return field_3;
    }

    public void setProcessing_code(String field_3) {
        this.field_3 = field_3;
    }

    public String getFrom() {
        return From;
    }

    public void setFrom(String from) {
        this.From = from;
    }

    public String getTo() {
        return To;
    }

    public void setTo(String to) {
        this.To = to;
    }

    public String getPrice() {
        return Price;
    }

    public void setPrice(String price) {
        this.Price = price;
    }
}
