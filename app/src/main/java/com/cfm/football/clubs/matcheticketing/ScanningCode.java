package com.cfm.football.clubs.matcheticketing;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.cfm.football.clubs.matcheticketing.utilities.Action;
import com.cfm.football.clubs.matcheticketing.utilities.Barcode;
import com.google.zxing.Result;

import org.json.JSONObject;

import me.dm7.barcodescanner.zxing.ZXingScannerView;


public class ScanningCode extends AppCompatActivity implements ZXingScannerView.ResultHandler {

    private Button scanBtn, submit;
    private TextView formatTxt, contentTxt;
    ProgressDialog mProgressDialog;
    private static final int PERMISSION_REQUEST_CODE = 1;
    private String simSerial;
    private String deviceID;
    private EditText etBookingRef, etPhoneNumber, etIdentityNumber;
    private String nambaYaSimu;
    private String nambaYaKitambulisho;
    private String nambaYaKumbukumbu;
    private ZXingScannerView mScannerView;

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mScannerView = new ZXingScannerView(this);   // Programmatically initialize the scanner view
        setContentView(mScannerView);

        TelephonyManager tm = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);

        if (checkSelfPermission(Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    Activity#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for Activity#requestPermissions for more details.
            return;
        }
        deviceID = tm.getDeviceId();

        simSerial = tm.getSimSerialNumber();

    }


    @Override
    protected void onStart() {
        super.onStart();
        if (!checkPermission()) {
            requestPermission();
        }else{

        }
    }


    private boolean checkPermission(){
        int result = ContextCompat.checkSelfPermission(ScanningCode.this, Manifest.permission.CAMERA);
        if (result == PackageManager.PERMISSION_GRANTED){

            return true;

        } else {

            return false;

        }
    }


    private void requestPermission(){

        if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.CAMERA)){
            ActivityCompat.requestPermissions(this,new String[]{Manifest.permission.CAMERA},PERMISSION_REQUEST_CODE);
        }
        else {
            ActivityCompat.requestPermissions(this,new String[]{Manifest.permission.CAMERA},PERMISSION_REQUEST_CODE);
        }
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case PERMISSION_REQUEST_CODE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    ShowMsg(getString(R.string.permission_granted));

                } else {

                }
                break;
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        mScannerView.setResultHandler(this); // Register ourselves as a handler for scan results.
        mScannerView.startCamera();          // Start camera on resume
    }

    @Override
    public void onPause() {
        super.onPause();
        mScannerView.stopCamera();           // Stop camera on pause
    }




    /*
     *Toaster
     */
    public void ShowMsg(String a){
        Toast.makeText(this,a,
                Toast.LENGTH_SHORT).show();
    }



//    /*
//     * function that receive the barcode content result and pass it postRequest function
//     */
//    @Override
//    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
//        IntentResult result = IntentIntegrator.parseActivityResult(requestCode, resultCode, data);
//        if(result != null) {
//            if(result.getContents() == null) {
//                Toast.makeText(this, getString(R.string.cancelled), Toast.LENGTH_LONG).show();
//            } else {
//                mProgressDialog.setIndeterminate(true);
//                mProgressDialog.setMessage(getResources().getString(R.string.loader_text));
//                mProgressDialog.setCancelable(false);
//                mProgressDialog.show();
//
//                postRequest(result.getContents(),nambaYaSimu,nambaYaKitambulisho);
//            }
//        } else {
//            super.onActivityResult(requestCode, resultCode, data);
//        }
//    }

    private void postRequest(String refNo, String phoneNo, String idNo ){
        RequestQueue queue = Action.getInstance(getApplicationContext()).getRequestQueue();
        Barcode barcode = new Barcode();
        barcode.setMTI("010");
        barcode.setField_3("910000");
        barcode.setField_61("5000");
        barcode.setBarcode(refNo);
        barcode.set_transmission_datetime();
        barcode.set_stan();
        barcode.set_field_68(deviceID);
        barcode.set_field_69(simSerial);
        barcode.setIdNo(idNo);
        barcode.setPhoneNo(phoneNo);

        String ENDPOINT=Action.getEndpoint("eticketing.php");
        JSONObject reqMsg=Action.getBarcode(barcode);

        Log.v("REQUEST_MESSAGE",reqMsg+"");
        JsonObjectRequest myReq = new JsonObjectRequest(Request.Method.POST,ENDPOINT,reqMsg, createMyReqSuccessListener(), createMyReqErrorListener());
        //myReq.setRetryPolicy(new DefaultRetryPolicy((int) TimeUnit.SECONDS.toMillis(5), 0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        queue.add(myReq);
    }
    private Response.Listener<JSONObject> createMyReqSuccessListener() {
        return new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(final JSONObject response) {

                String responseCode = Action.getFieldValue(response, "field_39");
                String booking_ref = Action.getFieldValue(response, "booking_ref");
                String seats = Action.getFieldValue(response, "seat_no");
                String bus_name = Action.getFieldValue(response, "bus_name");
                String plate_number = Action.getFieldValue(response, "plate_number");
                String status = Action.getFieldValue(response, "pay_status");
                String travel_date = Action.getFieldValue(response, "travel_date");
                String amount = Action.getFieldValue(response,"amount");


                if(status.equalsIgnoreCase("P")){
                    mProgressDialog.dismiss();
                    trxPopStatus(getString(R.string.valid),booking_ref, seats, bus_name, plate_number,travel_date,amount);
                }
                else if (status.equalsIgnoreCase("N")){
                    mProgressDialog.dismiss();
                    trxPopError("Tiketi Sahihi, Haijalipiwa.");
                }
                else if (status.equalsIgnoreCase("E")){
                    mProgressDialog.dismiss();
                    trxPopError("Tiketi imepita muda wa kulipiwa.");
                }else{
                    mProgressDialog.dismiss();
                    trxPopError("tiketi haipo");
                }
            }
        };
    }
    private Response.ErrorListener createMyReqErrorListener() {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError ex) {
                mProgressDialog.dismiss();
                new android.app.AlertDialog.Builder(ScanningCode.this)
                        .setTitle(R.string.h_error_details)
                        .setMessage(R.string.connection_error)
                        .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int whichButton) {
                                dialog.dismiss();
                                ScanningCode.this.finish();
                            }
                        }).show();
            }
        };
    }

    public void trxPopStatus(String a, String booking_ref, String seats, String bus_name, String plate_number, String travel_date, String amount){
        ImageView image = new ImageView(this);
        image.setImageResource(R.drawable.accept);
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(getString(R.string.payment))
                .setMessage(Action.getstringResource(getApplicationContext(), R.string.status,"",false)+
                        "\n" + Action.getstringResource(getApplicationContext(), R.string.reference_number,"",false)+": " + booking_ref +
                        "\n" + Action.getstringResource(getApplicationContext(), R.string.seat_number,"",false)+": " + seats +
                        "\n" + Action.getstringResource(getApplicationContext(), R.string.seat_number,"",false)+": " + bus_name +
                        "\n" + Action.getstringResource(getApplicationContext(), R.string.plate_number,"",false)+": " + plate_number +
                        "\n" + Action.getstringResource(getApplicationContext(), R.string.travelDate,"",false)+": " + travel_date +
                        "\n" + Action.getstringResource(getApplicationContext(), R.string.amount,"",false)+": " + amount +
                        "\n" + Action.getstringResource(getApplicationContext(), R.string.info,"",false)+": " + a
                )
                .setView(image)
                .setCancelable(false)
                .setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        mProgressDialog.dismiss();
                        dialog.cancel();
                    }
                });
        AlertDialog alert = builder.create();
        alert.show();
    }


    /*
     *Dialog to display invalid receipt response
     */
    public void trxPopError(String a){
        ImageView image = new ImageView(this);
        image.setImageResource(R.drawable.deny);
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(getString(R.string.payment))
                .setMessage(
                        "\n\n" + Action.getstringResource(getApplicationContext(), R.string.info,"",false)+": " + a
                )
                .setCancelable(false)
                .setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        mProgressDialog.dismiss();
                        dialog.cancel();
                    }
                })
                .setView(image);
        AlertDialog alert = builder.create();
        alert.show();
    }

    /*
     *Display menu
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        // getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public void handleResult(Result rawResult) {
        // Do something with the result here
        // Log.v("tag", rawResult.getText()); // Prints scan results
        // Log.v("tag", rawResult.getBarcodeFormat().toString()); // Prints the scan format (qrcode, pdf417 etc.)


        onBackPressed();

        // If you would like to resume scanning, call this method below:
        //mScannerView.resumeCameraPreview(this);
    }



}
