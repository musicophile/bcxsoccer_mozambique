package com.cfm.football.clubs.matcheticketing.menuActivities;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.cfm.football.clubs.matcheticketing.FormAgent;
import com.cfm.football.clubs.matcheticketing.FormOperator;
import com.cfm.football.clubs.matcheticketing.NfcActivity;
import com.cfm.football.clubs.matcheticketing.R;
import com.cfm.football.clubs.matcheticketing.ScanActivity;
import com.cfm.football.clubs.matcheticketing.utilities.Action;
import com.cfm.football.clubs.matcheticketing.utilities.SqliteHelper;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

import gr.escsoft.michaelprimez.searchablespinner.SearchableSpinner;

//import android.support.v7.app.AlertDialog;

/**
 * Created by michael.nkotagu on 6/18/2015.
 */
public class MainMDC extends AppCompatActivity {


    //Android Variables
    private Toolbar toolbar;
    private static RecyclerView.Adapter adapter;
    private RecyclerView.LayoutManager layoutManager;
    private static RecyclerView recyclerView;
    private static ArrayList<menuData> items;

    private ProgressDialog mProgressDialog;

    private SharedPreferences pref;
    private String userID;

    private SqliteHelper db;
    private String deviceID,operator_id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);
//        Action.DefaultLanguage(MainMDC.this, Action.language);
        toolbar =  findViewById(R.id.app_bar);
        setSupportActionBar(toolbar);


//        db = new SqliteHelper(this);
//        pref = getApplicationContext().getSharedPreferences("UhuruPayPref", 0);
//        userID = pref.getString("operatorID", "0");

        getSupportActionBar().setDisplayShowHomeEnabled(false);
        getSupportActionBar().setTitle(R.string.header_main_menu);
        getSupportActionBar().setElevation(5);
        getSupportActionBar().setDisplayShowTitleEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);

        db = new SqliteHelper(this);
        pref = getApplicationContext().getSharedPreferences("UhuruPayPref", 0);
        operator_id = pref.getString("operatorID", "");
        mProgressDialog = new ProgressDialog(this);

        recyclerView =  findViewById(R.id.my_recycler_view);
        recyclerView.setHasFixedSize(true);

        layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());

        items = new ArrayList<>();
        menuBuilder myMenu = new menuBuilder("MainMDC", getApplicationContext());

        for (int i = 0; i < myMenu.nameArray.length; i++) {
            items.add(
                    new menuData(
                            myMenu.nameArray[i],
                            myMenu.descriptionArray[i],
                            myMenu.drawableArray[i]
                    )
            );
        }


        adapter = new menuMyAdapter(items);
        recyclerView.setAdapter(adapter);

        recyclerView.addOnItemTouchListener(
                new menuRecyclerItemClickListener(this.getApplicationContext(), new menuRecyclerItemClickListener.OnItemClickListener() {
                    @Override
                    public void onItemClick(View view, int position) {
                        int selectedItemPosition = recyclerView.getChildAdapterPosition(view);
                        if (selectedItemPosition == 0) {
                                Intent intent;
                                if (pref.getString("operatorType", "").equals("2")){
//                                    if (pref.getString("hasTrip","").equals("YES")) {
                                        intent = new Intent(MainMDC.this, FormOperator.class);
                                        startActivity(intent);
//                                    }else{
//                                            Toast.makeText(MainMDC.this,"HAKUNA SAFARI KWA SIKU YA LEO",Toast.LENGTH_LONG).show();
//                                        }
                                  }else{
                                    intent = new Intent(MainMDC.this, FormAgent.class);
                                    startActivity(intent);
                                }

                        } else if (selectedItemPosition == 1) {

                                Intent intent = new Intent(MainMDC.this, ScanActivity.class);
                                startActivity(intent);
                        }
                        else if (selectedItemPosition == 2) {

                                Intent intent = new Intent(MainMDC.this, NfcActivity.class);
                                startActivity(intent);

                        } else if (selectedItemPosition == 3) {

                            SearchSummary();
                        } else {

                            new AlertDialog.Builder(MainMDC.this)
                                    .setTitle(R.string.h_feature_not_allowed)
                                    .setMessage(R.string.p_feature_not_allowed)
                                    .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int whichButton) {

                                        }
                                    }).show();
                        }
                    }
                })
        );
    }


    private String phone_number;
    private EditText et_phone_number;
    private AlertDialog validationDialog;
    private Button cancel, submit;

    private void InquireReferences() {
        try {
            AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(MainMDC.this);
            LayoutInflater inflater = MainMDC.this.getLayoutInflater();
            View dialogView = inflater.inflate(R.layout.reference_search, null);
            dialogBuilder.setView(dialogView);

            et_phone_number =  dialogView.findViewById(R.id.et_phone_number);

            submit =  dialogView.findViewById(R.id.submit);
            cancel =  dialogView.findViewById(R.id.cancel);

            validationDialog = dialogBuilder.create();
            validationDialog.setCancelable(false);
            cancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {
                        if (validationDialog != null)
                            if (validationDialog.isShowing()) {
                                validationDialog.dismiss();
                                validationDialog = null;
                            }


                    } catch (NullPointerException e) {
                        e.printStackTrace();

                        if (validationDialog != null)
                            if (validationDialog.isShowing()) {
                                validationDialog.dismiss();
                                validationDialog = null;
                            }
                    }


                }
            });
            submit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(MainMDC.this, PaymentsReference.class);
                    intent.putExtra("phone_number", et_phone_number.getText().toString().trim());
                    startActivity(intent);
                    if (validationDialog != null)
                        if (validationDialog.isShowing()) {
                            validationDialog.dismiss();
                            validationDialog = null;
                        }

                }
            });
            validationDialog.show();


        } catch (NullPointerException e) {
            e.printStackTrace();
        }
    }


    private TextView etFromDate, etToDate;

    private void SearchSummary() {
        try {
            AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(MainMDC.this);
            LayoutInflater inflater = MainMDC.this.getLayoutInflater();
            View dialogView = inflater.inflate(R.layout.search_summary, null);
            dialogBuilder.setView(dialogView);

            etFromDate =  dialogView.findViewById(R.id.from_date);
            etFromDate.setFocusable(false);
            etFromDate.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    // TODO Auto-generated method stub
                    new DatePickerDialog(MainMDC.this, date1, myCalendar
                            .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                            myCalendar.get(Calendar.DAY_OF_MONTH)).show();
                }
            });
            etToDate =  dialogView.findViewById(R.id.to_date);
            etToDate.setFocusable(false);
            etToDate.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    // TODO Auto-generated method stub
                    new DatePickerDialog(MainMDC.this, date2, myCalendar
                            .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                            myCalendar.get(Calendar.DAY_OF_MONTH)).show();
                }
            });

            submit =  dialogView.findViewById(R.id.submit);
            cancel =  dialogView.findViewById(R.id.cancel);

            validationDialog = dialogBuilder.create();
            validationDialog.setCancelable(false);
            cancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {
                        if (validationDialog != null)
                            if (validationDialog.isShowing()) {
                                validationDialog.dismiss();
                                validationDialog = null;
                            }


                    } catch (NullPointerException e) {
                        e.printStackTrace();

                        if (validationDialog != null)
                            if (validationDialog.isShowing()) {
                                validationDialog.dismiss();
                                validationDialog = null;
                            }
                    }


                }
            });
            submit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {


                    if (validationDialog != null)
                        if (validationDialog.isShowing()) {
                            validationDialog.dismiss();
                            validationDialog = null;
                        }

                    FinalRequest();

                }
            });
            validationDialog.show();


        } catch (NullPointerException e) {
            e.printStackTrace();
        }
    }


    final Calendar myCalendar = Calendar.getInstance();

    DatePickerDialog.OnDateSetListener date1 = new DatePickerDialog.OnDateSetListener() {

        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear,
                              int dayOfMonth) {
            // TODO Auto-generated method stub
            myCalendar.set(Calendar.YEAR, year);
            myCalendar.set(Calendar.MONTH, monthOfYear);
            myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
            fromDate();
        }

    };

    DatePickerDialog.OnDateSetListener date2 = new DatePickerDialog.OnDateSetListener() {

        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear,
                              int dayOfMonth) {
            // TODO Auto-generated method stub
            myCalendar.set(Calendar.YEAR, year);
            myCalendar.set(Calendar.MONTH, monthOfYear);
            myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
            toDate();
        }

    };

    private void fromDate() {
        String myFormat = "dd/MM/yyyy"; //In which you need put here
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);

        etFromDate.setText(sdf.format(myCalendar.getTime()));
    }

    private void toDate() {
        String myFormat = "dd/MM/yyyy"; //In which you need put here
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);

        etToDate.setText(sdf.format(myCalendar.getTime()));
    }

    private void loading_data() {
        mProgressDialog.setIndeterminate(true);
        mProgressDialog.setMessage(getResources().getString(R.string.loader_text));
        mProgressDialog.setCancelable(false);
        mProgressDialog.show();
    }

    private void FinalRequest() {

        loading_data();
        try {

            //Get number of seats selected

            RequestQueue queue = Action.getInstance(getApplicationContext()).getRequestQueue();

            JSONObject jsonObject = new JSONObject();
            jsonObject.accumulate("route_code", "006");
            jsonObject.accumulate("from_date", etFromDate.getText().toString());
            jsonObject.accumulate("to_date", etToDate.getText().toString());
            jsonObject.accumulate("operator_id", operator_id);
            jsonObject.accumulate("operator_type",  pref.getString("operatorType", ""));

            Log.v("XXXXXX", jsonObject + "");

            String ENDPOINT = Action.getEndpoint("index_api.php");
            JsonObjectRequest myReq = new JsonObjectRequest(Request.Method.POST, ENDPOINT, jsonObject, SummarySuccessListener(), createMyReqErrorListener());
            myReq.setRetryPolicy(new DefaultRetryPolicy((int) TimeUnit.SECONDS.toMillis(5), 0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            queue.add(myReq);


        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    /**
     * Getting error response after submission
     */
    private Response.ErrorListener createMyReqErrorListener() {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError ex) {
                Log.v("XXXXXX", ex + "");
                mProgressDialog.dismiss();

                if (alertDialog != null)
                    if (alertDialog.isShowing()) {
                        alertDialog.dismiss();
                    }
                if (validationDialog != null)
                    if (validationDialog.isShowing()) {
                        validationDialog.dismiss();
                    }


                new android.app.AlertDialog.Builder(MainMDC.this)
                        .setTitle(R.string.h_error_details)
                        .setMessage(R.string.connection_error)
                        .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int whichButton) {
                                dialog.dismiss();
                                MainMDC.this.finish();
                            }
                        }).show();

            }
        };
    }


    /**
     * Getting response from server
     */
    private Response.Listener<JSONObject> SummarySuccessListener() {
        return new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(final JSONObject response) {

                Log.v("XXXXXX", response + "");
                mProgressDialog.dismiss();

                String responseCode = Action.getFieldValue(response, "field_39");

                if (responseCode.equals("00")) {
                    if (mProgressDialog != null)
                        if (mProgressDialog.isShowing()) {
                            mProgressDialog.dismiss();
                        }
                    if (validationDialog != null)
                        if (validationDialog.isShowing()) {
                            validationDialog.dismiss();
                        }


                    summaryDialog(response);


                } else {
                    new android.app.AlertDialog.Builder(MainMDC.this)
                            .setTitle(R.string.h_error_details)
                            .setMessage(R.string.connection_error)
                            .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int whichButton) {
                                    dialog.dismiss();
                                    MainMDC.this.finish();
                                }
                            }).show();
                }


            }
        };
    }


    private AlertDialog alertDialog;

    private void summaryDialog(JSONObject response) {
        String from_date, to_date, total_tickets, total_amount, messageReturned;
        from_date = etFromDate.getText().toString().trim();
        to_date = etToDate.getText().toString().trim();
        total_amount = Action.getFieldValue(response, "total_amount");
        total_tickets = Action.getFieldValue(response, "tickets");

        if (total_amount.equalsIgnoreCase("null")) {
            total_amount = "0";
        }

        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(MainMDC.this);
        LayoutInflater inflater = MainMDC.this.getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.confirmation_dialog, null);
        dialogBuilder.setView(dialogView);


        final Button cancel = (Button) dialogView.findViewById(R.id.cancel_action);

        messageReturned =
                "\n\n" + Action.getstringResource(MainMDC.this, R.string.from_date, "", false) + ": " + from_date +
                        "\n\n" + Action.getstringResource(MainMDC.this, R.string.to_date, "", false) + ": " + to_date +
                        "\n\n" + Action.getstringResource(MainMDC.this, R.string.c_total_collections, "", false) + ": " + total_tickets +
                        "\n\n" + Action.getstringResource(MainMDC.this, R.string.total_price, "", false) + ": " + getString(R.string.tzs_currency) + " " + total_amount

        ;


        alertDialog = dialogBuilder.create();
        alertDialog.setTitle(getString(R.string.c_receipt_report));
        alertDialog.setMessage(messageReturned);
        alertDialog.setCancelable(false);
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (alertDialog.isShowing()) {
                    alertDialog.dismiss();
                }

            }

        });
        alertDialog.show();


    }
//    private void LogoutDialog() {
//        try {
//            AlertDialog.Builder pinDialogBuilder = new AlertDialog.Builder(MainMDC.this);
//            LayoutInflater inflater = MainMDC.this.getLayoutInflater();
//            View pinDialogView = inflater.inflate(R.layout.logout_dialog, null);
//            pinDialogBuilder.setView(pinDialogView);
//
//            Button funga = pinDialogView.findViewById(R.id.button_funga);
//            Button rudi = pinDialogView.findViewById(R.id.button_rudi);
//
//            validationDialog = pinDialogBuilder.create();
//            validationDialog.setCancelable(false);
//
//            rudi.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View view) {
//                    try {
//
//                        if (validationDialog != null)
//                            if (validationDialog.isShowing()) {
//                                validationDialog.dismiss();
//                                validationDialog = null;
//                            }
//                    } catch (NullPointerException e) {
//                        e.printStackTrace();
//
//                        if (validationDialog != null)
//                            if (validationDialog.isShowing()) {
//                                validationDialog.dismiss();
//                                validationDialog = null;
//                            }
//                    }
//                }
//            });
//
//            funga.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View view) {
//
//
//                    if (validationDialog != null)
//                        if (validationDialog.isShowing()) {
//                            validationDialog.dismiss();
//                        }
//
//                    MainMDC.this.finish();
//
//                }
//
//            });
//            validationDialog.show();
//
//
//        } catch (NullPointerException e) {
//            e.printStackTrace();
//        }
//
//    }



    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {

            case R.id.home:
                this.finish();
                return true;

//            case R.id.logout_button:
//                LogoutDialog();

            case R.id.restart_button:
                restartDialog();
        }
        return super.onOptionsItemSelected(item);

    }

    private void restartDialog() {

        try {
            AlertDialog.Builder pinDialogBuilder = new AlertDialog.Builder(MainMDC.this);
            LayoutInflater inflater = MainMDC.this.getLayoutInflater();
            View pinDialogView = inflater.inflate(R.layout.restart_menu, null);
            pinDialogBuilder.setView(pinDialogView);

            Button funga = pinDialogView.findViewById(R.id.button_restart);
            Button rudi = pinDialogView.findViewById(R.id.button_back);

            validationDialog = pinDialogBuilder.create();
            validationDialog.setCancelable(false);

            rudi.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    try {

                        if (validationDialog != null)
                            if (validationDialog.isShowing()) {
                                validationDialog.dismiss();
                                validationDialog = null;
                            }
                    } catch (NullPointerException e) {
                        e.printStackTrace();

                        if (validationDialog != null)
                            if (validationDialog.isShowing()) {
                                validationDialog.dismiss();
                                validationDialog = null;
                            }
                    }
                }
            });

            funga.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {


                    if (validationDialog != null)
                        if (validationDialog.isShowing()) {
                            validationDialog.dismiss();
                        }
                    db.clearTickets();
                    requestTkt();
                }

            });
            validationDialog.show();


        } catch (NullPointerException e) {
            e.printStackTrace();
        }

    }

    public void requestTkt(){
        loading_data();
        try {
            RequestQueue queue = Action.getInstance(getApplicationContext()).getRequestQueue();
            JSONObject jsonObject = new JSONObject();
            jsonObject.accumulate("route_code","008");
            jsonObject.accumulate("operator_id", pref.getString("operatorID", ""));
            Log.v("TI_CKET", jsonObject + "");
            String ENDPOINT = Action.getEndpoint("index_api.php");
            JsonObjectRequest myReq = new JsonObjectRequest(Request.Method.POST, ENDPOINT, jsonObject, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject object) {
                    Log.v("TI_CKET", object + "");
                    mProgressDialog.dismiss();
                    String responseCode = Action.getFieldValue(object, "field_39");
                    if (responseCode.equals("00")){
                        if (mProgressDialog != null)
                            if (mProgressDialog.isShowing()) {
                                mProgressDialog.dismiss();
                            }
                        String available_tkt = Action.getFieldValue(object, "assigned_tickets");
                        db.clearTickets();
                        db.addTickets("0", available_tkt);
                    }else{
                        Toast.makeText(MainMDC.this,"resp code == 01",Toast.LENGTH_LONG).show();
                    }
                    if (mProgressDialog != null)
                        if (mProgressDialog.isShowing()) {
                            mProgressDialog.dismiss();
                        }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError volleyError) {
                    mProgressDialog.dismiss();

                    if (mProgressDialog != null)
                        if (mProgressDialog.isShowing()) {
                            mProgressDialog.dismiss();
                        }
                }
            });
            myReq.setRetryPolicy(new DefaultRetryPolicy((int) TimeUnit.SECONDS.toMillis(5), 0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            queue.add(myReq);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.logout_menu,menu);
        return true;
    }


}
