package com.cfm.football.clubs.matcheticketing;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.device.PrinterManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.widget.ImageView;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.cfm.football.clubs.matcheticketing.utilities.Action;
import com.cfm.football.clubs.matcheticketing.utilities.Barcode;
import com.cfm.football.clubs.matcheticketing.utilities.SqliteHelper;
import com.google.zxing.Result;
import org.json.JSONArray;
import org.json.JSONObject;

import me.dm7.barcodescanner.zxing.ZXingScannerView;

public class ScanActivity extends AppCompatActivity implements ZXingScannerView.ResultHandler{

    ProgressDialog mProgressDialog;
    private ZXingScannerView mScannerView;
    private PrinterManager printer;
    private SharedPreferences pref;
    private JSONArray offLineArray,nowSavingArray;
    private String ref;
    SqliteHelper db;
    private String deviceID,operator_id,eventName,clubName,eventLocation;
    private String oppTeam,eventPrice,capacity,eventTime,eventDate;

    @Override
    public void onCreate(Bundle state) {
        super.onCreate(state);
        mScannerView = new ZXingScannerView(this);   // Programmatically initialize the scanner view
        setContentView(mScannerView);                // Set the scanner view as the content view
        mProgressDialog=new ProgressDialog(this);
        pref = getApplicationContext().getSharedPreferences("UhuruPayPref", 0);
        deviceID = pref.getString("deviceID", null);
    }

    @Override
    public void onResume() {
        super.onResume();
        mScannerView.setResultHandler(this); // Register ourselves as a handler for scan results.
        mScannerView.startCamera();          // Start camera on resume
    }

    @Override
    public void onPause() {
        super.onPause();
        mScannerView.stopCamera();           // Stop camera on pause
    }


    @Override
    public void handleResult(Result rawResult) {
        postRequest(rawResult.toString());
    }
    private void postRequest(String refNo ){
        RequestQueue queue = Action.getInstance(getApplicationContext()).getRequestQueue();
        Barcode barcode = new Barcode();
        barcode.setMTI("005");
        barcode.set_field_68(deviceID);
        barcode.setBarcode(refNo);

        String ENDPOINT=Action.getEndpoint("index_api.php");
        JSONObject reqMsg=Action.getBarcode(barcode);

        Log.v("REQUEST_MESSAGE",reqMsg+"");
        JsonObjectRequest myReq = new JsonObjectRequest(Request.Method.POST,ENDPOINT,reqMsg, createMyReqSuccessListener(), createMyReqErrorListener());
        //myReq.setRetryPolicy(new DefaultRetryPolicy((int) TimeUnit.SECONDS.toMillis(5), 0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        queue.add(myReq);
    }
    private Response.Listener<JSONObject> createMyReqSuccessListener() {
        return new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(final JSONObject response) {

                Log.v("RESP_MSG",response+"");
                String responseCode = Action.getFieldValue(response, "field_39");

                        String ref_no = Action.getFieldValue(response, "booking_ref");
                        String event_name = Action.getFieldValue(response, "event_name");
                        String price = Action.getFieldValue(response, "price");
                        String home_team = Action.getFieldValue(response, "home_team");
                        String status = Action.getFieldValue(response, "status");
                        String away_team = Action.getFieldValue(response, "away_team");
                        String location = Action.getFieldValue(response, "location");
                        String event_date = Action.getFieldValue(response,"event_date");
                        String event_time = Action.getFieldValue(response,"event_time");

                if(status.equalsIgnoreCase("VALID")) {
                    mProgressDialog.dismiss();
                    trxPopStatus(getString(R.string.valid), ref_no, event_name, home_team, away_team, price, location, event_date, event_time);
                }else if(status.equalsIgnoreCase("INVALID")){
                    mProgressDialog.dismiss();
                    trxPopError("Invalid ticket");
                }else {
                    mProgressDialog.dismiss();
                    trxPopError("Invalid ticket");
                }

            }
        };
    }
    private Response.ErrorListener createMyReqErrorListener() {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError ex) {
                mProgressDialog.dismiss();
                new android.app.AlertDialog.Builder(ScanActivity.this)
                        .setTitle(R.string.h_error_details)
                        .setMessage(R.string.connection_error)
                        .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int whichButton) {
                                dialog.dismiss();
                                ScanActivity.this.finish();
                            }
                        }).show();
            }
        };
    }

    public void trxPopStatus(String a, String ref_no, String event_name, String home_team, String away_team, String price, String location, String event_date, String event_time){
        ImageView image = new ImageView(this);
        image.setImageResource(R.drawable.accept);
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(getString(R.string.payment))
                .setMessage(Action.getstringResource(getApplicationContext(), R.string.status,"",false)+
                        "\n" + Action.getstringResource(getApplicationContext(), R.string.reference_number,"",false)+": " + ref_no +
                        "\n" +  getString(R.string.event_name)+": " + event_name +
                        "\n" + getString(R.string.home_team)+": " + home_team +
                        "\n" + getString(R.string.away_team)+": " + away_team +
                        "\n" + getString(R.string.event_price)+": " + price +
                        "\n" + getString(R.string.event_location)+": " + location +
                        "\n" + getString(R.string.event_date)+": " + event_date +
                        "\n" + getString(R.string.event_time)+": " + event_time +
                        "\n" + getString(R.string.status)+": " + a
                )
                .setView(image)
                .setCancelable(false)
                .setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        mProgressDialog.dismiss();
                        dialog.cancel();
                        ScanActivity.this.finish();
                    }
                });
        AlertDialog alert = builder.create();
        alert.show();
    }


    /*
     *Dialog to display invalid receipt response
     */
    public void trxPopError(String a){
        ImageView image = new ImageView(this);
        image.setImageResource(R.drawable.deny);
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(getString(R.string.payment))
                .setMessage(
                        "\n\n" + Action.getstringResource(getApplicationContext(), R.string.info,"",false)+": " + a
                )
                .setCancelable(false)
                .setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        mProgressDialog.dismiss();
                        dialog.cancel();
                        ScanActivity.this.finish();
                    }
                })
                .setView(image);
        AlertDialog alert = builder.create();
        alert.show();
    }

    /*
     *Display menu
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        // getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }



}