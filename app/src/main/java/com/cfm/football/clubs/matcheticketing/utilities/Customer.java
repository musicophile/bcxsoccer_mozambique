package com.cfm.football.clubs.matcheticketing.utilities;

import android.annotation.SuppressLint;

import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 * Created by jamesb on 2017/02/27.
 */

public class Customer {
    String MTI, field_3,field_7,field_11, field_61, field_68, field_69,field_58,  first_name, last_name, customer_id, phone_number,excess_luggage;
    String brief_description;
    String problem_details;
    String field_4;
    String field_39;
    String field_37;
    String route_from, route_to;

    public String getRoute_from() {
        return route_from;
    }

    public void setRoute_from(String route_from) {
        this.route_from = route_from;
    }

    public String getRoute_to() {
        return route_to;
    }

    public void setRoute_to(String route_to) {
        this.route_to = route_to;
    }

    public Customer(){

    }

    public Customer(String MTI, String field_3, String field_7, String field_11, String field_61, String field_68, String field_69, String field_58, String first_name, String last_name, String customer_id, String phone_number, String excess_luggage, String brief_description, String problem_details, String field_4, String field_39, String field_37) {
        this.MTI = MTI;
        this.field_3 = field_3;
        this.field_7 = field_7;
        this.field_11 = field_11;
        this.field_61 = field_61;
        this.field_68 = field_68;
        this.field_69 = field_69;
        this.field_58 = field_58;
        this.first_name = first_name;
        this.last_name = last_name;
        this.customer_id = customer_id;
        this.phone_number = phone_number;
        this.excess_luggage = excess_luggage;
        this.brief_description = brief_description;
        this.problem_details = problem_details;
        this.field_4 = field_4;
        this.field_39 = field_39;
        this.field_37 = field_37;
    }

    public String get_rsp_code() {
        return field_39;
    }

    public void set_rsp_code(String value) {
        this.field_39=value;
    }

    public String get_field_37() {
        return field_37;
    }

    public void set_field_37(String field_37) {
        this.field_37 = field_37;
    }

    public String getField_4() {
        return field_4;
    }

    public void setField_4(String field_4) {
        this.field_4 = field_4;
    }

    public String getBrief_description() {
        return brief_description;
    }

    public void setBrief_description(String brief_description) {
        this.brief_description = brief_description;
    }

    public String getProblem_details() {
        return problem_details;
    }

    public void setProblem_details(String problem_details) {
        this.problem_details = problem_details;
    }

    public String getExcess_luggage() {
        return excess_luggage;
    }

    public void setExcess_luggage(String excess_luggage) {
        this.excess_luggage = excess_luggage;
    }

    public void set_provider_name(String value) {
        this.field_58=value;
    }

    public String get_provider_name() {
        return field_58;
    }

    public void set_stan(String _STAN) {
        this.field_11 = _STAN;//field_7.substring(6)
    }
    public void set_stan() {
        this.field_11 = field_7.substring(6);
    }

    public String getField_7() {
        return field_7;
    }

    @SuppressLint("SimpleDateFormat")
    public void setField_7() {
        Calendar cal = Calendar.getInstance();
        cal.getTime();
        SimpleDateFormat sdf = new SimpleDateFormat("yyMMddHHmmss");
        String _datetime = sdf.format(cal.getTime()).toString();
        this.field_7=_datetime;
    }

    public String get_stan() {
        return field_11;
    }

    public String getField_68() {
        return field_68;
    }

    public void setField_68(String field_68) {
        this.field_68 = field_68;
    }

    public String getField_69() {
        return field_69;
    }

    public void setField_69(String field_69) {
        this.field_69 = field_69;
    }

    public String getField_61() {
        return field_61;
    }

    public void setField_61(String field_61) {
        this.field_61 = field_61;
    }


    public String getMTI() {
        return MTI;
    }

    public void setMTI(String MTI) {
        this.MTI = MTI;
    }

    public String getField_3() {
        return field_3;
    }

    public void setField_3(String field_3) {
        this.field_3 = field_3;
    }

    public String getFirst_name() {
        return first_name;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public String getLast_name() {
        return last_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public String getCustomer_id() {
        return customer_id;
    }

    public void setCustomer_id(String customer_id) {
        this.customer_id = customer_id;
    }

    public String getPhone_number() {
        return phone_number;
    }

    public void setPhone_number(String phone_number) {
        this.phone_number = phone_number;
    }
}
