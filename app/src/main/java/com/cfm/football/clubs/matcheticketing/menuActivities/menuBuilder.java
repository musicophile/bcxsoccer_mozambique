package com.cfm.football.clubs.matcheticketing.menuActivities;

import android.content.Context;
import android.content.SharedPreferences;

import com.cfm.football.clubs.matcheticketing.R;
import com.cfm.football.clubs.matcheticketing.utilities.Action;

/**
 * Created by michael.nkotagu on 6/18/2015.
 */
public class menuBuilder {
    public String[] nameArray;
    public String[] descriptionArray;
    public int[] drawableArray;

    private SharedPreferences pref;

    public menuBuilder(String selection, Context context) {
        pref = context.getSharedPreferences("UhuruPayPref", 0);
        if (selection.equals("MainTransactions")) {


            this.nameArray = new String[5];
            this.descriptionArray = new String[5];
            this.drawableArray = new int[5];

            this.nameArray[3] = context.getString(R.string.print_heading);
            this.nameArray[1] = context.getString(R.string.inspect_heading);
            this.nameArray[2] = context.getString(R.string.about_heading);
            this.nameArray[0] = context.getString(R.string.ticket_heading);
            this.nameArray[4] = context.getString(R.string.password_heading);


            this.descriptionArray[3] = context.getString(R.string.print_description);
            this.descriptionArray[1] = context.getString(R.string.inspect_description);
            this.descriptionArray[2] = context.getString(R.string.about_description);
            this.descriptionArray[0] = context.getString(R.string.ticket_description);
            this.descriptionArray[4] = context.getString(R.string.password_description);

            this.drawableArray[2] = R.drawable.about_logo;
            this.drawableArray[1] = R.drawable.code_logo;
            this.drawableArray[0] = R.drawable.bus_logo;
            this.drawableArray[3] = R.drawable.report_logo;
            this.drawableArray[4] = R.drawable.password_logo;

        } else if (selection.equals("MainMDC")) {
            Action.DefaultLanguage(context, Action.language);

            this.nameArray = new String[4];
            this.descriptionArray = new String[4];
            this.drawableArray = new int[4];

            this.descriptionArray[0] = "";
            this.descriptionArray[1] = "";
            this.descriptionArray[2] = "";
            this.descriptionArray[3] = "";


            this.drawableArray[0] = R.drawable.ic_ball;
            this.drawableArray[1] = R.drawable.code_logo;
            this.drawableArray[2] = R.drawable.ic_credit_card_black_24dp;
            this.drawableArray[3] = R.drawable.summary_logo;

            if (pref.getString("operatorType", "").equals("2")){
                this.nameArray[0] = context.getString(R.string.conductor);
                this.nameArray[1] = context.getString(R.string.scanning_code);
                this.nameArray[2] = context.getString(R.string.Verify_Member);
                this.nameArray[3] = context.getString(R.string.agent_summary);
            }else{
                this.nameArray[0] = context.getString(R.string.bus_heading);
                this.nameArray[1] = context.getString(R.string.scanning_code);
                this.nameArray[2] = context.getString(R.string.agent_summary);
                this.nameArray[3] = context.getString(R.string.agent_summary);
            }

        } else if (selection.equals("MainDataTools")) {
            this.nameArray = new String[2];
            this.descriptionArray = new String[2];
            this.drawableArray = new int[2];

            this.nameArray[0] = "Record Business Owner";
            this.nameArray[1] = "Record Business";

            this.descriptionArray[0] = "Make instant collection of fees such as fines etc.";
            this.descriptionArray[1] = "Make collection of recurring payments";

            this.drawableArray[0] = R.drawable.menu_instant;
            this.drawableArray[1] = R.drawable.menu_recurring;

        } else if (selection.equals("MainHospitals")) {
            this.nameArray = new String[2];
            this.descriptionArray = new String[2];
            this.drawableArray = new int[2];

            this.nameArray[0] = context.getString(R.string.h1_hospital_cash);
            this.nameArray[1] = context.getString(R.string.h1_hospital_bima);

            this.descriptionArray[0] = context.getString(R.string.p_hospital_cash);
            this.descriptionArray[1] = context.getString(R.string.p_hospital_bima);

            this.drawableArray[0] = R.drawable.menu_instant;
            this.drawableArray[1] = R.drawable.menu_recurring;

        }


    }
}
