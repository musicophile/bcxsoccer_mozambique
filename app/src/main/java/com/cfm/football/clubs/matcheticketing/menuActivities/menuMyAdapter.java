package com.cfm.football.clubs.matcheticketing.menuActivities;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.cfm.football.clubs.matcheticketing.R;

import java.util.ArrayList;

/**
 * Created by michael.nkotagu on 6/18/2015.
 */

public class menuMyAdapter extends RecyclerView.Adapter<menuMyAdapter.MyViewHolder> {

    private ArrayList<menuData> menuDataSet;

    public static class MyViewHolder extends RecyclerView.ViewHolder {

        TextView textViewName;
        TextView textViewDescription;
        ImageView imageViewIcon;

        public MyViewHolder(View itemView) {
            super(itemView);
            this.textViewName = (TextView) itemView.findViewById(R.id.textViewName);
            this.textViewDescription = (TextView) itemView.findViewById(R.id.textViewDescription);
            this.imageViewIcon = (ImageView) itemView.findViewById(R.id.imageView);
        }
    }

    public menuMyAdapter(ArrayList<menuData> items) {
        this.menuDataSet = items;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.cards_layout, parent, false);

        view.setOnClickListener(MainUtilities.myOnClickListener);

        MyViewHolder myViewHolder = new MyViewHolder(view);
        return myViewHolder;
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int listPosition) {

        TextView textViewName = holder.textViewName;
        TextView textViewDescription = holder.textViewDescription;
        ImageView imageView = holder.imageViewIcon;

        textViewName.setText(menuDataSet.get(listPosition).getName());
        textViewDescription.setText(menuDataSet.get(listPosition).getDescription());
        imageView.setImageResource(menuDataSet.get(listPosition).getImage());
    }

    @Override
    public int getItemCount() {
        return menuDataSet.size();
    }
}
