package com.cfm.football.clubs.matcheticketing.menuActivities;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.cfm.football.clubs.matcheticketing.R;
import com.cfm.football.clubs.matcheticketing.utilities.Action;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

/**
 * Created by michael.nkotagu on 6/18/2015.
 */

//TODO replace ReceiptPrinting class
public class PaymentsAdapter extends RecyclerView.Adapter<PaymentsAdapter.MyViewHolder> {

    private ArrayList<PaymentData> menuDataSet;
    private Context context;
    private ClickListener clickListener;

    public static class MyViewHolder extends RecyclerView.ViewHolder {

        TextView textViewName, textViewDate;
        TextView textViewDescription;
        ImageView imageViewIcon;
        LinearLayout linearLayout;

        public MyViewHolder(View itemView) {
            super(itemView);
            this.linearLayout =  itemView.findViewById(R.id.booking_ref);
            this.textViewName =  itemView.findViewById(R.id.textViewName);
            this.textViewDate =  itemView.findViewById(R.id.date_issued);
            this.textViewDescription =  itemView.findViewById(R.id.textViewDescription);
            this.imageViewIcon =  itemView.findViewById(R.id.imageView);
        }
    }

    public PaymentsAdapter(ArrayList<PaymentData> items, Context context) {
        this.menuDataSet = items;
        this.context = context;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.payment_refs, parent, false);

        view.setOnClickListener(MainUtilities.myOnClickListener);

        MyViewHolder myViewHolder = new MyViewHolder(view);
        return myViewHolder;
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int listPosition) {

        TextView textViewName = holder.textViewName;
        TextView textViewDate = holder.textViewDate;
        TextView textViewDescription = holder.textViewDescription;
        ImageView imageView = holder.imageViewIcon;
        LinearLayout linearLayout = holder.linearLayout;
        linearLayout.setClickable(true);
        linearLayout.setFocusable(true);

        String date_before =  menuDataSet.get(listPosition).getTerminal_date();
        String date_after = formateDateFromstring("yyyy-MM-dd", "dd, MMM yyyy", date_before);
        textViewDate.setText(date_after);
        textViewName.setText(context.getString(R.string.tzs)+" "+menuDataSet.get(listPosition).getAmount());
        textViewDescription.setText("TK"+menuDataSet.get(listPosition).getReference_number());
        imageView.setImageBitmap(Action.CreateCodeBar(menuDataSet.get(listPosition).getReference_number(),384,120));

        linearLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (clickListener != null){
                    clickListener.itemClicked(holder.getAdapterPosition());
                }
            }
        });


    }

    public static String formateDateFromstring(String inputFormat, String outputFormat, String inputDate){
        Date parsed = null;
        String outputDate = "";
        SimpleDateFormat df_input = new SimpleDateFormat(inputFormat, java.util.Locale.getDefault());
        SimpleDateFormat df_output = new SimpleDateFormat(outputFormat, java.util.Locale.getDefault());
        try {
            parsed = df_input.parse(inputDate);
            outputDate = df_output.format(parsed);

        } catch (ParseException e) {
        }
        return outputDate;
    }


    public void setClickListener(ClickListener clickListener){
        this.clickListener = clickListener;
    }

    public interface ClickListener{
        void itemClicked(int position);
    }

    @Override
    public int getItemCount() {
        return menuDataSet.size();
    }
}
