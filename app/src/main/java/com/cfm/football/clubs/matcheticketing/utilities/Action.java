package com.cfm.football.clubs.matcheticketing.utilities;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.widget.ArrayAdapter;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.cfm.football.clubs.matcheticketing.R;
import com.cfm.football.clubs.matcheticketing.Trans;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;



/**
 * @author james.buretta
 */
public class Action {

//    private static String ENDPOINT="https://cfmsoccer.ercis.co.tz/api/"; //URL To Post Request
//    private static String downloadURL="https://cfmsoccer.ercis.co.tz/app/";
    private static String downloadURL="https://cfmsoccer.ercis.co.tz/app/";
    private static String ENDPOINT="http://192.168.43.165/cfmsoccer/api/";// local host http://192.168.43.165/
  //  private static String ENDPOINT="https://cfmsoccer.ercis.co.tz/api/";
    private static Action sInstance=null; //Instance used to access request
    private RequestQueue mRequestQueue;
    private static Context mCtx;

    @SuppressLint("SimpleDateFormat")
    public static String set_terminal_date() {
        Calendar cal = Calendar.getInstance();
        cal.getTime();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        String _datetime = sdf.format(cal.getTime());
        return _datetime;
    }
    @SuppressLint("SimpleDateFormat")
    public static String set_terminal_time() {
        Date cal = Calendar.getInstance().getTime();
        cal.getTime();
        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
        String _datetime = sdf.format(Calendar.getInstance().getTime());
        return _datetime;
    }

    public static void createPref(String key, String value, SharedPreferences pref){
        SharedPreferences.Editor editor = pref.edit();
        editor.putString(key, value);
        editor.commit(); //commit changes
    }

    private Action(Context context){
        mCtx=context;
        mRequestQueue=getRequestQueue();
        sInstance=this;
    }

    public static synchronized Action getInstance(Context context){
        if(sInstance==null)
        {
            sInstance=new Action(context);
        }

        return sInstance;
    }
    private static Bitmap bitmap;
    public static Bitmap CreateCode(String str, int widthBmp, int heightBmp) {
        try {
            // 生成二维矩阵,编码时要指定大小,不要生成了图片以后再进行缩放,以防模糊导致识别失败
            BitMatrix matrix = new MultiFormatWriter().encode(str, BarcodeFormat.QR_CODE, widthBmp, heightBmp);
            int width = matrix.getWidth();
            int height = matrix.getHeight();
            // 二维矩阵转为一维像素数组（一直横着排）
            int[] pixels = new int[width * height];
            for (int y = 0; y < height; y++) {
                for (int x = 0; x < width; x++) {
                    if (matrix.get(x, y)) {
                        pixels[y * width + x] = 0xff000000;
                    } else {
                        pixels[y * width + x] = 0xffffffff;
                    }
                }
            }
            bitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
            // 通过像素数组生成bitmap,具体参考api
            bitmap.setPixels(pixels, 0, width, 0, 0, width, height);

        }catch (WriterException e)
        {
            e.printStackTrace();
        }

        return bitmap;
    }


    /*
  Barcode creation
   */

    public static void next(Context context, Class next_class)
    {
        context.startActivity(new Intent(context,next_class));
    }

    public RequestQueue getRequestQueue(){
        if (mRequestQueue == null) {
            // getApplicationContext() is key, it keeps you from leaking the
            // Activity or BroadcastReceiver if someone passes one in.
            mRequestQueue = Volley.newRequestQueue(mCtx.getApplicationContext());
        }
        return mRequestQueue;
    }

    public <T> void addToRequestQueue(Request<T> req) {
        getRequestQueue().add(req);
    }

    public static synchronized JSONObject getJSONObject(Msg msg)  {
        JSONObject jsonObject = new JSONObject();
         try {
                 jsonObject.accumulate("MTI",       msg.get_MTI());
                 jsonObject.accumulate("field_3",   msg.get_processing_code());
                 jsonObject.accumulate("field_4",   msg.get_amount());
                 jsonObject.accumulate("field_7",   msg.get_transmission_datetime());
                 jsonObject.accumulate("field_11",  msg.get_stan());
                 jsonObject.accumulate("field_12",  msg.get_localtime());
                 jsonObject.accumulate("field_37",  msg.get_field_37());
                 jsonObject.accumulate("field_39",  msg.get_rsp_code());//respostas
                 jsonObject.accumulate("field_41",  msg.get_terminal_id());
                 jsonObject.accumulate("field_42",  msg.get_business_name());
                 jsonObject.accumulate("field_48",  msg.get_additional_data());
                 jsonObject.accumulate("field_52",  msg.get_PIN_data());
                 jsonObject.accumulate("field_58",  msg.get_provider_name());
                 jsonObject.accumulate("field_61",  msg.get_extended_tran_type());
                 jsonObject.accumulate("field_62",  msg.get_field_62());
                 jsonObject.accumulate("field_67",  msg.get_extended_payment_code());
                 jsonObject.accumulate("field_68",  msg.get_field_68());
                 jsonObject.accumulate("field_69",  msg.get_field_69());
                 jsonObject.accumulate("field_100", msg.get_receiving_inst());
                 jsonObject.accumulate("field_102", msg.get_from_acc());
                 jsonObject.accumulate("field_103", msg.get_to_acc());
                 jsonObject.accumulate("field_128", msg.get_field_128());
                 jsonObject.accumulate("route_code", msg.getRoute_code());

         }
         catch (JSONException e) {
             jsonObject=null;
         }

        return jsonObject;
    }


    public static synchronized JSONObject getJSONObject(Trans tns)  {
        JSONObject jsonObject = new JSONObject();
         try {
                 jsonObject.accumulate("route_code",  tns.getRoute_code());
                 jsonObject.accumulate("system_date",  tns.getSystem_date());
                 jsonObject.accumulate("event_name",   tns.getEvent_name());
                 jsonObject.accumulate("location",   tns.getLocation());
                 jsonObject.accumulate("event_date",   tns.getEvent_date());
                 jsonObject.accumulate("event_time",  tns.getEvent_time());
                 jsonObject.accumulate("away_team",  tns.getAway_team());
                 jsonObject.accumulate("home_team",  tns.getHome_team());
                 jsonObject.accumulate("field_68",  tns.getField_68());
                 jsonObject.accumulate("operator_id",  tns.getOperator_id());
                 jsonObject.accumulate("operator_type",  tns.getOperator_type());
                 jsonObject.accumulate("ref_no",  tns.getRef());
                 jsonObject.accumulate("price",  tns.getPrice());
                 jsonObject.accumulate("issued_time",  tns.getIssued_time());

         }
         catch (JSONException e) {
             jsonObject=null;
         }

        return jsonObject;
    }





    public static Bitmap CreateCodeBar(String str, int widthBmp, int heightBmp) {
        try {
            // 生成二维矩阵,编码时要指定大小,不要生成了图片以后再进行缩放,以防模糊导致识别失败
            BitMatrix matrix = new MultiFormatWriter().encode(str, BarcodeFormat.CODE_128, widthBmp, heightBmp);
            int width = matrix.getWidth();
            int height = matrix.getHeight();
            // 二维矩阵转为一维像素数组（一直横着排）
            int[] pixels = new int[width * height];
            for (int y = 0; y < height; y++) {
                for (int x = 0; x < width; x++) {
                    if (matrix.get(x, y)) {
                        pixels[y * width + x] = 0xff000000;
                    } else {
                        pixels[y * width + x] = 0xffffffff;
                    }
                }
            }
            bitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
            // 通过像素数组生成bitmap,具体参考api
            bitmap.setPixels(pixels, 0, width, 0, 0, width, height);

        }catch (WriterException e)
        {
            e.printStackTrace();
        }

        return bitmap;
    }





    /*
    Getting routes
     */
    public static synchronized JSONObject getRoutes(Route route)  {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.accumulate("MTI",       route.getMTI());
            jsonObject.accumulate("field_3",   route.getProcessing_code());
            jsonObject.accumulate("field_4",   route.get_amount());
            jsonObject.accumulate("fromStop",  route.getFrom());
            jsonObject.accumulate("toStop",    route.getTo());
            jsonObject.accumulate("seat",      route.getSeat_no());
            jsonObject.accumulate("customer",  route.getCustomer_name());
            jsonObject.accumulate("category", route.getCustomerCategory());
            jsonObject.accumulate("field_7",   route.get_transmission_datetime());
            jsonObject.accumulate("field_11",  route.get_stan());
            jsonObject.accumulate("field_12",  route.get_localtime());
            jsonObject.accumulate("field_37",  route.get_field_37());
            jsonObject.accumulate("field_39",  route.get_rsp_code());
            jsonObject.accumulate("field_41",  route.get_terminal_id());
            jsonObject.accumulate("field_42",  route.get_business_name());
            jsonObject.accumulate("field_48",  route.get_additional_data());
            jsonObject.accumulate("field_52",  route.get_PIN_data());
            jsonObject.accumulate("field_58",  route.get_provider_name());
            jsonObject.accumulate("field_61",  route.get_extended_tran_type());
            jsonObject.accumulate("field_67",  route.get_extended_payment_code());
            jsonObject.accumulate("field_68",  route.get_field_68());
            jsonObject.accumulate("field_69",  route.get_field_69());
            jsonObject.accumulate("field_100", route.get_receiving_inst());
            jsonObject.accumulate("field_102", route.get_from_acc());
            jsonObject.accumulate("field_103", route.get_to_acc());
            jsonObject.accumulate("field_128", route.get_field_128());
            jsonObject.accumulate("route_code",route.getRoute_code());
            jsonObject.accumulate("phone_number",route.getPhone_number());
            jsonObject.accumulate("bus_id",route.getBus_id());
        }
        catch (JSONException e) {
            jsonObject=null;
        }

        return jsonObject;
    }


    /*
    Verifcation of receipt
     */
    public static synchronized JSONObject getBarcode(Barcode route)  {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.accumulate("route_code",route.getMTI());
            jsonObject.accumulate("field_3",route.getField_3());
            jsonObject.accumulate("field_61",   route.getField_61());
            jsonObject.accumulate("booking_ref", route.getBarcode());
            jsonObject.accumulate("field_68",route.get_field_68());
            jsonObject.accumulate("field_69",route.get_field_69());
            jsonObject.accumulate("field_11",route.get_stan());
            jsonObject.accumulate("field_7",route.get_transmission_datetime());
            jsonObject.accumulate("id_number",route.getIdNo());
            jsonObject.accumulate("phone_number",route.getPhoneNo());
        }
        catch (JSONException e) {
            jsonObject=null;
        }

        return jsonObject;
    }





    /*
     *passwording changing
     */
    public static synchronized JSONObject getPassword(Password password){
        JSONObject jsonObject = new JSONObject();
        try{
            jsonObject.accumulate("MTI",password.getMTI());
            jsonObject.accumulate("field_3", password.getField_3());
            jsonObject.accumulate("field_58", password.get_provider_name());
            jsonObject.accumulate("field_61", password.getField_61());
            jsonObject.accumulate("field_68", password.getField_68());
            jsonObject.accumulate("field_69", password.getField_69());
            jsonObject.accumulate("field_11", password.get_stan());
            jsonObject.accumulate("field_7", password.getField_7());
            jsonObject.accumulate("old_password", password.getOld_password());
            jsonObject.accumulate("new_password", password.getCurrent_password());

        }catch (JSONException e){
            e.printStackTrace();
        }

        return  jsonObject;
    }

    /*
    Password reseting
     */
    public static synchronized JSONObject getPasswordReset(Password password){
        JSONObject jsonObject = new JSONObject();
        try{
            jsonObject.accumulate("MTI",password.getMTI());
            jsonObject.accumulate("field_3", password.getField_3());
            jsonObject.accumulate("field_61", password.getField_61());
            jsonObject.accumulate("field_68", password.getField_68());
            jsonObject.accumulate("field_69", password.getField_69());
            jsonObject.accumulate("field_11", password.get_stan());
            jsonObject.accumulate("field_7", password.getField_7());
            jsonObject.accumulate("email", password.getEmail());


        }catch (JSONException e){
            e.printStackTrace();
        }

        return  jsonObject;
    }


    /*
     *Customer registration
     */
    public static synchronized JSONObject getCustomerDetails(Customer customer){
        JSONObject jsonObject = new JSONObject();
        try{
            jsonObject.accumulate("MTI", customer.getMTI());
            jsonObject.accumulate("field_3", customer.getField_3());
            jsonObject.accumulate("field_58", customer.get_provider_name());
            jsonObject.accumulate("field_61", customer.getField_61());
            jsonObject.accumulate("field_68", customer.getField_68());
            jsonObject.accumulate("field_69", customer.getField_69());
            jsonObject.accumulate("field_11", customer.get_stan());
            jsonObject.accumulate("field_7", customer.getField_7());
            jsonObject.accumulate("field_39", customer.get_rsp_code());
            jsonObject.accumulate("field_37", customer.get_field_37());
            jsonObject.accumulate("first_name", customer.getFirst_name());
            jsonObject.accumulate("last_name", customer.getLast_name());
            jsonObject.accumulate("phone_number", customer.getPhone_number());

        }catch (JSONException e){
            e.printStackTrace();
        }

        return  jsonObject;
    }

    /*
    Operator problem reporting
     */
    public static synchronized JSONObject getOperatorProblems(Customer customer){
        JSONObject jsonObject = new JSONObject();
        try{
            jsonObject.accumulate("MTI", customer.getMTI());
            jsonObject.accumulate("field_3", customer.getField_3());
            jsonObject.accumulate("field_58", customer.get_provider_name());
            jsonObject.accumulate("field_61", customer.getField_61());
            jsonObject.accumulate("field_68", customer.getField_68());
            jsonObject.accumulate("field_69", customer.getField_69());
            jsonObject.accumulate("field_11", customer.get_stan());
            jsonObject.accumulate("field_7", customer.getField_7());
            jsonObject.accumulate("brief_description", customer.getBrief_description());
            jsonObject.accumulate("problem_reported", customer.getProblem_details());
        }catch (JSONException e){
            e.printStackTrace();
        }

        return jsonObject;
    }


    /*
    Reprint ticket
     */
    public static synchronized JSONObject getReprintedCustomerTicket(Customer customer){
        JSONObject jsonObject = new JSONObject();
        try{
            jsonObject.accumulate("MTI", customer.getMTI());
            jsonObject.accumulate("field_3", customer.getField_3());
            jsonObject.accumulate("field_58", customer.get_provider_name());
            jsonObject.accumulate("field_61", customer.getField_61());
            jsonObject.accumulate("field_68", customer.getField_68());
            jsonObject.accumulate("field_69", customer.getField_69());
            jsonObject.accumulate("field_11", customer.get_stan());
            jsonObject.accumulate("field_7", customer.getField_7());
            jsonObject.accumulate("phone_number", customer.getPhone_number());

        }catch (JSONException e){
            e.printStackTrace();
        }

        return  jsonObject;
    }

    /*
     *Excess weight posting
     */
    public static synchronized JSONObject getExcessLuggageCustomerTicket(Customer customer){
        JSONObject jsonObject = new JSONObject();
        try{
            jsonObject.accumulate("MTI", customer.getMTI());
            jsonObject.accumulate("field_3", customer.getField_3());
            jsonObject.accumulate("field_39", customer.get_rsp_code());
            jsonObject.accumulate("field_37", customer.get_field_37());
            jsonObject.accumulate("field_58", customer.get_provider_name());
            jsonObject.accumulate("field_61", customer.getField_61());
            jsonObject.accumulate("field_68", customer.getField_68());
            jsonObject.accumulate("field_69", customer.getField_69());
            jsonObject.accumulate("field_11", customer.get_stan());
            jsonObject.accumulate("field_7",  customer.getField_7());
            jsonObject.accumulate("field_4", customer.getField_4());
            jsonObject.accumulate("excess_weight", customer.getExcess_luggage());
            jsonObject.accumulate("customer_name",customer.getCustomer_id());
            jsonObject.accumulate("phone_number",customer.getPhone_number());
            jsonObject.accumulate("route_from",customer.getRoute_from());
            jsonObject.accumulate("route_to",customer.getRoute_to());

        }catch (JSONException e){
            e.printStackTrace();
        }

        return  jsonObject;
    }

    public static String language="sw";
    public static void DefaultLanguage(Context context, String lang){
        String languageToLoad  = lang; // your language
        Locale locale = new Locale(languageToLoad);
        Locale.setDefault(locale);
        Configuration config = new Configuration();
        config.locale = locale;
        context.getResources().updateConfiguration(config,
                context.getResources().getDisplayMetrics());

        language = lang;
    }


    public static synchronized JSONObject getJSONObject (JSONArray msg, int counter){
        JSONObject jsonObject = new JSONObject();

        try
        {
            jsonObject=msg.getJSONObject(counter);
        }
        catch (JSONException e)
        {
            e.printStackTrace();
        }

        return jsonObject;
    }

    public static synchronized JSONArray getJSONArray(JSONObject msg){
        JSONArray jsonArray = new JSONArray();
        jsonArray.put(msg);

        return jsonArray;
    }

    public static synchronized String parseResponse(JSONObject response){
        String parsedRsp="";
        try {
               String processingCode=response.getString("field_3");
               String extendedTranType=response.getString("field_61");
               String responseCode=response.getString("field_39");
               String MTI=response.getString("MTI");

               if (processingCode.length() > 2) {
                   String tranType=processingCode.substring(0,2);
                   if(MTI.equals("0210")&& tranType.equals("31") && extendedTranType.equals("9006") && responseCode.equals("00")){
                      parsedRsp="Description: " + response.getString("field_48") + "\n\n" +
                                "Quantity: " + response.getString("field_67") + "\n\n"+
                                "Amount: " + response.getString("field_4") + "\n\n";
                   }
                   else
                   {
                        parsedRsp="Error code: "+response.getString("field_39");
                   }

               }
        }
        catch(JSONException ex){

        }
        return parsedRsp;

    }

    public static synchronized String getFieldValue(JSONObject response, String field){
        String responseCode="";
        try {

             responseCode=response.getString(field);
        }
        catch(JSONException ex){

        }
        return responseCode;

    }

    public static synchronized JSONArray getField_48(String response){
        String[] array = response.split("~", -1);

        JSONArray returnVal =  new JSONArray();

        String[] values;

        for(int i=0; i<array.length-1;i++){
            values=array[i].split("\\|");

            try{
                JSONObject jsonObject = new JSONObject();
                jsonObject.put("book_id",values[0]);
                jsonObject.put("ticket_no",values[1]);
                jsonObject.put("departure_date",values[2]);
                jsonObject.put("bus_no",values[3]);
                jsonObject.put("bus_class",values[4]);
                jsonObject.put("route", values[5]);
                jsonObject.put("stop_from", values[6]);
                jsonObject.put("stop_to", values[7]);
                jsonObject.put("bus_fare",values[8]);
                jsonObject.put("customer_name", values[9]);
                jsonObject.put("phone_number", values[10]);
                jsonObject.put("seat_no", values[11]);
                returnVal.put(jsonObject);
            }catch (JSONException e){
                e.printStackTrace();
            }
        }

        return returnVal;
    }

    public static synchronized JSONArray parseField48(String response){

        String[] array = response.split("~", -1); //Response string

        JSONArray returnVal=new JSONArray(); //JSON Array

        String[] values;

        for (int i=0;i<(array.length-1);i++)
        {

            values=array[i].split("\\|");

            try
            {
                JSONObject obj=new JSONObject(); //JSON Object
                obj.put("stop_id",values[0]);
                obj.put("stop_name",values[1]);
                obj.put("longitude",values[2]);
                obj.put("latitude",values[3]);
                obj.put("bus_no",values[4]);
                obj.put("route", values[5]);
                obj.put("bus_class", values[6]);
                returnVal.put(obj);
            }
            catch (JSONException e)
            {
                e.printStackTrace();
            }
        }

        return returnVal;
    }

    public static synchronized JSONArray parseField50(String response){

        JSONArray returnVal=new JSONArray(); //JSON Array

            try
            {
                JSONObject obj=new JSONObject(); //JSON Object
                obj.put("price_rate",response);
                returnVal.put(obj);
            }
            catch (JSONException e)
            {
                e.printStackTrace();
            }


        return returnVal;
    }

    public static synchronized JSONArray parseField49(String response){

        String[] array = response.split("~", -1); //Response string

        JSONArray returnVal=new JSONArray(); //JSON Array

        String[] values;

        for (int i=0;i<(array.length-1);i++)
        {

            values=array[i].split("\\|");

            try
            {
                JSONObject obj=new JSONObject(); //JSON Object
                obj.put("stop_from",values[0]);
                obj.put("stop_to",values[1]);
                obj.put("distance",values[2]);
                obj.put("fare",values[3]);
                returnVal.put(obj);
            }
            catch (JSONException e)
            {
                e.printStackTrace();
            }
        }

        return returnVal;
    }

    public static synchronized String parseErrorResponse(VolleyError ex){

        String message="";
        if (ex instanceof TimeoutError || ex instanceof NoConnectionError) {
            message="Connection could be established";
        } else if (ex instanceof AuthFailureError) {
            message="HTTP Authentication Could not be done";
        } else if (ex instanceof ServerError) {
            message="Server error";
        } else if (ex instanceof NetworkError) {
            message="Network error";
        } else if (ex instanceof ParseError) {
            message="Parse error";
        }else{
            message="Unknown error";
        }
        return message;
    }

    public static synchronized  int resolveRspCode(String rspCode){
        int messageCode=0;

        switch (rspCode)
        {
            case "00":messageCode= R.string.rsp00;break;
            case "01":messageCode= R.string.rsp01;break;
            case "09":messageCode=R.string.rsp09;break;
            case "12":messageCode=R.string.rsp12;break;
            case "13":messageCode=R.string.rsp13;break;
            case "15":messageCode=R.string.rsp15;break;
            case "26":messageCode=R.string.rsp26;break;
            case "42":messageCode=R.string.rsp42;break;
            case "51":messageCode=R.string.rsp51;break;
            case "58":messageCode=R.string.rsp58;break;
            case "63":messageCode=R.string.rsp63;break;
            case "75":messageCode=R.string.rsp75;break;
            case "76":messageCode=R.string.rsp76;break;
            case "91":messageCode=R.string.rsp91;break;
            case "92":messageCode=R.string.rsp92;break;
            case "93":messageCode=R.string.rsp93;break;
            case "94":messageCode=R.string.rsp94;break;
            case "96":messageCode=R.string.rsp96;break;
            default:messageCode=R.string.rspUnkown;break;
        }


      return messageCode;
    }

    public static String getFormattedDate(String inputFormatStr, String outputFormatStr, String dateString){
        DateFormat outputFormat = new SimpleDateFormat(outputFormatStr);
        DateFormat inputFormat =  new SimpleDateFormat(inputFormatStr);
        String outputText="";

        try
        {
            java.util.Date date = inputFormat.parse(dateString);
            outputText = outputFormat.format(date);
        }
        catch (ParseException e)
        {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        return outputText;
    }

    public static String getEndpoint(String finalURL){
        return ENDPOINT+finalURL;
    }

    public static String getDownloadendpoint(){
        return downloadURL;
    }

    public static synchronized String getRightJustifiedText(String str, int perLineLength){

        return insertPeriodically(str, "\n", perLineLength);
    }

    public static String insertPeriodically(String text, String insert, int period)
    {
        StringBuilder builder = new StringBuilder(text.length() + insert.length() * (text.length()/period)+1);

        int index = 0;
        String prefix = "";
        while (index < text.length())
        {
            // Don't put the insert in the very first iteration.This is easier than appending it *after* each substring
            builder.append(prefix);
            prefix = insert;
            builder.append(padRight(text.substring(index, Math.min(index + period, text.length())),24));
            index += period;
        }
        return builder.toString();
    }

    public static String padRight(String s, int n) {
        return String.format("%1$-" + n + "s", s);
    }

    public static String padLeft(String s, int n) {
        return String.format("%1$" + n + "s", s);
    }

    public static String getstringResource(Context ctx, int resourceID, String append, Boolean isUpperCase){
        String string=(ctx.getResources().getString(resourceID)+append);

        if(isUpperCase)
        {
            string=string.toUpperCase();
        }

        return string;
    }

}


