package com.cfm.football.clubs.matcheticketing.utilities;

import android.annotation.SuppressLint;

import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 * Created by jamesb on 2017/02/28.
 */

public class Password {
    String MTI, field_3,field_61,field_7, field_11,field_58,field_68, field_69, old_password, current_password,email;

    public Password(){

    }

    public Password(String MTI, String field_3, String field_61, String field_7, String field_11, String field_58, String field_68, String field_69, String old_password, String current_password, String email) {
        this.MTI = MTI;
        this.field_3 = field_3;
        this.field_61 = field_61;
        this.field_7 = field_7;
        this.field_11 = field_11;
        this.field_58 = field_58;
        this.field_68 = field_68;
        this.field_69 = field_69;
        this.old_password = old_password;
        this.current_password = current_password;
        this.email = email;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void set_provider_name(String value) {
        this.field_58=value;
    }

    public String get_provider_name() {
        return field_58;
    }

    public void set_stan(String _STAN) {
        this.field_11 = _STAN;//field_7.substring(6)
    }
    public void set_stan() {
        this.field_11 = field_7.substring(6);
    }

    public String getField_7() {
        return field_7;
    }

    @SuppressLint("SimpleDateFormat")
    public void setField_7() {
        Calendar cal = Calendar.getInstance();
        cal.getTime();
        SimpleDateFormat sdf = new SimpleDateFormat("yyMMddHHmmss");
        String _datetime = sdf.format(cal.getTime()).toString();
        this.field_7=_datetime;
    }

    public String get_stan() {
        return field_11;
    }

    public String getField_68() {
        return field_68;
    }

    public void setField_68(String field_68) {
        this.field_68 = field_68;
    }

    public String getField_69() {
        return field_69;
    }

    public void setField_69(String field_69) {
        this.field_69 = field_69;
    }

    public String getField_61() {
        return field_61;
    }

    public void setField_61(String field_61) {
        this.field_61 = field_61;
    }

    public String getMTI() {
        return MTI;
    }

    public void setMTI(String MTI) {
        this.MTI = MTI;
    }

    public String getField_3() {
        return field_3;
    }

    public void setField_3(String field_3) {
        this.field_3 = field_3;
    }

    public String getOld_password() {
        return old_password;
    }

    public void setOld_password(String old_password) {
        this.old_password = old_password;
    }

    public String getCurrent_password() {
        return current_password;
    }

    public void setCurrent_password(String current_password) {
        this.current_password = current_password;
    }
}
