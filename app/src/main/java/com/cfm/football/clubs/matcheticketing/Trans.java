package com.cfm.football.clubs.matcheticketing;

public class Trans {
    private String event_name;
    private String location;
    private String event_date;
    private String event_time;
    private String away_team;
    private String home_team;
    private String field_68;
    private String operator_id;
    private String operator_type;
    private String price;
    private String issued_time;
    private String route_code;
    private String ref;
    private String system_date;

    public Trans() {
    }

    public Trans(String route_code, String system_date, String event_name, String location, String event_date, String event_time, String away_team, String home_team, String field_68, String operator_id, String operator_type, String ref, String price, String issued_time) {
        this.event_name = event_name;
        this.location = location;
        this.event_date = event_date;
        this.event_time = event_time;
        this.away_team = away_team;
        this.home_team = home_team;
        this.field_68 = field_68;
        this.operator_id = operator_id;
        this.operator_type = operator_type;
        this.price = price;
        this.issued_time = issued_time;
        this.route_code = route_code;
        this.ref = ref;
        this.system_date = system_date;
    }

    public String getEvent_name() {
        return event_name;
    }

    public void setEvent_name(String event_name) {
        this.event_name = event_name;
    }

    public String getRef() {
        return ref;
    }

    public void setRef(String ref) {
        this.ref = ref;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getEvent_date() {
        return event_date;
    }

    public void setEvent_date(String event_date) {
        this.event_date = event_date;
    }

    public String getEvent_time() {
        return event_time;
    }

    public void setEvent_time(String event_time) {
        this.event_time = event_time;
    }

    public String getAway_team() {
        return away_team;
    }

    public void setAway_team(String away_team) {
        this.away_team = away_team;
    }

    public String getHome_team() {
        return home_team;
    }

    public void setHome_team(String home_team) {
        this.home_team = home_team;
    }

    public String getField_68() {
        return field_68;
    }

    public void setField_68(String field_68) {
        this.field_68 = field_68;
    }

    public String getOperator_id() {
        return operator_id;
    }

    public void setOperator_id(String operator_id) {
        this.operator_id = operator_id;
    }


    public String getOperator_type() {
        return operator_type;
    }

    public void setOperator_type(String operator_type) {
        this.operator_type = operator_type;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getIssued_time() {
        return issued_time;
    }

    public void setIssued_time(String issued_time) {
        this.issued_time = issued_time;
    }

    public String getRoute_code() {
        return route_code;
    }

    public void setRoute_code(String route_code) {
        this.route_code = route_code;
    }

    public String getSystem_date() {
        return system_date;
    }

    public void setSystem_date(String system_date) {
        this.system_date = system_date;
    }

}
