<?php
class App extends chochote
{
    protected $msg;                     //Request message Object
    protected $db;                     //Database connection
    protected $messageCode;

    function __construct($db){
        //Set the database connection and recieve the transaction request message
        $this->db=$db;
        $rawJSON = file_get_contents("php://input");
        $this->msg=json_decode($rawJSON, true);
        $this->log_event('JsonReq',$rawJSON);
        // $this->msg=json_decode($rawJSON, true);
    }

    public function processBooking()
    {

        if(isset($this->msg['route_code']))
        {
            $field_61=$this->msg['route_code'];
        }else{
            if(isset($this->msg[0]['route_code']))
            {
                $field_61=$this->msg[0]['route_code'];
            }
        }
        switch($field_61){
            case '000':
                //SplashScreen
                $this->splashScreenVerification();
                break;
            case '007':

                $this->getOperator();

                break;
            case '003':

                $this->getTransaction();

                break;
            default:
                # code...

                break;
        }
    }

    public function splashScreenVerification()
    {

        if(isset($this->msg['field_68']))
        {
            $device=$this->msg['field_68'];
        }

        // $sql="SELECT count(*) as counter,device_type,printer_BDA FROM `tbl_device_details` WHERE device_imei=:device_imei";
        $sql="SELECT device_type,printer_BDA,device_imei FROM `tbl_device_details` WHERE device_imei=:device_imei";

        try{

            $sql = $this->db->prepare($sql);
            $sql->bindParam(':device_imei',$device);
            $sql->execute();
            $output = $sql->fetch(PDO::FETCH_ASSOC);

            //if($output['counter']>=1)
            if($output['device_imei']!='')
            {
                $output['field_39']='00';
            }
            else{
                $output['field_39']='91';
            }

        }catch(PDOException $e)
        {
            echo $e->getMessage();
        }


        echo json_encode($output);

    }

    public function getOperator()
    {

        $username = $this->msg['field_42'];
        $password = $this->msg['field_52'];
        $imei = $this->msg['field_68'];
        $operator_type=$this->check_who_login($imei,$username);
        $hastrip=$this->check_if_hastrip($operator_type[2]);
//        if(empty($hastrip)){
//            $hastrip='NO';
//        }else{
//            $hastrip='YES';
//        }
        if($operator_type[0]==1){
            $sql="SELECT operator_id,operator_type,P.phone_number FROM tbl_operator AS O
				INNER JOIN bus_ticket.tbl_device_details AS D ON D.device_ID=O.device_ID
				INNER JOIN tbl_bus_companies AS P ON P.company_id=O.company_id
				where username=:username AND password=:password AND device_imei=:imei";
            $sql=$this->db->prepare($sql);
            $sql->bindParam(':username',$username);
            $sql->bindParam(':password',$password);
            $sql->bindParam(':imei',$imei);
            $sql->execute();
            $result=$sql->fetch();
            if($result[0]==0)
            {
                $this->msg['field_39']='91';
                echo (json_encode($this->msg));
            }else{
                $this->msg['field_39']='00';
                $this->msg['operator_id']=$result[0];
                $this->msg['operator_type']=$result[1];
                echo (json_encode($this->msg));
            }
        }
        else{
            try{
                $sql="SELECT O.operator_id,O.operator_type,O.company_id,O.bus_id,B.bus_name, B.plate_number,
							CONCAT(F.stop_name, '-TO-', T.stop_name, '-Via-', V.stop_name) as route,
							`total_seats`,`number_row`, `total_back_seats`, CONCAT(`left_seat_format`,'x' ,`right_seat_format`) as seat_orientation
							,F.stop_name, T.stop_name,P.phone_number, NULL as 'p_o_box',get_company_configured_fare(B.route_id,B.class_id,B.company_id) AS fare,R.route_id
						FROM bus_ticket.`tbl_operator` AS O 
						INNER JOIN bus_ticket.tbl_device_details AS D ON D.device_ID=O.device_ID
                    				inner join tbl_bus_details AS B ON B.bus_id=O.bus_id
                     				inner join tbl_routes AS R ON B.route_id=R.route_id
                       				inner join tbl_stops AS F ON F.stop_id=R.stop_from
                      				inner join tbl_stops AS T ON T.stop_id=R.stop_to
                     				inner join tbl_stops AS V ON V.stop_id=R.via_stop
						INNER JOIN tbl_bus_companies AS P ON P.company_id=O.company_id
						WHERE O.username=:username AND O.password=:password AND D.device_imei=:imei AND O.status=1 AND D.activation_status='1'";


                $sql=$this->db->prepare($sql);
                $sql->bindParam(':username',$username);
                $sql->bindParam(':password',$password);
                $sql->bindParam(':imei',$imei);
                $sql->execute();
                $result=$sql->fetch();
                if($result[0]==0)
                {
                    $this->msg['field_39']='91';
                    $this->msg['count']=$result[0];
                    echo (json_encode($this->msg));
                }else{
                    $this->msg['field_39']='00';
                    $this->msg['operator_id']=$result[0];
                    $this->msg['operator_type']=$result[1];
                    $this->msg['company_id']=$result[2];
                    $this->msg['bus_id']=$result[3];
                    $this->msg['bus_name']=$result[4];
                    $this->msg['plate_number']=$result[5];
                    $this->msg['route']=$result[6];
                    $this->msg['total_seats']=$result[7];
                    $this->msg['number_row']=$result[8];
                    $this->msg['total_back_seats']=$result[9];
                    $this->msg['seat_orientation']=$result[10];
                    $this->msg['from_stop']=$result[11];
                    $this->msg['to_stop']=$result[12];
                    $this->msg['phone_number']=$result[13];
                    $this->msg['p_o_box']=$result[14];
                    $this->msg['fare']=$result[15];
                    $this->msg['route_id']=$result[16];
                    $this->msg['hastrip']=$hastrip;
                    $this->msg['main_route']=$this->get_route_passed();
                    $this->msg['label']=$this->get_seats_label($result[3]);


                    echo (json_encode($this->msg));
                }

            }catch(PDOException $e){
                echo json_encode($e->getMessage());
            }
        }
    }

    public function getTransaction()
    {

        $operator_id = $this->msg['operator_id'];
        $card_id = $this->msg['card_id'];
        if($operator_id=="1"){
            $sql="select card_number from tbl_cards where  card_uid  = :card_id AND status_id = 1";
            $sql=$this->db->prepare($sql);
            $sql->bindParam(':card_id',$card_id);
            $sql->execute();
            $result=$sql->fetch();
            if($result[0]==0)
            {
                $this->msg['status']='inactive';
                echo (json_encode($this->msg));
            }else{
//                echo 'MAGANGA'; exit;
                $sql="select m.card_number, p.start_date,p.expiry_date, m.first_name, m.last_name from tbl_payments as p
                      inner join tbl_member_details as m on m.id =  p.member_id where card_uid  = :card_id and expiry_date >= now();
                      ";
                $sql=$this->db->prepare($sql);
                $sql->bindParam(':card_id',$card_id);
                $sql->execute();
                $result=$sql->fetch();
                if($result[0]==0)
                {
                    $this->msg['status']='Not Paid';
                    echo (json_encode($this->msg));
                }else{
                    $this->msg['card_no']=$result[0];
                    $this->msg['start_date']=$result[1];
                    $this->msg['expiry_date']=$result[2];
                    $this->msg['first_name']=$result[3];
                    $this->msg['last_name']=$result[4];
                    $this->msg['status']='Paid';

                    echo (json_encode($this->msg));
                }
            }
        }
        else{
                            echo 'Error Occured';
        }
    }

    public function log_event($msgType,$text){
        $now = new DateTime();
        $now12 = new DateTime();
        $now=$now->format('Ymd His');
        $today=$now12->format('Y-m-d');

        $logs= 'logsx-'.$today.'.txt';

        $file = 'logs/'.$logs.'';

        //Open the file to get existing content
        if(file_exists($file)){
            $current = file_get_contents($file);
            if (strpos($msgType, 'Res') === false) {
                $current .= "\n$now-$msgType-$text";
            }else{
                $current .= "\n$now-$msgType-$text\n";
            }


        }else {
            $current = "\n$now-$msgType-$text";
        }

        // Write the contents back to the file
        file_put_contents($file, $current);
    }
}

Class chochote{
    public function generate_json_response($response){
        $outputArr = array();
        $jsonData[] = $response;
        $outputArr['Response'] = $jsonData;
        print_r(json_encode($outputArr));
    }
    public function execute_query($sql,$parameters){
        $status=FALSE;
        try
        {
            $sql = $this->db->prepare($sql);
            if($sql->execute($parameters))
            {
                $num_rows=$sql->rowCount();
                if($num_rows>0)
                {
                    $_SESSION['message_code']='SS';
                    $status=TRUE;
                }
                else
                {
                    $_SESSION['message_code']='NR';
                }
            }


        }
        catch (PDOException $e)
        {
            $this->messageCode=$e->errorInfo[1];

            if(DEBUG_MOD)
            {
                //echo $e->getMessage();
            }
        }
        return $status;
    }
    public function fetch_query_result($sql,$parameters,$isloop){
        $status=FALSE;
        $result=null;
        try
        {
            $sql = $this->db->prepare($sql);
            if($parameters!=NULL)
            {
                $sql->execute($parameters);
            }
            else
            {
                $sql->execute();
            }
            if($isloop==true)
            {
                $result=$sql->fetchAll();
            }
            else $result=$sql->fetch();

        }
        catch (PDOException $e)
        {
            $_SESSION['message_code']='FE';
            if(DEBUG_MOD)
            {
                echo $e->getMessage();
                echo '<br/><br/>';
                //var_dump($e);
            }
        }

        return $result;
    }
    public function get_money_value($value,$isMinor){
        if($isMinor)
        {
            $value=substr($value, 0, -2);
        }

        if(is_numeric($value)){
            return number_format($value,2,'.',',');
        }
        else return '-';
    }
}