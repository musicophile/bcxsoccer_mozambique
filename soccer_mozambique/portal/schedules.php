<?php
session_start();
include 'includes/config.php';

$i = 1;
$query = 'SELECT * FROM tbl_event_schedules_match';

$stmt = $crud ->getDetails($query);


$opp_teams = array();
$event_dates = array();
$kickoff_times = array();
$prices = array();
$clubs = array();
$statuses = array();
$ids = array();


$count = $stmt -> rowCount();

//echo $count;

if ($stmt -> rowCount() > 0){
    while($row = $stmt->fetch(PDO::FETCH_ASSOC)){
        $opp_teams[] = $row['opp_team'];
        $event_dates[] = $row['event_date'];
        $kickoff_times[] = $row['kickoff_time'];
        $prices[] = $row['price'];
        $clubs[] = $row['club_id'];
        $statuses[] = $row['validity_status'];
        $ids[] = $row['schedule_id'];
    }
}

include 'includes/layouts/header.php';
?>
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            <a href="new_member.php" class="btn btn-success">New Schedule</a>
        </h1>
        <ol class="breadcrumb">
            <li><a href="welcome.php"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Members</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">All Members</h3>
                    </div>

                    <div class="box-body">
                        <table id="example2" class="table table-bordered table-hover">
                            <thead>
                            <tr>
                                <th>Opponent Team</th>
                                <th>Event Date</th>
                                <th>Kickoff Time</th>
                                                                <th>Payment status</th>
                                <th>Card Number</th>
                                <th>Expiry Date</th>
                                <th>Email</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            for ($j = 0; $j < $count; $j++)
                            {
                                echo
                                    '<tr>'.
                                    '<td>' .$i++ .'</td>'.
                                    '<td>' .$names[$j]. '</td>'.
                                    '<td>' .$membership_types[$j]. '</td>'.
//                                    '<td>' .$payments[$j]. '</td>'.
                                    '<td>' .$card_nos[$j]. '</td>'.
                                    '<td>' .$expiries[$j]. '</td>'.
                                    '<td>' .$emails[$j] .'</td>'.
                                    '<td>'.
                                    '<span><span></span><a href="editMember.php?id='.$ids[$j].'" class="btn btn-success edit"><ion-icon name="create"></ion-icon></a></span>'.
                                    '<span><span></span><a href="delete_member.php?id='.$ids[$j].'" class="btn btn-danger edit"><ion-icon ios="ios-trash" md="md-trash"></ion-icon></a></span>'.
                                    '</td>'.
                                    '</tr>';
                            }
                            ?>
                            </tbody>
                            <tfoot>
                            <tr>
                            <tr>
                                <th>S/No</th>
                                <th>Name</th>
                                <th>Membership Category</th>
                                <!--                                <th>Payment status</th>-->
                                <th>Card Number</th>
                                <th>Expiry Date</th>
                                <th>Email</th>
                                <th>Action</th>
                            </tr>
                            </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
                <!-- /.box -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
</div>






<!--///////////////////////////////////////////////////////////-->
<?php
include 'includes/layouts/footer.php';
?>

