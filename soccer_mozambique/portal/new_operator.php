<?php
session_start();
include 'includes/config.php';
if (isset($_POST['submit'])) {

    $operator_name = $_POST['operator_name'];
    $user_name = $_POST['username'];
    $phone = $_POST['phone'];
    $pitch = $_POST['pitch_id'];
    $password = $_POST['password'];
    $device_id = $_POST['device_ID'];
//    $operator_type = $_POST['operator_type'];
    $status = $_POST['status'];
    $club_id = $_POST['club_id'];

    $query = "INSERT INTO tbl_operator_details(operator_name, username, phone, pitch_id, password, device_id, status, club_id) 
                VALUES('$operator_name', '$user_name', '$phone', '$pitch', '$password', '$device_id' , '$status', '$club_id')";

    $result = $crud->create($query);

}
include 'includes/layouts/header.php';
?>
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            New Operator Details
        </h1>
        <ol class="breadcrumb">
            <li><a href="welcome.php"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="operators.php">Operators</a></li>
            <li class="active">New Operator</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <!-- left column -->
            <div class="col-md-12">
                <!-- general form elements -->
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Fill out form to register operator</h3>
                    </div>
                    <!-- /.box-header -->
                    <!-- form start -->
                    <div class="register-and-edit-form">
                    <form role="form" method="POST" action="<?php echo $_SERVER['PHP_SELF']; ?>">
                        <div class="box-body">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Operator's Name</label>
                                <input type="text" class="form-control" id="exampleInputEmail1" placeholder="Enter Operator Name" name="operator_name">
                            </div>
                            <div class="form-group">
                                <label for="exampleInputEmail1">User Name</label>
                                <input type="text" class="form-control" id="exampleInputEmail1" placeholder="Enter User Name" name="username">
                            </div>
                            <div class="form-group">
                                <label for="exampleInputEmail1">Phone number</label>
                                <input type="text" class="form-control" id="exampleInputEmail1" placeholder="Enter Phone number" name="phone">
                            </div>

                            <div class="form-group">
                                <label> Club</label>
                                <select class="form-control" name ="club_id">
                                    <option value="1">Club 1</option>
                                    <option value="2">Club 2</option>
                                    <option value="3">Club 3</option>
                                    <option value="4">Club 4</option>
                                </select>
                            </div>

                            <div class="form-group">
                                <label> Pitch</label>
                                <select class="form-control" name ="pitch_id">
                                    <option value="1">Pitch 1</option>
                                    <option value="2">Pitch 2</option>
                                    <option value="3">Pitch 3</option>
                                    <option value="4">Pitch 4</option>
                                </select>
                            </div>

                            <div class="form-group">
                                <label> Device Name</label>
                                <select class="form-control" name ="device_ID">
                                    <option value="1">Device 1</option>
                                    <option value="2">Device 2</option>
                                    <option value="3">Device 3</option>
                                    <option value="4">Device 4</option>
                                </select>
                            </div>

                            <div class="form-group">
                                <label for="exampleInputPassword1">Password</label>
                                <input type="text" class="form-control" id="exampleInputPassword1" placeholder="Enter Password" name="password">
                            </div>

                            <div class="form-group">
                                <label> Status</label>
                                <select class="form-control" name ="status">
                                    <option value="1">Active</option>
                                    <option value="2">Inactive</option>
                                    <option value="3">Blocked</option>
                                </select>
                            </div>

                        </div>
                        <div class="box-footer">
                            <input type="submit" class="btn btn-primary" name="submit" value="Add Operator"/>
                        </div>
                    </form>
                    </div>
                </div>
                <!-- /.box -->


            </div>
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
</div>

<?php
include 'includes/layouts/footer.php'
?>

