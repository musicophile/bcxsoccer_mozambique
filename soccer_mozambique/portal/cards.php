<?php
session_start();
include 'includes/config.php';

$i = 1;
$query = 'SELECT * FROM tbl_cards';
$stmt = $crud ->getDetails($query);

$card_nos = array();
$statuses = array();

$count = $stmt -> rowCount();

//echo $count;

if ($stmt -> rowCount() > 0){
    while($row = $stmt->fetch(PDO::FETCH_ASSOC)){
        $card_nos[] = $row['card_number'];
        $statuses[] = $row['status_id'];
    }
}

include 'includes/layouts/header.php';
?>
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <ol class="breadcrumb" style="top: 0;">
            <li><a href="welcome.php"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Cards</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">All Cards</h3>
                    </div>

                    <div class="box-body">
                        <table id="example2" class="table table-bordered table-hover">
                            <thead>
                            <tr>
                                <th>S/No</th>
                                <th>Card Number</th>
                                <th>Status</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            for ($j = 0; $j < $count; $j++)
                            {
                                echo
                                    '<tr>'.
                                    '<td>' .$i++ .'</td>'.
                                    '<td>' .$card_nos[$j]. '</td>'.
                                    '<td>' .$statuses[$j]. '</td>'.
                                    '</tr>';
                            }
                            ?>
                            </tbody>
                            <tfoot>
                            <tr>
                            <tr>
                                <th>S/No</th>
                                <th>Card Number</th>
                                <th>Status</th>
                            </tr>
                            </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
                <!-- /.box -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
</div>






<!--///////////////////////////////////////////////////////////-->
<?php
include 'includes/layouts/footer.php';
?>



