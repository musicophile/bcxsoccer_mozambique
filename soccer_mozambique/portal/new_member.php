<?php
session_start();
include 'includes/config.php';
if (isset($_POST['submit'])) {

    $fname = $_POST['first_name'];
    $lname = $_POST['last_name'];
    $email = $_POST['email'];
    $membership_type = $_POST['membership_id'];
    $card_no = $_POST['card_no'];
    $emails = $_POST['email'];
    $start_date = $_POST['start_date'];
    $expiry_date = $_POST['expiry_date'];

    $query2 = "INSERT INTO tbl_member_details(first_name,last_name,membership_id, card_number, email) 
                VALUES('$fname', '$lname', '$membership_type', '$card_no', '$email')";

    $result2 = $crud->create($query2);
    $id = $DB_con->lastInsertId();

    if ($result2){

//        echo  666; exit;
        $sql = "UPDATE tbl_cards SET status_id = 1 WHERE card_number = '$card_no'";
        $stmt=$crud->updateDetails($sql);

        $query3 = "SELECT c.card_uid as card_uid from tbl_cards as c inner join tbl_member_details as d on c.card_number=d.card_number
                    where c.card_number = 	'$card_no'";
        $result3 = $crud->getDetails($query3);
        if ($result3 -> rowCount() > 0){
            while($row = $result3->fetch(PDO::FETCH_ASSOC)){
                $card_uid = $row['card_uid'];
            }
        }

    }


//
//    $sql = "SELECT id FROM tbl_member_details WHERE membership_id ='$membership_type'";
//    $stmt = $crud->getDetails($sql);
//    if($stmt->rowCount() >0){
//        while($row = $stmt->fetch(PDO::FETCH_ASSOC)){
//            $id = $row['id'];
//        }
//    }

    $query = "INSERT INTO tbl_payments(member_id, start_date,expiry_date,membership_id, card_uid) 
                VALUES('$id','$start_date', '$expiry_date', '$membership_type', '$card_uid')";

    $result = $crud->create($query);

    if ($result) {
        echo "<script>alert('Data inserted successfully')</script>";
    } else
        echo "<script>alert('Error Occured')</script>";

    header('location:members.php');


}
include 'includes/layouts/header.php';
?>
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            New Member Details
        </h1>
        <ol class="breadcrumb">
            <li><a href="welcome.php"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="members.php">Members</a></li>
            <li class="active">New Member</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <!-- left column -->
            <div class="col-md-12">
                <!-- general form elements -->
                <div class="box box-success">
                    <div class="box-header with-border">
                        <h3 class="box-title">Fill out form to add member</h3>
                    </div>
                    <!-- /.box-header -->
                    <!-- form start -->
                    <div class="register-and-edit-form">
                    <form role="form" method="POST" action="<?php echo $_SERVER['PHP_SELF']; ?>">
                        <div class="box-body">
                            <div class="form-group">
                                <label for="exampleInputEmail1">First Name</label>
                                <input type="text" class="form-control" id="exampleInputEmail1" placeholder="Enter First Name" name="first_name">
                            </div>
                            <div class="form-group">
                                <label for="exampleInputEmail1">Second Name</label>
                                <input type="text" class="form-control" id="exampleInputEmail1" placeholder="Enter Second Name" name="last_name">
                            </div>
                            <div class="form-group">
                                <label for="exampleInputEmail1">Email</label>
                                <input type="email" class="form-control" id="exampleInputEmail1" placeholder="Enter Email" name="email">
                            </div>

                            <div class="form-group">
                                <label for="exampleInputPassword1">Card Number</label>
                                <input type="text" class="form-control" id="exampleInputPassword1" placeholder="Enter Card No" name="card_no">
                            </div>

                            <div class="form-group">
                                <label>Membership Category</label>
                                <select class="form-control" name ="membership_id">
                                    <option value="1">Normal</option>
                                    <option value="2">Diamond</option>
                                    <option value="3">Gold</option>
                                    <option value="4">Platinum</option>
                                </select>
                            </div>

                            <div class="form-group">
                                <label for="datepicker">Start Date</label>
                                <input type="text" class="form-control" id="datepicker" placeholder="Enter Card No" name="start_date">
                            </div>

                            <div class="form-group">
                                <label for="exampleInputPassword1">Expiry Date</label>
                                <input type="text" class="form-control" id="exampleInputPassword1" placeholder="Enter Card No" name="expiry_date">
                            </div>


                        </div>
<!--                            <div class="form-group">-->
<!--                                <label for="exampleInputFile">File input</label>-->
<!--                                <input type="file" id="exampleInputFile">-->
<!---->
<!--                                <p class="help-block">Example block-level help text here.</p>-->
<!--                            </div>-->
<!--                            <div class="checkbox">-->
<!--                                <label>-->
<!--                                    <input type="checkbox"> Check me out-->
<!--                                </label>-->
<!--                            </div>-->
<!--                        </div>-->
                        <!-- /.box-body -->

                        <div class="box-footer">
                            <input type="submit" class="btn btn-success" name="submit" value="Add member"/>
                        </div>
                    </form>
                    </div>
                </div>
                <!-- /.box -->


            </div>
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
</div>
<?php
include 'includes/layouts/footer.php'
?>


