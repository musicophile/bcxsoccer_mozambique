<?php


class crud
{
    private $conn;

    public function __construct($db)
    {
        $this->conn = $db;

    }


    public function create ($query)
    {
        try {
            // Solution to HTML Injection (removes HTML, XML, and PHP tags)
//            $fname = htmlspecialchars(strip_tags($fname), ENT_QUOTES, 'UTF-8');
//            $lname = htmlspecialchars(strip_tags($lname), ENT_QUOTES, 'UTF-8');
//            $email = htmlspecialchars(strip_tags($email), ENT_QUOTES, 'UTF-8');
//            $password = htmlspecialchars(strip_tags($password), ENT_QUOTES, 'UTF-8');
//            $membership_type = htmlspecialchars(strip_tags($membership_type), ENT_QUOTES, 'UTF-8');
//            $payment_status = htmlspecialchars(strip_tags($payment_status), ENT_QUOTES, 'UTF-8');
//            $card_no = htmlspecialchars(strip_tags($card_no), ENT_QUOTES, 'UTF-8');
//            $email = htmlspecialchars(strip_tags($email), ENT_QUOTES, 'UTF-8');


            $stmt = $this->conn->prepare($query);
            $stmt -> execute();
//            $stmt->bindValue(":fname", $fname, PDO::PARAM_STR);
//            $stmt->bindValue(":lname", $lname);
//            $stmt->bindValue(":email", $email);
//            $stmt->bindValue(":password", $password);
//            $stmt->bindValue(":membership_type", $membership_type);
//            $stmt->bindValue(":payment_status", $payment_status);
//            $stmt->bindValue(":card_no", $card_no);
//            $stmt->execute();
            return true;
        } catch (PDOException $e) {
            echo $e->getMessage();
            return false;
        }
    }

    public function updateDetails($query){
        try{

            $stmt = $this ->conn ->prepare($query);

            $success  = $stmt->execute();

            if ($success) {

                return true;
            } else

            {

                return false;
            }
        } catch (PDOException $e){
            echo $e->getMessage();
            return false;
        }

    }

    public function delete_details($query){
        try{

            $stmt = $this ->conn ->prepare($query);

            $success  = $stmt->execute();

            if ($success) {

                return true;
            } else

            {

                return false;
            }
        } catch (PDOException $e){
            echo $e->getMessage();
            return false;
        }

    }

    public function getDetails($query)
    {
        $stmt = $this->conn->prepare($query);
        $stmt->execute();
        return $stmt;
    }

    public function getAdmins($query)
    {
        $stmt = $this->conn->prepare($query);
        $stmt->execute();
        return $stmt;
    }
}