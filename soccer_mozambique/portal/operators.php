<?php
session_start();
include 'includes/config.php';

$i = 1;
$query = 'SELECT * FROM tbl_operator_details';
$stmt = $crud ->getDetails($query);

$operator_names = array();
$user_names = array();
$phones = array();
$grounds = array();
$passwords = array();
$devices = array();
$operator_types = array();
$statuses = array();
$clubs = array();

$query2 = "SELECT a.pitch_name as pitch FROM tbl_pitch_details a, tbl_operator_details d WHERE a.pitch_id = d.pitch_id";
$stmt2 = $crud->getDetails($query2);

$count = $stmt -> rowCount();

//echo $count;

if ($stmt -> rowCount() > 0){
    while($row = $stmt->fetch(PDO::FETCH_ASSOC)){
        $operator_names[] = $row['operator_name'];
        $user_names[] = $row['username'];
        $phones[] = $row['phone'];
        $passwords[] = $row['password'];
        $devices[] = $row['device_id'];
//        $statuses[] = $row['activation_status_id'];
        $clubs[] = $row['club_id'];

    }
}

if ($stmt2 -> rowCount() > 0){
    while($row = $stmt2->fetch(PDO::FETCH_ASSOC)){
        $grounds[] = $row['pitch'];

    }
}


include 'includes/layouts/header.php';
?>
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            <a href="new_operator.php" class="btn btn-success">New Operator</a>
        </h1>
        <ol class="breadcrumb">
            <li><a href="welcome.php"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Operators</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">All Members</h3>
                    </div>

                    <div class="box-body">
                        <table id="example2" class="table table-bordered table-hover">
                            <thead>
                            <tr>
                                <th>S/No</th>
                                <th>Operator Name</th>
                                <th>User Name</th>
                                <th>Phone Number</th>
                                <th>Pitch Name</th>
                                <th>Device IMEI</th>
<!--                                <th>Status</th>-->
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            for ($j = 0; $j < $count; $j++)
                            {
                                echo
                                    '<tr>'.
                                    '<td>' .$i++ .'</td>'.
                                    '<td>' .$operator_names[$j]. '</td>'.
                                    '<td>' .$user_names[$j]. '</td>'.
                                    '<td>' .$phones[$j]. '</td>'.
                                    '<td>' .$grounds[$j] .'</td>'.
                                    '<td>' .$devices[$j] .'</td>'.
//                                    '<td>' .$statuses[$j] .'</td>'.
                                    '<td>'.
                                    '<span><span></span><a href="#" class="btn btn-success edit"><ion-icon name="create"></ion-icon></a></span>'.
                                    '<span><span></span><a class="btn btn-danger edit" data-toggle="modal" data-target="#deleteModal"><ion-icon ios="ios-trash" md="md-trash"></ion-icon></a></span>'.
                                    '</td>'.
                                    '</tr>';
                            }
                            ?>
                            </tbody>
                            <tfoot>
                            <tr>
                            <tr>
                                <th>S/No</th>
                                <th>Operator Name</th>
                                <th>User Name</th>
                                <th>Phone Number</th>
                                <th>Pitch Name</th>
                                <th>Device IMEI</th>
<!--                                <th>Status</th>-->
                                <th>Action</th>
                            </tr>
                            </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
                <!-- /.box -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
</div>






<!--///////////////////////////////////////////////////////////-->
<?php
include 'includes/layouts/footer.php';
?>

