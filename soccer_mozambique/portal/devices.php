<?php
session_start();
include 'includes/config.php';

$i = 1;
$query = 'SELECT * FROM tbl_device_details';
$stmt = $crud ->getDetails($query);

$query2 = "SELECT a.description as status FROM tbl_activation_status a, tbl_device_details d WHERE a.id = d.activation_status_id";
$stmt2 = $crud->getDetails($query2);

$query3 = "SELECT a.club_name as club FROM tbl_club_details a, tbl_device_details d WHERE a.club_id = d.club_id";
$stmt3 = $crud->getDetails($query3);


$device_names = array();
$device_types = array();
$device_imeis = array();
$device_serials = array();
$printer_bdas = array();
$versions = array();
$activation_statuses = array();
$clubs = array();


$count = $stmt -> rowCount();

//echo $count;

if ($stmt -> rowCount() > 0){
    while($row = $stmt->fetch(PDO::FETCH_ASSOC)){
        $device_names[] = $row['device_name'];
        $device_types[] = $row['device_type'];
        $device_imeis[] = $row['device_imei'];
        $device_serials[] = $row['device_serial'];
        $printer_bdas[] = $row['printer_BDA'];
        $versions[] = $row['version'];
        $ids[] = $row['device_id'];
    }
}

if ($stmt2 -> rowCount() > 0){
    while($row = $stmt2->fetch(PDO::FETCH_ASSOC)){
        $activation_statuses[] = $row['status'];

    }
}


if ($stmt3 -> rowCount() > 0){
    while($row = $stmt3->fetch(PDO::FETCH_ASSOC)){
        $clubs[] = $row['club'];

    }
}

include 'includes/layouts/header.php';
?>
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            <a href="new_device.php" class="btn btn-success">New Device</a>
        </h1>
        <ol class="breadcrumb">
            <li><a href="welcome.php"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Devices</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">All Devices</h3>
                    </div>

                    <div class="box-body">
                        <table id="example2" class="table table-bordered table-hover">
                            <thead>
                            <tr>
                                <th>S/No</th>
                                <th>Device Type</th>
                                <th>Device Name</th>
                                <th>Device IMEI</th>
                                <th>Device Serial</th>
                                <th>Printer BDA</th>
                                <th>Version</th>
                                <th>Activation Status</th>
                                <th>Club</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            for ($j = 0; $j < $count; $j++)
                            {
                                echo
                                    '<tr>'.
                                    '<td>' .$i++ .'</td>'.
                                    '<td>' .$device_types[$j]. '</td>'.
                                    '<td>' .$device_names[$j]. '</td>'.
                                    '<td>' .$device_imeis[$j]. '</td>'.
                                    '<td>' .$device_serials[$j] .'</td>'.
                                    '<td>' .$printer_bdas[$j] .'</td>'.
                                    '<td>' .$versions[$j] .'</td>'.
                                    '<td>' .$activation_statuses[$j] .'</td>'.
                                    '<td>' .$clubs[$j] .'</td>'.
                                    '<td>'.
                                    '<span><span></span><a href="#" class="btn btn-success edit"><ion-icon name="create"></ion-icon></a></span>'.
                                    '<span><span></span><a class="btn btn-danger edit" data-toggle="modal" data-target="#deleteModal"><ion-icon ios="ios-trash" md="md-trash"></ion-icon></a></span>'.
                                    '</td>'.
                                    '</tr>';
                            }
                            ?>
                            </tbody>
                            <tfoot>
                            <tr>
                            <tr>
                                <th>S/No</th>
                                <th>Device Type</th>
                                <th>Device Name</th>
                                <th>Device IMEI</th>
                                <th>Device Serial</th>
                                <th>Printer BDA</th>
                                <th>Version</th>
                                <th>Activation Status</th>
                                <th>Club</th>
                                <th>Action</th>
                            </tr>
                            </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
                <!-- /.box -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
</div>






<!--///////////////////////////////////////////////////////////-->
<?php
include 'includes/layouts/footer.php';
?>



