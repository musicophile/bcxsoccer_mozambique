<?php
session_start();
include 'includes/config.php';
if (isset($_POST['submit'])) {

    $device_type = $_POST['device_type'];
    $device_name = $_POST['device_name'];
    $device_imei = $_POST['device_imei'];
    $device_serial = $_POST['device_serial'];
    $printer_BDA = $_POST['printer_BDA'];
    $version = $_POST['version'];
    $activation_status = $_POST['activation_status'];
    $club = $_POST['club'];

    $query = "INSERT INTO tbl_device_details(device_type, device_name, device_imei, device_serial, printer_BDA, version, activation_status, club_id) 
                VALUES('$device_type', '$device_name', '$device_imei', '$device_serial', '$printer_BDA', '$version', '$activation_status', '$club')";

    $result = $crud->create($query);

}
include 'includes/layouts/header.php';
?>
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            New Device Details
        </h1>
        <ol class="breadcrumb">
            <li><a href="welcome.php"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="devices.php">Devices</a></li>
            <li class="active">New Device</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <!-- left column -->
            <div class="col-md-12">
                <!-- general form elements -->
                <div class="box box-success">
                    <div class="box-header with-border">
                        <h3 class="box-title">Fill out form to register device</h3>
                    </div>
                    <!-- /.box-header -->
                    <!-- form start -->
                    <div class="register-and-edit-form">
                    <form role="form" method="POST" action="<?php echo $_SERVER['PHP_SELF']; ?>">
                        <div class="box-body">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Device Name</label>
                                <input type="text" class="form-control" id="exampleInputEmail1" placeholder="Enter Device Name" name="device_name">
                            </div>
                            <div class="form-group">
                                <label for="exampleInputEmail1">Device Type</label>
                                <input type="text" class="form-control" id="exampleInputEmail1" placeholder="Enter Device Type" name="device_type">
                            </div>
                            <div class="form-group">
                                <label for="exampleInputEmail1">Device IMEI</label>
                                <input type="text" class="form-control" id="exampleInputEmail1" placeholder="Enter Device IMEI" name="device_imei">
                            </div>
                            <div class="form-group">
                                <label for="exampleInputPassword1">Device Serial</label>
                                <input type="text" class="form-control" id="exampleInputPassword1" placeholder="Enter Device Serial" name="device_serial">
                            </div>

                            <div class="form-group">
                                <label for="exampleInputPassword1">Printer BDA</label>
                                <input type="text" class="form-control" id="exampleInputPassword1" placeholder="Enter Printer BDA" name="printer_BDA">
                            </div>

                            <div class="form-group">
                                <label for="exampleInputPassword1">Version</label>
                                <input type="text" class="form-control" id="exampleInputPassword1" placeholder="Enter Version" name="version">
                            </div>

                            <div class="form-group">
                                <label>Activation status</label>
                                <select class="form-control" name ="activation_status">
                                    <option value="1">Active</option>
                                    <option value="2">In Active</option>
                                </select>
                            </div>

                            <div class="form-group">
                                <label> Club</label>
                                <select class="form-control" name ="club">
                                    <option value="1">Club 1</option>
                                    <option value="2">Club 2</option>
                                    <option value="3">Club 3</option>
                                    <option value="4">Club 4</option>
                                </select>
                            </div>
                        </div>
                        <div class="box-footer">
                            <input type="submit" class="btn btn-success" name="submit" value="Add Device"/>
                        </div>
                    </form>
                </div>
                </div>
                <!-- /.box -->


            </div>
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
</div>

<?php
include 'includes/layouts/footer.php'
?>

