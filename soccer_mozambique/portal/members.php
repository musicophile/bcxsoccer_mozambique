<?php
session_start();
include 'includes/config.php';

$i = 1;
$query = 'SELECT * FROM tbl_member_details';
$query2 = 'SELECT p.expiry_date as expiry_date FROM tbl_payments p, tbl_member_details a WHERE p.member_id = a.id';
$query3 = 'SELECT m.category as category FROM tbl_memberships m, tbl_member_details d WHERE m.membership_id = d.membership_id';

$stmt = $crud ->getDetails($query);
$stmt2 = $crud ->getDetails($query2);
$stmt3 = $crud ->getDetails($query3);


$names = array();
$membership_types = array();
$payments = array();
$emails = array();
$payments = array();
$card_nos = array();
$expiries = array();
$ids = array();


$count = $stmt -> rowCount();

//echo $count;

if ($stmt -> rowCount() > 0){
    while($row = $stmt->fetch(PDO::FETCH_ASSOC)){
            $names[] = $row['first_name'].' '.$row['last_name'];
            $emails[] = $row['email'];
            $card_nos[] = $row['card_number'];
            $ids[] = $row['id'];
    }
}

if ($stmt2 -> rowCount() > 0){
    while($row = $stmt2->fetch(PDO::FETCH_ASSOC)){

            $expiries[] = $row['expiry_date'];
    }
}

if ($stmt3 -> rowCount() > 0){
    while($row = $stmt3->fetch(PDO::FETCH_ASSOC)){
        $membership_types[] = $row['category'];
    }
}


include 'includes/layouts/header.php';
?>
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            <a href="new_member.php" class="btn btn-success">New Member</a>
        </h1>
        <ol class="breadcrumb">
            <li><a href="welcome.php"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Members</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">All Members</h3>
                    </div>

                    <div class="box-body">
                        <table id="example2" class="table table-bordered table-hover">
                            <thead>
                            <tr>
                                <th>S/No</th>
                                <th>Name</th>
                                <th>Membership Category</th>
<!--                                <th>Payment status</th>-->
                                <th>Card Number</th>
                                <th>Expiry Date</th>
                                <th>Email</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            for ($j = 0; $j < $count; $j++)
                            {
                                echo
                                    '<tr>'.
                                    '<td>' .$i++ .'</td>'.
                                    '<td>' .$names[$j]. '</td>'.
                                    '<td>' .$membership_types[$j]. '</td>'.
//                                    '<td>' .$payments[$j]. '</td>'.
                                    '<td>' .$card_nos[$j]. '</td>'.
                                    '<td>' .$expiries[$j]. '</td>'.
                                    '<td>' .$emails[$j] .'</td>'.
                                    '<td>'.
                                    '<span><span></span><a href="editMember.php?id='.$ids[$j].'" class="btn btn-success edit"><ion-icon name="create"></ion-icon></a></span>'.
                                    '<span><span></span><a href="delete_member.php?id='.$ids[$j].'" class="btn btn-danger edit"><ion-icon ios="ios-trash" md="md-trash"></ion-icon></a></span>'.
                                    '</td>'.
                                    '</tr>';
                            }
                            ?>
                            </tbody>
                            <tfoot>
                            <tr>
                            <tr>
                                <th>S/No</th>
                                <th>Name</th>
                                <th>Membership Category</th>
<!--                                <th>Payment status</th>-->
                                <th>Card Number</th>
                                <th>Expiry Date</th>
                                <th>Email</th>
                                <th>Action</th>
                            </tr>
                            </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
                <!-- /.box -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
</div>






<!--///////////////////////////////////////////////////////////-->
<?php
include 'includes/layouts/footer.php';
?>

