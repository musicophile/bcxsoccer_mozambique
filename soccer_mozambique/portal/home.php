
<?php
if($_POST) {
    $username = $_POST['username'];
    $password = $_POST['password'];

    $con = mysqli_connect("localhost","root","");
    if(!$con) {
        die('Could not connect: ' . mysqli_error());
    }

    mysqli_select_db($con, "soccer_mozambique");

    $result = mysqli_query($con, "SELECT username, password FROM tbl_system_users");

    while($row = mysqli_fetch_array($result)) {
        if($row['username'] == $username && $row['password'] == $password) {

            $_SESSION['username'] = $username;
            header("Location: welcome.php");
        }
        else
            unset($_SESSION["username"]);
            $results = "<div class='alert alert-danger' style='color: darkred; margin-left: 36%; margin-top: 20px;'>
                                Wrong details. Please enter valid username and password!
                        </div>";
    }
    echo $results;
}
?>

<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8" name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Login Page</title>

    <style type = "text/css">
        body {
            font-family:Arial, Helvetica, sans-serif;
            font-size:14px;
            background-color: #888888;
        }
        label {
            font-weight:bold;
            width:100px;
            font-size:14px;
        }
        .box {
            border:#666666 solid 1px;
        }

        .box-2
        {
            background-color: cadetblue;
        }

        .login-form input{
            margin-bottom: 30px;
            margin-top: 10px;
            padding: 10px;
            width: 100%;
        }

    </style>

</head>

<body bgcolor = "#FFFFFF">

<div align = "center" style=" margin-top: 60px;">
    <div style = "width:300px; border: 1px solid #508688; background-color: cadetblue;">
        <div style = "background-color:#36bdc1; color:#FFFFFF; padding:10px;"><b>Login</b></div>
    <div class="box-2" >
        <div style = "margin:30px; height: 60%;">

            <form action = "" method = "post" align="left" class="login-form">
                <label>UserName  :</label><input type = "text" name = "username" class = "box"/><br /><br />
                <label>Password  :</label><input type = "password" name = "password" class = "box" /><br/><br />
                <input type = "submit" style="background-color: #36878a; border: #62a4a7;" value = " Login "/><br />
            </form>

<!--            <div style = "font-size:11px; color:#cc0000; margin-top:10px">--><?php //echo $error; ?><!--</div>-->

        </div>
    </div>
    </div>

</div>

</body>
</html>