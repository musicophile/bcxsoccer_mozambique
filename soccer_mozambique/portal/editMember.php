<?php
session_start();
include 'includes/config.php';

$member_id = $_GET['id'];

$query = "SELECT * FROM tbl_member_details WHERE id = '$member_id'";

$stmt = $crud->getDetails($query);

if ($stmt -> rowCount() > 0){
    while($row = $stmt->fetch(PDO::FETCH_ASSOC)){
        $first_name = $row['first_name'];
        $last_name = $row['last_name'];
        $membership_type = $row['membership_id'];
        $email = $row['email'];
        $card_no = $row['card_number'];
        $id = $row['id'];
    }
}

//for updating
if (isset($_POST['submit'])) {
    $fname = $_POST['first_name'];
    $lname = $_POST['last_name'];
    $email = $_POST['email'];
    $membership_type = $_POST['membership_type'];
//    $payment_status = $_POST['payment_status'];
    $card_no = $_POST['card_no'];
    $member_id = $_POST['id'];

    $sql = "UPDATE tbl_member_details SET 
              first_name = '$fname', last_name='$lname', membership_id='$membership_type', card_number='$card_no', email='$email' WHERE
              id='$member_id'";

    $result = $crud->updateDetails($sql);

    if ($result){
        $sql = "SELECT card_uid FROM tbl_cards WHERE card_number = '$card_no'";
        $result3=$crud->getDetails($sql);
        if ($result3 -> rowCount() > 0){
            while($row = $result3->fetch(PDO::FETCH_ASSOC)){
                $card_uid = $row['card_uid'];
            }
        }
        if($result3){
            $sql = "UPDATE tbl_payments SET card_uid = '$card_uid' WHERE member_id = '$member_id'";
            $stmt=$crud->updateDetails($sql);
        }
        header('location: members.php');
        $alert = "<div class='alert alert-success'>Record Updated successfully</div>";
    }
    else
    $alert = "<div class='alert alert-success'>Record Updated successfully</div>";




}


include 'includes/layouts/header.php';
?>
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Edit Member Details
        </h1>
        <ol class="breadcrumb">
            <li><a href="welcome.php"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="members.php">Members</a></li>
            <li class="active">Edit Member</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <!-- left column -->
            <div class="col-md-12">

                <!-- general form elements -->
                <div class="box box-success">
                    <div class="box-header with-border">
                        <h3 class="box-title">Fill out form to edit member's details</h3>
                    </div>
                    <!-- /.box-header -->
                    <!-- form start -->
                    <div class="register-and-edit-form">
                    <form role="form" method="POST" action="<?php echo $_SERVER['PHP_SELF']; ?>">
                        <div class="box-body">
                            <div class="form-group">
                                <input type="hidden" class="form-control" id="exampleInputPassword1" placeholder="Enter Card No" name="id" value="<?php echo $member_id ?>">
                            </div>
                            <div class="form-group">
                                <label for="exampleInputEmail1">Last Name</label>
                                <input type="text" class="form-control" id="exampleInputEmail1" placeholder="Enter First Name" name="first_name" value="<?php echo $first_name ?>">
                            </div>
                            <div class="form-group">
                                <label for="exampleInputEmail1">Second Name</label>
                                <input type="text" class="form-control" id="exampleInputEmail1" placeholder="Enter Second Name" name="last_name" value="<?php echo $last_name ?>">
                            </div>
                            <div class="form-group">
                                <label for="exampleInputEmail1">Email</label>
                                <input type="email" class="form-control" id="exampleInputEmail1" placeholder="Enter Email" name="email" value="<?php echo $email ?>">
                            </div>
                            <div class="form-group">
                                <label for="exampleInputPassword1">Membership Type</label>
                                <input type="text" class="form-control" id="exampleInputPassword1" placeholder="Enter Role" name="membership_type" value="<?php echo $membership_type ?>">
                            </div>

                            <div class="form-group">
                                <label for="exampleInputPassword1">Card Number</label>
                                <input type="text" class="form-control" id="exampleInputPassword1" placeholder="Enter Card No" name="card_no" value="<?php echo $card_no ?>">
                            </div>


<!--                            <div class="form-group">-->
<!--                                <label>Payment Status</label>-->
<!--                                <select class="form-control" name ="payment_status">-->
<!--                                    <option value="0">Indebt</option>-->
<!--                                    <option value="1">Cleared</option>-->
<!--                                </select>-->
<!--                            </div>-->
<!--                            <input value="--><?php //echo $member_id ?><!--" visibility = "hidden"/>-->
                        </div>
                        <!--                            <div class="form-group">-->
                        <!--                                <label for="exampleInputFile">File input</label>-->
                        <!--                                <input type="file" id="exampleInputFile">-->
                        <!---->
                        <!--                                <p class="help-block">Example block-level help text here.</p>-->
                        <!--                            </div>-->
                        <!--                            <div class="checkbox">-->
                        <!--                                <label>-->
                        <!--                                    <input type="checkbox"> Check me out-->
                        <!--                                </label>-->
                        <!--                            </div>-->
                        <!--                        </div>-->
                        <!-- /.box-body -->

                        <div class="box-footer">
                            <input type="submit" class="btn btn-success" name="submit" value="Update user"/>
                        </div>
                    </form>
                    </div>
                </div>
                <!-- /.box -->


            </div>
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
</div>
<?php
include 'includes/layouts/footer.php';
?>
